package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.common

/**
 * Interface for VehicleHealthCheckSubSectionRecyclerAdapter
 */
interface IVehicleHealthCheckSubSectionRecyclerAdapter {
    /**
     * This function will pass the upload Photo click event to parent
     * @param sectionPosition : Int (subsection position in which evnt triggered)
     */
    fun onUploadPhotoClicked(sectionPosition: Int)

    /**
     * This function will pass the image click event to parent to show in full view
     * @param images : List<String> (All captured images)
     * @param selectedIndex : Int (Position where event is triggered)
     */
    fun onImageItemClicked(images: List<String>, selectedIndex: Int)
}