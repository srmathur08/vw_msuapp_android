package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.acknowledge.AcknowledgePagerItemFragment
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.common.VehicleHealthCheckPagerFragment
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview.ThreeSixtyViewPagerItemFragment
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.IVehicleHeathCheckPagerAdapter
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem

class VehicleHeathCheckPagerAdapter(
    private var itemList: List<VehicleHealthCheckUIModelItem>,
    private val manager: FragmentManager,
    private val lifeCycle: Lifecycle,
    private val listener: IVehicleHeathCheckPagerAdapter
) : FragmentStateAdapter(manager, lifeCycle) {

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun createFragment(position: Int): Fragment {

        when (itemList[position].SectionName) {
            "Scratch/Dent/Damage" -> {
                val fragment = ThreeSixtyViewPagerItemFragment()
                fragment.setPage(itemList[position], listener, position)
                return fragment
            }
            "Acknowledge" -> {
                val fragment = AcknowledgePagerItemFragment()
                fragment.setPage(itemList[position], listener, position)
                return fragment
            }
            else -> {
                val fragment = VehicleHealthCheckPagerFragment()
                fragment.setPage(itemList[position], listener, position)
                return fragment
            }
        }
        val fragment = VehicleHealthCheckPagerFragment()
        fragment.setPage(itemList[position], listener, position)
        return fragment
    }

    /**
     * This function will notify the items in Pager Fragment
     * @param sectionIndex : Int (index of page in view pager)
     * @param subSectionIndex : Int (index of subsection inside Pager Fragment)
     */
    fun notify(sectionIndex: Int, subSectionIndex: Int) {
        val fragment = manager.fragments.findLast { f -> f.isVisible }
        (fragment as VehicleHealthCheckPagerFragment).notifySubSection(subSectionIndex)
    }
}