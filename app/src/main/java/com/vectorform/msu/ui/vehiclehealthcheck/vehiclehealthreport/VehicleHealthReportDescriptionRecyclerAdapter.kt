package com.vectorform.msu.ui.vehiclehealthcheck.vehiclehealthreport

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vectorform.msu.databinding.ItemVehicleHealthReportDescriptionCellBinding

class VehicleHealthReportDescriptionRecyclerAdapter(
    val context: Context,
    var itemList: List<VehicleHealthReportDescriptionModel>
) : RecyclerView.Adapter<VehicleHealthReportDescriptionRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ItemVehicleHealthReportDescriptionCellBinding =
            ItemVehicleHealthReportDescriptionCellBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ItemVehicleHealthReportDescriptionCellBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding?.apply {
                textVHRDKey.text = context.getString(itemList[position].key)
                textVHRDValue.text = itemList[position].value
            }
        }
    }
}