package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vectorform.msu.data.input.MarkersValueInput
import com.vectorform.msu.databinding.FragmentThreeSixtyViewPagerItemBinding
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.ui.vehiclehealthcheck.markerdialog.MarkerDialogFragment
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.IVehicleHeathCheckPagerAdapter
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.BaseFragment
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.Logger
import com.vectorform.msu.utils.MarkerEnum
import com.vectorform.msu.views.CarView

class ThreeSixtyViewPagerItemFragment : BaseFragment(),
    ThreeSixtyViewPagerItemContract.IThreeSixtyViewPagerItemFragment, View.OnClickListener,
    CarView.ICarView, MarkerDialogFragment.IMarkerDialog {

    private var binding: FragmentThreeSixtyViewPagerItemBinding? = null
    private lateinit var item: VehicleHealthCheckUIModelItem
    private var presenter: ThreeSixtyViewPagerItemPresenter? = null
    private lateinit var listener: IVehicleHeathCheckPagerAdapter
    private var sectionIndex: Int = 0

    fun setPage(
        pagerItem: VehicleHealthCheckUIModelItem,
        listener: IVehicleHeathCheckPagerAdapter,
        sectionIndex: Int
    ) {
        this.item = pagerItem
        this.listener = listener
        this.sectionIndex = sectionIndex
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = ThreeSixtyViewPagerItemPresenter(
                safeContext,
                item,
                this,
                activity as VehicleHealthCheckActivity
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentThreeSixtyViewPagerItemBinding.inflate(inflater, container, false)
        presenter?.viewCreated(0)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            layoutTSVMarkRepair.setOnClickListener(this@ThreeSixtyViewPagerItemFragment)
            layoutTSVMarkDamage.setOnClickListener(this@ThreeSixtyViewPagerItemFragment)
            carTSVCar.setMarkerListener(this@ThreeSixtyViewPagerItemFragment)
            textTSVUndo.setOnClickListener(this@ThreeSixtyViewPagerItemFragment)
            imageTSVUndo.setOnClickListener(this@ThreeSixtyViewPagerItemFragment)
            imageTSVLeftArrow.setOnClickListener(this@ThreeSixtyViewPagerItemFragment)
            imageTSVRightArrow.setOnClickListener(this@ThreeSixtyViewPagerItemFragment)
            carTSVCar.setOnClickListener(this@ThreeSixtyViewPagerItemFragment)

            inputTSVCleaningRemark.addWatcher(CleaningRemarkWatcher)
            inputTSVDamageRemark.addWatcher(DamageRemarkWatcher)
            inputTSVOffersAndNotes.addWatcher(NoteskWatcher)
        }
    }

    override fun onPause() {
        super.onPause()
        item.ScratchAndDamage = binding?.carTSVCar?.getPoints() ?: ArrayList()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                layoutTSVMarkRepair -> {
                    carTSVCar.enableDrawing(true)
                    carTSVCar.setPainColor(MarkerEnum.Repair.getMarkerColor())

                    Logger.log(
                        "Repair Color Name :: ",
                        MarkerEnum.getMarkerName(Color.parseColor(Constants.REPAIR_MARKER_COLOR))
                    )
                }
                layoutTSVMarkDamage -> {
                    carTSVCar.enableDrawing(true)
                    carTSVCar.setPainColor(MarkerEnum.Damage.getMarkerColor())
                }
                textTSVUndo, imageTSVUndo -> {
                    carTSVCar.undo()
                }
                imageTSVLeftArrow -> {
                    carTSVCar.previousImage()
                }
                imageTSVRightArrow -> {
                    carTSVCar.nextImage()
                }
                carTSVCar -> {
                    presenter?.onCarViewClicked(carTSVCar.getCurrentImageIndex())
                }
            }
        }
    }//endregion

    //Region IThreeSixtyViewPagerFragment
    override fun updateTitle(title: String) {
        binding?.apply {
            textTSVPagerTitle.text = title
        }
    }

    override fun showThreeSixtyImages(imagePaths: List<String>, index: Int) {
        binding?.apply {
            carTSVCar.post {
                carTSVCar.setImageList(imagePaths)
                carTSVCar.setImageIndex(index)
                carTSVCar.setContainerHeightWidth(
                    carTSVCar.width,
                    carTSVCar.height
                )
            }?.apply {
                presenter?.onImageAdded()
            }
        }
    }

    override fun updateScratchAndDamages(markers: ArrayList<ImageMarkerModel>) {
        binding?.apply {
            carTSVCar.setMarkers(markers)
        }
    }

    override fun showCleaningRemark(remark: String) {
        binding?.apply {
            inputTSVCleaningRemark.setValue(remark)
        }
    }

    override fun showDamageRemark(remark: String) {
        binding?.apply {
            inputTSVDamageRemark.setValue(remark)
        }
    }

    override fun showNotes(notes: String) {
        binding?.apply {
            inputTSVOffersAndNotes.setValue(notes)
        }
    }

    override fun openMarkerDialog(dataItem: VehicleHealthCheckUIModelItem, index: Int) {
        binding?.apply {
            val markerDialog = MarkerDialogFragment().apply {
                setPage(dataItem, index, this@ThreeSixtyViewPagerItemFragment)
            }
            markerDialog.show(childFragmentManager, "MarkerDialog")
        }
    }
    //endregion

    //Region CarView.ICarView
    override fun markerAdded(image: String, marker: MarkersValueInput) {
        presenter?.updateMarkers(image, marker)
    }
    //endregion

    //Region Cleaning Remark Text Watcher
    val CleaningRemarkWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            if (!TextUtils.isEmpty(s)) {
                presenter?.updateCleaningRemark(s.toString())
            }
        }

    }
    //endregion

    //Region Damage Remark Text Watcher
    val DamageRemarkWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            if (!TextUtils.isEmpty(s)) {
                presenter?.updateDamageRemark(s.toString())
            }
        }

    }
    //endregion

    //Region Offer and Notes Text Watcher
    val NoteskWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            if (!TextUtils.isEmpty(s)) {
                presenter?.updateNotes(s.toString())
            }
        }
    }
    //endregion

    //Region IMarkerDialog
    override fun onMarkerDrawComplete(index: Int) {
        presenter?.viewCreated(index)
    }
    //endregion
}