package com.vectorform.msu.ui.home.notificationdetails

import com.vectorform.msu.utils.BaseFragmentContract

class NotificationDetailsContract {
   
    interface INotificationDetailsFragment : BaseFragmentContract.IBaseFragment {
        fun bindNotifications()
    }

    interface INotificationDetailsPresenter {
        fun onBackClicked()
        fun fetchNotifications()
    }
    
}