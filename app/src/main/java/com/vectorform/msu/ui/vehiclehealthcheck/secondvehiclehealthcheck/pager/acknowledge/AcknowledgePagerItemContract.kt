package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.acknowledge

import com.vectorform.msu.utils.BaseFragmentContract

interface AcknowledgePagerItemContract {

    interface IAcknowledgePagerItemFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will update the section title
         * @param title : String (section name)
         */
        fun updateTitle(title: String)

        /**
         * This function will initialize the signature view
         */
        fun initSignatureView()

        /**
         * This function will open view to capture SA Signature
         * @param signature : String (optional already captured local path)
         */
        fun captureSaSignature(signature: String?)

        /**
         * This function will open view to capture Customer Signature
         * @param signature : String (optional already captured local path)
         */
        fun captureCustomerSignature(signature: String?)

        /**
         * This function will show the captured SA signature in UI
         * @param signature : String (captured local path)
         */
        fun setSaSignature(signature: String)

        /**
         * This function will show the captured Customer signature in UI
         * @param signature : String (captured local path)
         */
        fun setCustomerSignature(signature: String)

        /**
         * This function will clear the SA Signature
         */
        fun clearSaSignature()

        /**
         * This function will Clear Customer Signature
         */
        fun clearCustomerSignature()
    }

    interface IAcknowledgePagerItemPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will handle SA Signature view click
         */
        fun onSaSignatureClicked()

        /**
         * This function will handle Customer Signature view click
         */
        fun onCustomerSignatureClicked()

        /**
         * This function will handle the captured signature
         * @param signature : String (the local path of captured signature)
         * @param isCustomer : Boolean (to differentiate SA or Customer Signature)
         */
        fun updateSignature(signature: String, isCustomer: Boolean)

        /**
         * This function will handle the clear SA signature in Signature Dialog Fragment
         */
        fun clearSaSignature()

        /**
         * This function will handle the clear Customer signature in Signature Dialog Fragment
         */
        fun clearCustomerSignature()
    }

}