package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck

import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.BaseFragmentContract

interface SecondVehicleHealthCheckContract {

    interface ISecondVehicleHealthCheckFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will bind the VehicleHealthCheck sections in view pager
         * @param itemList : List<VehicleHealthCheckUIModelItem>
         */
        fun bindViewPager(itemList: List<VehicleHealthCheckUIModelItem>)

        /**
         * This function will update the view pager items and set the current item to a particular index
         * @param itemList: List<VehicleHealthCheckUIModelItem>
         * @param index : Int
         */
        fun bindTab(itemList: List<VehicleHealthCheckUIModelItem>, index: Int)

        /**
         * This function will notify the child adapter when image is clicked
         * @param sectionIndex : Int (index of section in view pager)
         * @param subSectionIndex : Int (index of subsection in which image is added)
         */
        fun updateImageInSubSection(sectionIndex: Int, subSectionIndex: Int)

        /**
         * This function will show the validation toaster if subsection items are not marked
         * @param message : String
         */
        fun showValidationMessage(message: String)
    }

    interface ISecondVehicleHealthCheckPresenter {
        /**
         * This function will handle the back button click
         * @param index : Int (index of current selected page in view pager)
         */
        fun onBackClicked(index: Int)

        /**
         * This function will handle the next button click
         * @param index : Int (index of current selected page in view pager)
         */
        fun onNextClicked(index: Int)

        /**
         * This function will fetch the vehicle health check list data initially
         */
        fun getHealthCheckList()

        /**
         * This function will handle the upload photo button click in subsection
         * @param sectionIndex : Int (index of section in view pager)
         * @param subSectionIndex : Int (index of subsection in which upload photo clicked)
         */
        fun uploadPhotoClicked(sectionIndex: Int, subSectionIndex: Int)

        /**
         * This function will handle the camera result
         * @param imagePath : String (path of image in local storage)
         */
        fun processCameraResult(imagePath: String)
    }

}