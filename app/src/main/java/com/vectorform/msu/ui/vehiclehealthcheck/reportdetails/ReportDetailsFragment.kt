package com.vectorform.msu.ui.vehiclehealthcheck.reportdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.vectorform.msu.databinding.FragmentVehicleHealthReportDetailsBinding
import com.vectorform.msu.ui.vehiclehealthcheck.IVehicleImageClicked
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.SubSection
import com.vectorform.msu.utils.BaseFragment

class ReportDetailsFragment : BaseFragment(), ReportDetailsContract.IReportDetailsFragment,
    View.OnClickListener,
    IVehicleImageClicked {

    private var binding: FragmentVehicleHealthReportDetailsBinding? = null
    private var presenter: ReportDetailsContract.IReportDetailsPresenter? = null
    private val args: ReportDetailsFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = ReportDetailsPresenter(
                safeContext,
                this,
                activity as VehicleHealthCheckActivity,
                args.itemList
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVehicleHealthReportDetailsBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageReportDeatilsBack.setOnClickListener(this@ReportDetailsFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageReportDeatilsBack -> {
                    presenter?.onBackClicked()
                }
            }
        }
    }
    //endregion

    //Region IReportDetailsFragment
    override fun updateTitle(title: String) {
        binding?.apply {
            textReportDeatilsTitle.text = title
            textDetailsSectionTitle.text = "Vehicle\n$title Score"
            textDetailsImagesHeader.text = "Images of $title"
        }
    }

    override fun updateIcon(icon: Int) {
        binding?.apply {
            imageDetailsSectionIcon.setImageResource(icon)
        }
    }

    override fun updatePercent(percent: String) {
        binding?.apply {
            textDetailsSectionPercent.text = percent
        }
    }

    override fun setHeaderBackground(drawableRes: Int) {
        binding?.apply {
            layoutDetailsHeaderContainer.setBackgroundResource(drawableRes)
        }
    }

    override fun applyColor(color: Int) {
        binding?.apply {
            imageDetailsSectionIcon.setColorFilter(color)
            textDetailsSectionTitle.setTextColor(color)
            textDetailsSectionPercent.setTextColor(color)
        }
    }

    override fun bindDetails(subSections: List<SubSection>) {
        activity?.let { safeContext ->
            binding?.apply {
                val adapter = ReportDetailsSubSectionRecyclerAdapter(safeContext, subSections)
                recyclerDetailsDetails.layoutManager = LinearLayoutManager(safeContext)
                recyclerDetailsDetails.adapter = adapter
            }
        }
    }

    override fun bindImages(imageList: List<String>) {
        activity?.let { safeContext ->
            binding?.apply {
                val adapter = ReportDetailsPhotoRecyclerAdapter(
                    safeContext,
                    imageList,
                    this@ReportDetailsFragment
                )
                recyclerDetailsImages.layoutManager = GridLayoutManager(safeContext, 3)
                recyclerDetailsImages.adapter = adapter
            }
        }
    }

    override fun hideImageContainer() {
        binding?.apply {
            recyclerDetailsImages.visibility = View.GONE
            textDetailsImagesHeader.visibility = View.GONE
        }
    }
    //endregion

    //Region IVehicleImageClicked
    override fun onImageItemClicked(images: List<String>, selectedIndex: Int) {
        presenter?.onImageItemClicked(images, selectedIndex)
    }
    //endregion
}