package com.vectorform.msu.ui.home.notifications

import com.vectorform.msu.data.responses.vf.NotificationResponse
import com.vectorform.msu.data.responses.vf.Notifications
import com.vectorform.msu.utils.BaseFragmentContract

class NotificationsContract {
   
    interface INotificationFragment : BaseFragmentContract.IBaseFragment {
        fun bindNotifications(notifications : List<Notifications>)
        fun showNoData()
        fun hideNoData()
    }

    interface INotificationPresenter {
        fun onBackClicked()
        fun fetchNotifications()
        fun onNotificationClicked(notification : Notifications)
    }
    
}