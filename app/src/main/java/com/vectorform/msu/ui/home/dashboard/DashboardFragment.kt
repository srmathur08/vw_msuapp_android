package com.vectorform.msu.ui.home.dashboard

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.fcaindia.drive.utils.CommonUtils
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.vf.DashboardCounts
import com.vectorform.msu.data.responses.vf.LoginResponse
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.databinding.FragmentDashboardBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.BaseFragment
import com.vectorform.msu.utils.Constants

class DashboardFragment : BaseFragment(), DashboardContract.IDashboardFragment,
    IDashBoardServiceRecylcerAdapter, View.OnClickListener, MotionLayout.TransitionListener {

    private var binding: FragmentDashboardBinding? = null
    private var presenter: DashboardContract.IDashBoardPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = DashBoardPresenter(safeContext, this, activity as HomeActivity)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(binding?.motionDashboardLayout?.transitionState ?: outState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDashboardBinding.inflate(inflater, container, false)
        savedInstanceState?.let { inState ->
            binding?.motionDashboardLayout?.transitionState = inState
        }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            viewInProgressBackground.setOnClickListener(this@DashboardFragment)
            imageDashboardWorkshop.setOnClickListener(this@DashboardFragment)
            textDashboardServiceRequest.setOnClickListener(this@DashboardFragment)
            imageDashboardPrivateVehicle.setOnClickListener(this@DashboardFragment)
            textDashboardVehicleHealthCheck.setOnClickListener(this@DashboardFragment)
            imageDashboardBackground.setOnClickListener(this@DashboardFragment)
            layoutDashboardProfile.setOnClickListener(this@DashboardFragment)
            textNotificationCount.setOnClickListener(this@DashboardFragment)
            motionDashboardLayout.setTransitionListener(this@DashboardFragment)
            swipeDashboardRefresh.setOnRefreshListener {
                swipeDashboardRefresh.isRefreshing = false
                presenter?.fetchServiceList()
            }
            textDashboardMTD.setOnClickListener(this@DashboardFragment)
            textDashboardToday.setOnClickListener(this@DashboardFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter?.fetchServiceList()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region IDashboardPresenter
    override fun bindServiceRecycler(services: List<SRsByDateResponse.ServiceRequest>) {
        activity?.let { safeContext ->
            binding?.apply {
                val adapter =
                    DashboardServiceRecyclerAdapter(safeContext, services, this@DashboardFragment)
                recyclerDashboardServices.layoutManager = LinearLayoutManager(safeContext)
                recyclerDashboardServices.adapter = adapter
            }
        }
    }

    override fun resetAddTransition() {
        binding?.apply {
            layoutDashboardServiceImageContainer.transitionToStart()
        }
    }

    override fun showUsername(loginModel: LoginResponse) {
        binding?.apply {
            textDashboardUsername.text = loginModel.Fullname
        }
    }

    override fun showNoData() {
        binding?.apply {
            textDashboardNoData.text = getString(R.string.text_no_data)
            recyclerDashboardServices.visibility = View.INVISIBLE
        }
    }

    override fun hideNoData() {
        binding?.apply {
            recyclerDashboardServices.visibility = View.VISIBLE
            textDashboardNoData.text = ""
        }
    }

    override fun enableWorkInProgress(wip: SRsByDateResponse.ServiceRequest) {
        binding?.apply {
            motionDashboardLayout.setTransition(R.id.transitionDashboardMotionScene)
            Glide.with(imageDashboardVehicle.context).load(Constants.BASEURL + wip.VehicleImage)
                .diskCacheStrategy(
                    DiskCacheStrategy.NONE
                ).placeholder(R.color.app_bg_color).into(imageDashboardVehicle)
            textDashboardServiceCardTitle.text = wip.CustomerName
            textDashboardVehicleNumber.text = wip.RegNum
            textDashboardCardDistance.text = wip.OdometerReading
            textDashboardServiceType.text = wip.ServiceType
            hideAddIcon()
        }
    }

    override fun disableWorInProgress() {
        binding?.apply {
            motionDashboardLayout.setTransition(R.id.transitionDashboardMotionSceneDisabled)
            hideAddIcon()
        }
    }

    override fun updateDate(date: String) {
        binding?.apply {
            textDashboardDate.text = date
        }
    }

    override fun emptyServiceRecycler() {
        binding?.apply {
            recyclerDashboardServices.adapter = null
        }
    }

    override fun showUserProfilePic(picPath: String?) {
        activity?.let { safeContext ->
            binding?.apply {
                Glide.with(safeContext).load(picPath).skipMemoryCache(true).diskCacheStrategy(
                    DiskCacheStrategy.NONE
                ).into(imageDashboardUser)
            }
        }
    }

    override fun showAddIcon() {
        binding?.apply {
            layoutDashboardServiceImageContainer.transitionToState(R.id.StartClickAddTransition)
        }
    }

    override fun hideAddIcon() {
        binding?.apply {
            layoutDashboardServiceImageContainer.transitionToState(R.id.constraintHideAdd)
        }
    }

    override fun highlightToday(counts: DashboardCounts?) {
        activity?.let { safeContext ->
            binding?.apply {
                counts?.let { dCounts ->

                    textDashboardAssignedCount.text = dCounts.Service_Assigned_Today.toString()
                    textDashboardCompletedCount.text = dCounts.Service_Completed_Today.toString()
                    textDashboardMissedCount.text = dCounts.Service_Missed_Today.toString()
                    textDashboardAmountCount.text =
                        CommonUtils.getFormattedAmount(dCounts.Estimate_Amount_Today)

                    textDashboardToday.setBackgroundColor(
                        ContextCompat.getColor(
                            safeContext,
                            R.color.button_enabled_color
                        )
                    )
                    textDashboardToday.setTextColor(
                        ContextCompat.getColor(
                            safeContext,
                            R.color.white
                        )
                    )
                    textDashboardMTD.setBackgroundColor(
                        ContextCompat.getColor(
                            safeContext,
                            R.color.white
                        )
                    )
                    textDashboardMTD.setTextColor(
                        ContextCompat.getColor(
                            safeContext,
                            R.color.app_bg_color
                        )
                    )
                }
            }
        }
    }

    override fun highlightMTD(counts: DashboardCounts?) {
        activity?.let { safeContext ->
            binding?.apply {
                counts?.let { dCounts ->

                    textDashboardAssignedCount.text = dCounts.Service_Assigned_Month.toString()
                    textDashboardCompletedCount.text = dCounts.Service_Completed_Month.toString()
                    textDashboardMissedCount.text = dCounts.Service_Missed_Month.toString()
                    textDashboardAmountCount.text =
                        CommonUtils.getFormattedAmount(dCounts.Estimate_Amount_Month)

                    textDashboardMTD.setBackgroundColor(
                        ContextCompat.getColor(
                            safeContext,
                            R.color.button_enabled_color
                        )
                    )
                    textDashboardMTD.setTextColor(
                        ContextCompat.getColor(
                            safeContext,
                            R.color.white
                        )
                    )
                    textDashboardToday.setBackgroundColor(
                        ContextCompat.getColor(
                            safeContext,
                            R.color.white
                        )
                    )
                    textDashboardToday.setTextColor(
                        ContextCompat.getColor(
                            safeContext,
                            R.color.app_bg_color
                        )
                    )
                }
            }
        }
    }
    //end region

    //Region IDashboardServiceRecyclerAdapter
    override fun onServiceItemClicked(service: SRsByDateResponse.ServiceRequest) {
        presenter?.onServiceRequestItemClicked(service)
    }
    //endregion

    //Region ViewOnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                viewInProgressBackground -> {
                    presenter?.onUpdateStatusClicked()
                }
                imageDashboardWorkshop, textDashboardServiceRequest -> {
                    presenter?.onServiceRequestClicked()
                }
                imageDashboardPrivateVehicle, textDashboardVehicleHealthCheck -> {
                    presenter?.onVehicleHealthCheckClicked()
                }
                imageDashboardBackground -> {
                    presenter?.onBackgroundImageClicked()
                }
                layoutDashboardProfile, textNotificationCount -> {
                    presenter?.onProfileClicked()
                }
                textDashboardMTD -> {
                    presenter?.onMTDClicked()
                }
                textDashboardToday -> {
                    presenter?.onTodayClicked()
                }
            }
        }
    }
    //endregion

    //Region Motion Layout Transition Listener
    override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
        Log.e("Transition ", p0?.toString() ?: "")
    }

    override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
        Log.e("Transition ", p0?.toString() ?: "")
    }

    override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
        Log.e("Transition ", p0?.toString() ?: "")
        binding?.swipeDashboardRefresh?.isEnabled = p1 == R.id.dashboard_motion_start
    }

    override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {
        Log.e("Transition ", p0?.toString() ?: "")
    }
    //end region


}