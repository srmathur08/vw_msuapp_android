package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck

import android.content.Context
import android.text.TextUtils
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.utils.CommonUtils
import com.google.gson.Gson
import com.vectorform.msu.R
import com.vectorform.msu.data.input.CreateVHCDataArrayInput
import com.vectorform.msu.data.input.CreateVHCDataAssetInput
import com.vectorform.msu.data.input.CreateVHCDataInput
import com.vectorform.msu.data.responses.vf.CreateVHCDataArrayResponse
import com.vectorform.msu.data.responses.vf.CreateVHCDataAssetResponse
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModel
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.*

class SecondVehicleHealthCheckPresenter(
    private val context: Context,
    private val view: SecondVehicleHealthCheckContract.ISecondVehicleHealthCheckFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter,
) : SecondVehicleHealthCheckContract.ISecondVehicleHealthCheckPresenter {

    private var itemList = ArrayList<VehicleHealthCheckUIModelItem>()
    private var subItemList = ArrayList<List<VehicleHealthCheckUIModelItem>>()
    private var chunkIndex = 0

    private var photoSectionIndex: Int = 0
    private var photoSubSectionIndex: Int = 0

    init {
        var uiJson = UiJsonParser().loadJSONFromAsset(context)
        itemList = Gson().fromJson(
            uiJson,
            VehicleHealthCheckUIModel::class.java
        )
        for (section in itemList) {
            when (section.SectionName) {
                Constants.SECTION_INSPECTION -> {
                    section.Icon = R.drawable.ic_inspection
                }
                Constants.SECTION_INTERIOR -> {
                    section.Icon = R.drawable.ic_interior
                }
                Constants.SECTION_EXTERIOR -> {
                    section.Icon = R.drawable.ic_exterior
                }
                Constants.SECTION_ENGINE_COMPARTMENT -> {
                    section.Icon = R.drawable.ic_engine_compartment
                }
                Constants.SECTION_BOOT -> {
                    section.Icon = R.drawable.ic_boot
                }
                Constants.SECTION_EXTERIOR_CHECK -> {
                    section.Icon = R.drawable.ic_exterior_lift
                }
            }
        }

        val chunks = itemList.withIndex().groupBy { it.index / 4 }.map { it.value.map { it.value } }
        subItemList.addAll(chunks)
    }


    //Region ISecondVehicleHealthCheckPresenter
    override fun onBackClicked(index: Int) {
        if (chunkIndex == 0 && index == 0) {
            router?.popBack()
        } else {
            if (index == 0) {
                --chunkIndex
                view?.bindViewPager(subItemList[chunkIndex])
                view?.bindTab(subItemList[chunkIndex], subItemList[chunkIndex].size - 1)
            } else {
                view?.bindTab(subItemList[chunkIndex], index - 1)
            }
        }
    }

    override fun onNextClicked(index: Int) {

        var validated = false
        var message = ""
        when (subItemList[chunkIndex][index].SectionName) {
            Constants.SECTION_SCRATCH_DENT_AND_DAMAGE -> {
                validated = true
//                uploadScratchAndDamageData(subItemList[chunkIndex][index])
            }
            Constants.SECTION_ACKNOWLEDGE -> {
                if (!TextUtils.isEmpty(subItemList[chunkIndex][index].CustomerSignature)
                    && !TextUtils.isEmpty(subItemList[chunkIndex][index].SaSignature)
                ) {
                    validated = true
//                    uploadAcknowledgeData(subItemList[chunkIndex][index])
                } else {
                    message = "Signatures are mandatory"
                    validated = false
                }
            }
            else -> {

                val statuses = subItemList[chunkIndex][index].SubSection.filter { s -> s.Status > 0 }.size
                val validatedCount = subItemList[chunkIndex][index].SubSection.filter { s -> s.Validated }.size
                if (statuses == subItemList[chunkIndex][index].SubSection.size) {
                    if(statuses == validatedCount){
                        validated = true
                    }else{
                        message = "Please mark the checkpoints in subsections"
                        validated = false
                    }
//                    uploadSectionData(subItemList[chunkIndex][index])
                } else {
                    message = "All fields are mandatory"
                    validated = false
                }
            }
        }
        if (validated) {
            if (chunkIndex < subItemList.size - 1) {
                if (index < subItemList[chunkIndex].size - 1) {
                    view?.bindTab(subItemList[chunkIndex], index + 1)
                } else {
                    ++chunkIndex
                    view?.bindViewPager(subItemList[chunkIndex])
                    view?.bindTab(subItemList[chunkIndex], 0)
                }
            } else if (index < subItemList[chunkIndex].size - 1) {
                view?.bindTab(subItemList[chunkIndex], index + 1)
            } else {
                view?.showLoading()
                for (item in itemList) {
                    item.SubSection?.apply {
                        if (size > 0) {
                            val greens = filter { s ->
                                s.Status == SectionStatusEnum.GREEN.getStatusId()
                            }
                            val yellows = filter { s ->
                                s.Status == SectionStatusEnum.YELLOW.getStatusId()
                            }
                            val reds = filter { s ->
                                s.Status == SectionStatusEnum.RED.getStatusId()
                            }

                            val greenScore = greens.size * 2
                            val yellowScore = yellows.size * 1
                            val redScore = reds.size * 0

                            item.Score = ((greenScore + yellowScore + redScore) * 100) / (size * 2)

                            if(item.Score < 80){
                                item.ScoreEnum = SectionStatusEnum.RED
                            }else if(item.Score in 80..90){
                                item.ScoreEnum = SectionStatusEnum.YELLOW
                            }else{
                                item.ScoreEnum = SectionStatusEnum.GREEN
                            }
                        }
                    }
                    when(item.SectionName){
                        Constants.SECTION_SCRATCH_DENT_AND_DAMAGE ->{
                            uploadScratchAndDamageData(item)
                        }
                        Constants.SECTION_ACKNOWLEDGE->{
                            uploadAcknowledgeData(item)
                        }
                        else->{
                            uploadSectionData(item)
                        }
                    }
                }
//                view?.hideLoading()
//                router?.navigateVehicleHealthCheckToVehicleHealthReport(itemList)
            }
        } else {
            view.showValidationMessage(message)
        }
    }

    override fun getHealthCheckList() {
        view?.bindViewPager(subItemList[chunkIndex])
    }

    override fun uploadPhotoClicked(sectionIndex: Int, subSectionIndex: Int) {
        photoSectionIndex = sectionIndex
        photoSubSectionIndex = subSectionIndex
        router?.openCameraToTakePhoto()
    }

    override fun processCameraResult(imagePath: String) {
        view?.showLoading()
        ImageCompression(object : ImageCompression.FileCompressed {
            override fun onSuccess(path: String) {
                when (subItemList[chunkIndex][photoSectionIndex].SubSection[photoSubSectionIndex].Assets) {
                    null -> {
                        subItemList[chunkIndex][photoSectionIndex].SubSection[photoSubSectionIndex].Assets =
                            ArrayList()
                        subItemList[chunkIndex][photoSectionIndex].SubSection[photoSubSectionIndex].Assets?.add(
                            path
                        )
                    }
                    else -> {
                        subItemList[chunkIndex][photoSectionIndex].SubSection[photoSubSectionIndex].Assets?.add(
                            path
                        )
                    }
                }
                view?.hideLoading()
                view?.updateImageInSubSection(photoSectionIndex, photoSubSectionIndex)
            }
        }).execute(imagePath)
    }
    //endregion

    //Region Private functions
    /**
     * This function will upload the scratch and damage section data
     * @param section: VehicleHealthCheckUIModelItem (Scratch/Dent/Damage section data and marked images)
     */
    private fun uploadScratchAndDamageData(section: VehicleHealthCheckUIModelItem) {
        Thread {
            val vhcDataList = ArrayList<CreateVHCDataInput>()
            for (s in section.ScratchAndDamage) {
                val vhcData = CreateVHCDataInput(
                    s.angle,
                    "",
                    section.SectionName,
                    0,
                    Gson().toJson(s.markers),
                    router?.getVehicleChecckUpId()
                )
                vhcDataList.add(vhcData)
            }
            vhcDataList.add(
                CreateVHCDataInput(
                    context.getString(R.string.text_cleaning_remark),
                    "",
                    section.SectionName,
                    0,
                    section.CleaningRemark ?: "",
                    router?.getVehicleChecckUpId()
                )
            )

            vhcDataList.add(
                CreateVHCDataInput(
                    context.getString(R.string.text_damage_remark),
                    "",
                    section.SectionName,
                    0,
                    section.DamageRemark ?: "",
                    router?.getVehicleChecckUpId()
                )
            )
            vhcDataList.add(
                CreateVHCDataInput(
                    context.getString(R.string.text_other_offers_and_notes),
                    "",
                    section.SectionName,
                    0,
                    section.OffersAndNotes ?: "",
                    router?.getVehicleChecckUpId()
                )
            )
            VfCalls(context).createVHCDataArray(
                CreateVHCDataArrayInput(
                    vhcDataList,
                    router.getVehicleChecckUpId()
                ), object : VfCalls.OnResponse<CreateVHCDataArrayResponse> {
                    override fun onSucess(response: CreateVHCDataArrayResponse) {
                        Logger.log("Scratch And Damage :: ", "Success")
                    }

                    override fun onFailure(message: String) {
                        Logger.log("Scratch And Damage :: ", message)
                    }
                })

        }.start()
    }

    /**
     * This function will upload the Acknowledge section data
     * @param sectionAcknowledgeData: VehicleHealthCheckUIModelItem (Includes Customer and SA Signatures)
     */
    private fun uploadAcknowledgeData(sectionAcknowledgeData: VehicleHealthCheckUIModelItem) {
        view.showLoading()
        Thread {
            val vhcDataList = ArrayList<CreateVHCDataInput>()
            vhcDataList.add(
                CreateVHCDataInput(
                    "SA Signature",
                    "",
                    sectionAcknowledgeData.SectionName,
                    0,
                    "",
                    router?.getVehicleChecckUpId()
                )
            )
            vhcDataList.add(
                CreateVHCDataInput(
                    "Customer Signature",
                    "",
                    sectionAcknowledgeData.SectionName,
                    0,
                    "",
                    router?.getVehicleChecckUpId()
                )
            )

            VfCalls(context).createVHCDataArray(
                CreateVHCDataArrayInput(
                    vhcDataList,
                    router.getVehicleChecckUpId()
                ), object : VfCalls.OnResponse<CreateVHCDataArrayResponse> {
                    override fun onSucess(response: CreateVHCDataArrayResponse) {
                        Thread {
                            val sa =
                                response.CheckupData.filter { r -> r.Name.equals("SA Signature") }
                            val customer =
                                response.CheckupData.filter { c -> c.Name.equals("Customer Signature") }

                            if (sa.isNotEmpty()) {
                                sectionAcknowledgeData.SaSignature?.let { signature ->
                                    val input = CreateVHCDataAssetInput(
                                        CommonUtils.getFileName(signature),
                                        CommonUtils.convertBase64(signature),
                                        true,
                                        sa[0].VehicleCheckupDataId
                                    )
                                    VfCalls(context).createVHCDataAsset(
                                        input,
                                        object : VfCalls.OnResponse<CreateVHCDataAssetResponse> {
                                            override fun onSucess(response: CreateVHCDataAssetResponse) {
                                                Logger.log("SA Signature :: ", "Success")
                                            }

                                            override fun onFailure(message: String) {
                                                Logger.log("SA Signature :: ", message)
                                            }
                                        })
                                }
                            }

                            if (customer.isNotEmpty()) {
                                sectionAcknowledgeData.CustomerSignature?.let { signature ->
                                    val input = CreateVHCDataAssetInput(
                                        CommonUtils.getFileName(signature),
                                        CommonUtils.convertBase64(signature),
                                        true,
                                        customer[0].VehicleCheckupDataId
                                    )
                                    VfCalls(context).createVHCDataAsset(
                                        input,
                                        object : VfCalls.OnResponse<CreateVHCDataAssetResponse> {
                                            override fun onSucess(response: CreateVHCDataAssetResponse) {
                                                Logger.log("Customer Signature :: ", "Success")
                                                view.hideLoading()
                                                router.navigateVehicleHealthCheckToVehicleHealthReport(itemList)
                                            }

                                            override fun onFailure(message: String) {
                                                Logger.log("Customer Signature :: ", message)
                                            }
                                        })
                                }
                            }

                        }.start()
                    }

                    override fun onFailure(message: String) {
                        Logger.log("CreateDataArray :: ", message)
                    }
                })
        }.start()
    }

    /**
     * This function will upload the subsection health check data
     * @param vehicleHealthCheckUIModelItem: VehicleHealthCheckUIModelItem
     */
    private fun uploadSectionData(vehicleHealthCheckUIModelItem: VehicleHealthCheckUIModelItem) {
        Thread {
            val vhcDataList = ArrayList<CreateVHCDataInput>()
            for (subSection in vehicleHealthCheckUIModelItem.SubSection) {
                val vhcData = CreateVHCDataInput(
                    subSection.Name,
                    subSection.Remarks,
                    vehicleHealthCheckUIModelItem.SectionName,
                    subSection.Status,
                    subSection.Value,
                    router.getVehicleChecckUpId()
                )
                vhcDataList.add(vhcData)
            }

            VfCalls(context).createVHCDataArray(
                CreateVHCDataArrayInput(
                    vhcDataList,
                    router.getVehicleChecckUpId()
                ), object : VfCalls.OnResponse<CreateVHCDataArrayResponse> {
                    override fun onSucess(response: CreateVHCDataArrayResponse) {
                        Thread {
                            val item = itemList.filter { i ->
                                i.SectionName.equals(vehicleHealthCheckUIModelItem.SectionName)
                            }
                            if (item.isNotEmpty()) {
                                for (c in response.CheckupData) {
                                    for (i in item[0].SubSection) {
                                        if (c.Name.equals(i.Name)) {
                                            i.VehicleChekUpDataId = c.VehicleCheckupDataId
                                            break

                                        }
                                    }
                                }
                                uploadAssets(item[0])
                            }
                        }.start()
                    }

                    override fun onFailure(message: String) {
                        Logger.log("CreateDataArray :: ", message)
                    }
                })

        }.start()
    }

    /**
     * This function will upload the assets clicked in subsections
     * @param vehicleHealthCheckUIModelItem: VehicleHealthCheckUIModelItem
     */
    private fun uploadAssets(vehicleHealthCheckUIModelItem: VehicleHealthCheckUIModelItem) {
        val subSection =
            vehicleHealthCheckUIModelItem.SubSection.filter { s -> s.Assets?.isNotEmpty() ?: false }
        if (subSection.isNotEmpty()) {
            for (s in subSection) {


                for (a in s.Assets!!) {
                    Thread {
                        val input = CreateVHCDataAssetInput(
                            CommonUtils.getFileName(a),
                            CommonUtils.convertBase64(a),
                            true,
                            s.VehicleChekUpDataId
                        )
                        VfCalls(context).createVHCDataAsset(
                            input,
                            object : VfCalls.OnResponse<CreateVHCDataAssetResponse> {
                                override fun onSucess(response: CreateVHCDataAssetResponse) {
                                    Logger.log("Asset Upload :: ", "Success")
                                }

                                override fun onFailure(message: String) {
                                    Logger.log("Asset Upload :: ", message)
                                }
                            })
                    }.start()
                }
            }
        }
    }
    //endregion
}