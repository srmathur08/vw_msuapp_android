package com.vectorform.msu.ui.home.servicedetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.vectorform.msu.databinding.FragmentServiceDetailsBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.BaseFragment

class ServiceDetailsFragment : BaseFragment(), ServiceDetailsContract.IServiceDetailsFragment,
    View.OnClickListener {

    private var binding: FragmentServiceDetailsBinding? = null
    private var presenter: ServiceDetailsContract.IServiceDetailsPresenter? = null
    private val args: ServiceDetailsFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = ServiceDetailsPresenter(safeContext, this, activity as HomeActivity, args)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentServiceDetailsBinding.inflate(inflater, container, false)
        presenter?.onViewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageSDBack.setOnClickListener(this@ServiceDetailsFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageSDBack -> {
                    presenter?.onBackClicked()
                }
            }
        }
    }
    //endregion

    //Region IServiceDetailsFragment
    override fun updateServiceCompletedDate(date: String) {
        binding?.apply {
            textSDDateTime.text = date
            textSDRoServiceCompletedDate.text = date
        }
    }

    override fun updateServiceDate(date: String) {
        binding?.apply {
            textSDServiceDate.text = date
        }
    }

    override fun updateRepairOrderNo(repairOrderNo: String) {
        binding?.apply {
            textSDRepaiOrderNo.text = repairOrderNo
        }
    }

    override fun updateRoType(roType: String) {
        binding?.apply {
            textSDRoType.text = roType
        }
    }

    override fun updateRoServiceType(roServiceType: String) {
        binding?.apply {
            textSDRoServiceType.text = roServiceType
        }
    }

    override fun updateServiceEngineer(name: String) {
        binding?.apply {
            textSDServiceEngineer.text = name
        }
    }

    override fun updateServiceInstruction(instructions: String) {
        binding?.apply {
            textSDServiceInstructionsValue.text = instructions
        }
    }

    override fun updateSAInstruction(instructions: String) {
        /* binding?.apply {
             textSDServiceAdvisorInstructionsVa.text = instructions
         }*/
    }

    override fun updateInvoice(amount: String) {
        binding?.apply {
            textSDInvoiceAmount.text = amount
        }
    }

    override fun updateRatings(rating: String) {
        binding?.apply {
            textSHDRating.text = rating
        }
    }

    override fun hideRatingFields() {
        binding?.apply {
            textSHDRating.visibility = View.GONE
            imageSHIRating.visibility = View.GONE
        }
    }
    //endregion
}