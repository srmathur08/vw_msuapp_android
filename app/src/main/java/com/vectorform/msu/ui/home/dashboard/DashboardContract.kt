package com.vectorform.msu.ui.home.dashboard

import com.vectorform.msu.data.responses.vf.DashboardCounts
import com.vectorform.msu.data.responses.vf.LoginResponse
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.utils.BaseFragmentContract

interface DashboardContract : BaseFragmentContract.IBaseFragment {

    interface IDashboardFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will bind the Upcoming Services in UI
         * @param services : List<SRsByDateResponse.ServiceRequest>
         */
        fun bindServiceRecycler(services: List<SRsByDateResponse.ServiceRequest>)

        /**
         * This function will reset the transition for Add button click
         */
        fun resetAddTransition()

        /**
         * This function will update the logged in user details in UI
         * @param loginModel : LoginResponse
         */
        fun showUsername(loginModel: LoginResponse)

        /**
         * This function will show No Data text if there is no Upcoming service
         */
        fun showNoData()

        /**
         * This function will hide No Data text if any upcoming service is there
         */
        fun hideNoData()

        /**
         * This function will bind the 'In Progress Service' card and enable the scroll transition
         * @param wip: SRsByDateResponse.ServiceRequest
         */
        fun enableWorkInProgress(wip: SRsByDateResponse.ServiceRequest)

        /**
         * This function will hide the work in progress card from UI and disable the scroll transition
         */
        fun disableWorInProgress()

        /**
         * This function will update the date field text in UI
         * @param date : String
         */
        fun updateDate(date: String)

        /**
         * This function will clear the service recycler view
         */
        fun emptyServiceRecycler()

        fun showUserProfilePic(picPath : String?)
        fun showAddIcon()
        fun hideAddIcon()
        fun highlightToday(counts: DashboardCounts?)
        fun highlightMTD(counts: DashboardCounts?)
    }

    interface IDashBoardPresenter {
        /**
         * This function will fetch the Service Requests List from API
         */
        fun fetchServiceList()

        /**
         * This function will be invoked when user clicks on In Progress Service Card
         */
        fun onUpdateStatusClicked()

        /**
         * This function will handle the Create New Service Request button click
         */
        fun onServiceRequestClicked()

        /**
         * This function will handle the VehicleHealthCheck button click
         */
        fun onVehicleHealthCheckClicked()

        /**
         * This function will handle the click on the blank screen to reset the Add Button transition
         */
        fun onBackgroundImageClicked()

        /**
         * This function will handle the click of Upcoming Service Card
         * @param serviceData : SRsByDateResponse.ServiceRequest
         */
        fun onServiceRequestItemClicked(serviceData: SRsByDateResponse.ServiceRequest)

        /**
         * This function will handle the User Profile click
         */
        fun onProfileClicked()
        fun checkInprogressWIP()
        fun onTodayClicked()
        fun onMTDClicked()
    }
}