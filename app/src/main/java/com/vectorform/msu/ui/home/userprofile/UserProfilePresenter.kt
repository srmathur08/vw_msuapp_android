package com.vectorform.msu.ui.home.userprofile

import android.content.Context
import android.provider.SyncStateContract
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.utils.CommonUtils
import com.google.gson.Gson
import com.vectorform.msu.R
import com.vectorform.msu.data.input.ProfilePicUploadInput
import com.vectorform.msu.data.responses.vf.LoginResponse
import com.vectorform.msu.data.responses.vf.ProfilePicUploadResponse
import com.vectorform.msu.ui.home.HomeContract
import com.vectorform.msu.utils.Constants

class UserProfilePresenter(
    private val context: Context,
    private val view: UserProfileContract.IUserProfileFragment,
    private val router: HomeContract.HomeRouter
) : UserProfileContract.IUserProfilePresenter {

    private var isChooserOpen : Boolean = false

    //Region IUserProfilePresenter
    override fun OnViewCreated() {
        val loginJson = CommonUtils.getString(context, Constants.LOGIN_RESPONSE)
        loginJson?.let { json->
            val loginResponse = Gson().fromJson(json, LoginResponse::class.java)
            loginResponse?.let{response->
                view?.bindUserData(response, CommonUtils.getString(context, Constants.USER_MOBILE_NUMBER))
                view?.showProfileImage(Constants.BASEURL + response.ProfilePicUrl?.replace("~", ""))
            }
        }
    }

    override fun onBackClicked() {
        router?.popBack()
    }

    override fun onLogoutClicked() {
        CommonUtils.clearPrefs(context).also { router?.navigateToLogin() }
    }

    override fun onProifleIconClicked() {
       when(isChooserOpen){
           true->{
               isChooserOpen = false
               view?.hideChooser()
           }
           false->{
               isChooserOpen = true
               view?.showChooser()
           }
       }
    }

    override fun onOpenCamera() {
        onProifleIconClicked()
        router?.openCameraToTakePhoto()
    }

    override fun onOpenGallery() {
        onProifleIconClicked()
        router?.openGalleryToChoosePhoto()
    }

    override fun processCameraResult(cameraImagePath: String) {
       view?.showLoading()
        val userId = CommonUtils.getString(context, Constants.LOGGED_IN_USER_ID)
        val splitpath = cameraImagePath.split("/")
        val filename = splitpath[splitpath.size - 1]
        val base64 = CommonUtils.convertBase64(cameraImagePath)

        val input = ProfilePicUploadInput().apply {
            UserId = userId
            ImageFileName = filename
            ImageBase64String = base64
        }

        VfCalls(context).UploadProfilePic(input, object : VfCalls.OnResponse<ProfilePicUploadResponse>{
            override fun onSucess(response: ProfilePicUploadResponse) {
                view?.hideLoading()
                response.ProfilePicUrl?.let { url->
                    CommonUtils.saveString(context, Constants.PROFILE_PIC_URL, url)
                    view?.showProfileImage(Constants.BASEURL + url.replace("~", ""))
                }
            }

            override fun onFailure(message: String) {
                view?.hideLoading()
                view?.showCustomMessage(R.string.alert, message, null)
            }
        })
    }

    override fun onNotificationIconClicked() {
        router?.navigateUserProfileToNotifications()
    }
    //endregion


}