package com.vectorform.msu.ui.login

/**
 * Contract between LoginMain Presenter and View
 */
interface LoginMainContract {

    /**
     * Contract to update view from presenter
     */
    interface ILoginMainActivity {
    }

    /**
     * Contract to update presenter from view
     */
    interface ILoginMainPresenter {
    }

    /**
     * his tells the router to handle view events
     */
    interface LoginMainRouter {
        /**
         * This function will navigate users to HomeActivity page
         */
        fun navigateToHome()
    }

}