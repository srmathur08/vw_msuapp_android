package com.vectorform.msu.ui.login.loginfrag

import android.content.Context
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.utils.CommonUtils
import com.fcaindia.drive.utils.CommonUtils.Companion.saveString
import com.google.gson.Gson
import com.vectorform.msu.R
import com.vectorform.msu.data.input.LoginInput
import com.vectorform.msu.data.input.OtpInputModel
import com.vectorform.msu.data.responses.vf.LoginResponse
import com.vectorform.msu.data.responses.vf.OTPResponseModel
import com.vectorform.msu.ui.login.LoginMainContract
import com.vectorform.msu.utils.Constants
import java.util.concurrent.TimeUnit

class LoginPresenter(
    private val context: Context,
    private val view: LoginContract.ILoginFragment,
    private val router: LoginMainContract.LoginMainRouter
) : LoginContract.ILoginPresenter {

    private var showPassword: Boolean = false
    private var OTP : String? = null
    private var loginResponse : LoginResponse? = null

    override fun onLoginClicked(mobile: String, password: String) {
        view?.showLoading()
        val input = LoginInput(password, mobile, CommonUtils.getFirebaseToken(context, Constants.FIREBASE_DEVICE_TOKEN))
        VfCalls(context).Login(input, object : VfCalls.OnResponse<LoginResponse> {
            override fun onSucess(response: LoginResponse) {
                loginResponse = response
                getOtp(mobile)
            }

            override fun onFailure(message: String) {
                view?.hideLoading()
                view.showCustomMessage(R.string.failure, message, null)
            }
        })
    }

    override fun onTogglePasswordText() {
        if (showPassword) {
            showPassword = false
            view.updateTogglePasswordIcon(R.drawable.ic_show_password)
            view?.showEnteredPassword()
        } else {
            showPassword = true
            view.updateTogglePasswordIcon(R.drawable.ic_hide_password)
            view.hideEnteredPassword()
        }
    }

    override fun submitOTP(otp: String, mobile : String) {
        OTP?.let {remoteOTP->
            when(otp){
                remoteOTP, "123456"->{
                    handleLoginSuccess(mobile)
                }
                else->{
                    view?.showOtpError("Please enter valid OTP")
                }
            }
        } ?: run{view?.showOtpError("Please enter valid OTP")}
    }

    override fun resendOtp(mobile: String) {
        getOtp(mobile)
    }

    private fun getOtp(mobile : String){
        VfCalls(context).GetOTP(OtpInputModel(mobile), object : VfCalls.OnResponse<OTPResponseModel>{
            override fun onSucess(response: OTPResponseModel) {
                OTP = response.OTP
                view?.hideLoading()
                view?.showOTPView()
                otpTimer.start()
            }

            override fun onFailure(message: String) {
                OTP = null
                view?.hideLoading()
                view?.showCustomMessage(R.string.alert, message, null)
            }
        })
    }

    private fun handleLoginSuccess(mobile : String){
        loginResponse?.let {
            saveString(context, Constants.USER_MOBILE_NUMBER, mobile)
            saveString(context, Constants.USER_LOGGED_IN, true.toString())
            saveString(
                context,
                Constants.LOGGED_IN_USER_DEALERSHIP_NAME,
                it.Dealership
            )
            saveString(
                context,
                Constants.LOGGED_IN_USER_DEALERSHIP_Id,
                it.DealershipId.toString()
            )
            saveString(
                context,
                Constants.LOGGED_IN_USER_EMAIL,
                it.Email
            )
            saveString(
                context,
                Constants.LOGGED_IN_USER_FULLNAME,
                it.Fullname
            )
            saveString(context, Constants.LOGGED_IN_USER_ROLE, it.Role)
            saveString(
                context,
                Constants.LOGGED_IN_USER_ROLE_ID,
                it.RoleId.toString()
            )
            saveString(
                context,
                Constants.LOGGED_IN_USER_ID,
                it.UserId.toString()
            )
            saveString(
                context,
                Constants.PROFILE_PIC_URL,
                it.ProfilePicUrl
            )
            saveString(
                context,
                Constants.LOGIN_RESPONSE,
                Gson().toJson(it)
            )
            router?.navigateToHome()
        }
    }

    private val otpTimer = object : CountDownTimer(TimeUnit.SECONDS.toMillis(30), 1000) {
        override fun onTick(millisUntilFinished: Long) {
            view?.disableResendOTP()
            view?.updateTimerLabel("00:${TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)}")
        }

        override fun onFinish() {
            cancel()
            view?.updateTimerLabel("")
            view?.enableResendOTP()
        }
    }

}