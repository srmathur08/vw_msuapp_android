package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.acknowledge

import android.content.Context
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem

class AcknowledgePagerItemPresenter(
    private val context: Context, private val dataItem: VehicleHealthCheckUIModelItem,
    private val view: AcknowledgePagerItemContract.IAcknowledgePagerItemFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter
) : AcknowledgePagerItemContract.IAcknowledgePagerItemPresenter {

    //Region IAcknowledgePagerItemPresenter
    override fun viewCreated() {
        view?.updateTitle(dataItem.SectionName)
        view?.initSignatureView()
        dataItem.SaSignature?.let { view?.setSaSignature(it) }
        dataItem.CustomerSignature?.let { view?.setCustomerSignature(it) }
    }

    override fun onSaSignatureClicked() {
        view?.captureSaSignature(dataItem.SaSignature)
    }

    override fun onCustomerSignatureClicked() {
        view?.captureCustomerSignature(dataItem.CustomerSignature)
    }

    override fun updateSignature(signature: String, isCustomer: Boolean) {

        when (isCustomer) {
            true -> {
                dataItem.CustomerSignature = signature
                view?.setCustomerSignature(signature)
            }
            false -> {
                dataItem.SaSignature = signature
                view?.setSaSignature(signature)
            }
        }
    }

    override fun clearSaSignature() {
        dataItem.SaSignature = null
        view?.clearSaSignature()
    }

    override fun clearCustomerSignature() {
        dataItem?.CustomerSignature = null
        view?.clearCustomerSignature()
    }
    //endregion


}