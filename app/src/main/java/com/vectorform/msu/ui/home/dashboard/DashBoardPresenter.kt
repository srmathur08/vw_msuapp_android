package com.vectorform.msu.ui.home.dashboard

import android.content.Context
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.utils.CommonUtils
import com.google.gson.Gson
import com.vectorform.msu.R
import com.vectorform.msu.data.input.DashboardCountInput
import com.vectorform.msu.data.input.SRsByDateInput
import com.vectorform.msu.data.input.SrStatusInput
import com.vectorform.msu.data.responses.vf.*
import com.vectorform.msu.ui.home.HomeContract
import com.vectorform.msu.utils.Constants
import java.text.SimpleDateFormat
import java.util.*

class DashBoardPresenter(
    private val context: Context,
    private val view: DashboardContract.IDashboardFragment,
    private val router: HomeContract.HomeRouter
) : DashboardContract.IDashBoardPresenter {

    private var inProgressService: SRsByDateResponse.ServiceRequest? = null
    private var countsResponse: DashboardCounts? = null
    private var isToday = true

    //Region IDashboardFragment
    override fun fetchServiceList() {

        view?.showLoading()

        val loginJson = CommonUtils.getString(context, Constants.LOGIN_RESPONSE)
        val loginModel = Gson().fromJson(loginJson, LoginResponse::class.java)
        val profilePicUrl = CommonUtils.getString(context, Constants.PROFILE_PIC_URL)
        view?.showUsername(loginModel)
        view?.showUserProfilePic(Constants.BASEURL + profilePicUrl?.replace("~", ""))
        view?.disableWorInProgress()
        view?.updateDate(SimpleDateFormat("EEEE, MMM dd").format(Date().time))

        val date = SimpleDateFormat("dd/MM/yyyy").format(Date().time)
        val dealershipId = CommonUtils.getString(context, Constants.LOGGED_IN_USER_DEALERSHIP_Id)
        val asssigneId = CommonUtils.getString(context, Constants.LOGGED_IN_USER_ID)

        getDashboarCounts(asssigneId)

        view?.emptyServiceRecycler()

        VfCalls(context).getServicesByDate(
            SRsByDateInput(dealershipId, date, asssigneId),
            object : VfCalls.OnResponse<SRsByDateResponse> {
                override fun onSucess(response: SRsByDateResponse) {
                    val inProgress = response.ServiceRequests.filter { s -> s.IsStarted }
                    if (inProgress.isNotEmpty()) {
                        inProgressService = inProgress[0]
                        inProgressService?.let { wip ->
                            (response.ServiceRequests as MutableList).removeAll { s -> s.Srid == wip.Srid }
                            view.enableWorkInProgress(wip)
                        }
                    }else{
                        view?.hideAddIcon()
                    }
                    if (response.ServiceRequests.isNotEmpty()) {
                        view?.bindServiceRecycler(response.ServiceRequests)
                        view?.hideNoData()
                    } else {
                        view?.showNoData()
                    }
                    view?.hideLoading()
                }

                override fun onFailure(message: String) {
                    view?.showNoData()
                    view?.hideLoading()
                    view?.showCustomMessage(R.string.failure, message, null)
                    view?.hideAddIcon()
                }
            })
    }

    override fun onUpdateStatusClicked() {
        inProgressService?.let { wip ->
            router?.navigateDashboardToWorkInProgressDirection(wip)
        }
    }

    override fun onServiceRequestClicked() {
        view?.resetAddTransition().also {
            router?.navigateDashBoardToCreateServiceRequest()
        }
    }

    override fun onVehicleHealthCheckClicked() {
        view?.resetAddTransition().also {
            inProgressService?.let { wip ->
                when (wip.IsVCCompleted) {
                    true -> {
                        router?.navigateDashboardToWorkInProgressDirection(wip)
                    }
                    else -> {
                        router?.navigateDashBoardToFirstVehicleHealthCheck(wip)
                    }
                }
            } ?: run {
                view.showCustomMessage(R.string.alert, "No In Progress Service", null)
            }
        }
    }

    override fun onBackgroundImageClicked() {
        view?.resetAddTransition()
    }

    override fun onServiceRequestItemClicked(serviceData: SRsByDateResponse.ServiceRequest) {
        view?.showLoading()
        val input = SrStatusInput(serviceData.Srid.toString(), Constants.GET_DIRECTIONS_CLICKED_STATUS, "", "")
        VfCalls(context).updateSrStatus(input, object : VfCalls.OnResponse<SRMarkResponse>{
            override fun onSucess(response: SRMarkResponse) {
                view?.hideLoading()
                router?.navigateDashboardToWorkInProgressDirection(serviceData)
            }

            override fun onFailure(message: String) {
                view?.hideLoading()
                view?.showCustomMessage(R.string.alert, message, null)
            }

        })
    }

    override fun onProfileClicked() {
        router?.navigateDashboardToUserProfile()
    }

    override fun checkInprogressWIP() {
        inProgressService?.let {
            view?.showAddIcon()
        } ?: run{
            view?.hideAddIcon()
        }
    }

    override fun onTodayClicked() {
        isToday = true
        view?.highlightToday(countsResponse)
    }

    override fun onMTDClicked() {
        isToday = false
        view?.highlightMTD(countsResponse)
    }
    //end region


    private fun getDashboarCounts(assignee : String){
        val input = DashboardCountInput(assignee)
        VfCalls(context).GetDashboardCounts(input, object : VfCalls.OnResponse<DashboardCountResponse>{
            override fun onSucess(response: DashboardCountResponse) {
                response?.serviceRequests?.let { counts->
                    countsResponse = counts
                    when(isToday){
                        true->{
                            view?.highlightToday(counts)
                        }
                        false->{
                            view?.highlightMTD(counts)
                        }
                    }
                } ?: run{
                    countsResponse = null
                }
            }

            override fun onFailure(message: String) {
                countsResponse = null
            }
        })
    }
}