package com.vectorform.msu.ui.vehiclehealthcheck.vehiclehealthreport

import androidx.annotation.StringRes

data class VehicleHealthReportDescriptionModel(@StringRes val key: Int, val value: String?)
