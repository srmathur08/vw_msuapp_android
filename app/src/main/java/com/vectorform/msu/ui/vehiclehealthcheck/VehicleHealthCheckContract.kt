package com.vectorform.msu.ui.vehiclehealthcheck

import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.ui.vehiclehealthcheck.fullimage.FullImageModel
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem

interface VehicleHealthCheckContract {

    interface IVehicleHealthCheckActivity {

    }

    /**
     * Contract to update presenter from view
     */
    interface IVehicleHealthCheckPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will update the vehicle checkUp Id
         * @param vehicleCheckUpId : Int
         */
        fun setVehicleChecpId(vehicleCheckUpId: Int)

        /**
         * This function will update fuel level
         * @param fuelLevelPercent : String
         */
        fun setFuelLevel(fuelLevelPercent: String)

        /**
         * This function will return vehicle check-up id
         * @return vehicleCheckUpId : Int
         */
        fun getVehicleChecckUpId(): Int

        /**
         * This function will return vehicle fuel level
         * @return vehicleFuelLevel : String
         */
        fun getFuelLevel(): String
    }

    /**
     * This tells the router to handle view events
     */
    interface VehicleHealthCheckRouter {
        /**
         * This function will navigate users to previous screen
         */
        fun popBack()

        /**
         * This function will close the activity and navigate user to parent activity
         */
        fun closeActivity()

        /**
         * This function will navigate user from First Vehicle Health Check screen to Vehicle Assets Screen
         * Also it will update vehicleCheckUpId and FuelLevel percent in activity to be accessed fro child
         * @param vehicleCheckupId : Int
         * @param fuelLevelPercent : Int
         */
        fun navigateFirstVehicleHealthCheckToVehicleAsets(
            vehicleCheckupId: Int,
            fuelLevelPercent: Int
        )

        /**
         * This function will navigate users from VehicleAssets Screen to SecondVehicleHealth Check screen
         */
        fun navigateVehicleAssetsToSecondVehicleHealthCheck()

        /**
         * This function will navigate user from Second Vehicle Health Check to Vehicle Health Report Screen
         * @param itemList: ArrayList<VehicleHealthCheckUIModelItem> (list of vehicle health check sections)
         */
        fun navigateVehicleHealthCheckToVehicleHealthReport(itemList: ArrayList<VehicleHealthCheckUIModelItem>)

        /**
         * This function will open camera to capture image
         */
        fun openCameraToTakePhoto()

        /**
         * This function will navigate users from Vehicle Health Report to Vehicle Health Report Details screen
         * @param sectionName: VehicleHealthCheckUIModelItem
         */
        fun navigateReportsToDetailedReport(sectionName: VehicleHealthCheckUIModelItem)

        /**
         * This function will navigate users from Vehicle Health Report to Vehicle Images screen
         * @param scratchAndDamage: VehicleHealthCheckUIModelItem (Scratch/Dent/Damage)
         * @param acknowledge : VehicleHealthCheckUIModelItem (SA and Customer Signature)
         */
        fun navigateReportsToVehicleImages(
            scratchAndDamage: VehicleHealthCheckUIModelItem,
            acknowledge: VehicleHealthCheckUIModelItem
        )

        /**
         * This function will navigate users to Full Screen Image view
         * @param fullImageModel : FullImageModel (contains list of images and default selection index)
         */
        fun navigateToFullImageView(fullImageModel: FullImageModel)

        /**
         * This function will return the In Progress Service Instance
         * @return SRsByDateResponse.ServiceRequest?
         */
        fun getServiceRequestMasterData(): SRsByDateResponse.ServiceRequest?


        /**
         * This function will return the Vehicle Check-up Id
         * @return Int
         */
        fun getVehicleChecckUpId(): Int

        /**
         * This function will return the Vehicle Fuel Level
         * @return String
         */
        fun getFuelLevel(): String
    }

}