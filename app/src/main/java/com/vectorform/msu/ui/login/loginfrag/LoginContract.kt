package com.vectorform.msu.ui.login.loginfrag

import com.vectorform.msu.utils.BaseFragmentContract

interface LoginContract {

    interface ILoginFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will show error in password if any
         * @param error String message
         */
        fun showPasswordError(error: String)

        /**
         * This function will update the password text toggle icon
         * @param imageId Int drawable resource id
         */
        fun updateTogglePasswordIcon(imageId: Int)

        /**
         * This function will show entered password
         */
        fun showEnteredPassword()

        /**
         * This function will hide entered password
         */
        fun hideEnteredPassword()

        fun showOtpError(otpErrorMessage : String)

        fun showOTPView()

        fun enableResendOTP()
        fun disableResendOTP()
        fun updateTimerLabel(label : String)

    }

    interface ILoginPresenter {
        /**
         * This function will handle login button click
         * @param mobile String
         * @param password String
         */
        fun onLoginClicked(mobile: String, password: String)

        /**
         * This function will toggle visibility of entered password
         */
        fun onTogglePasswordText()

        fun submitOTP(otp : String, mobile : String)
        fun resendOtp(mobile : String)
    }

}