package com.vectorform.msu.ui.home.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.databinding.ServiceRecylcerItemBinding
import com.vectorform.msu.utils.Constants

class DashboardServiceRecyclerAdapter(
    val context: Context,
    var serviceList: List<SRsByDateResponse.ServiceRequest>,
    val itemClickListener: IDashBoardServiceRecylcerAdapter
) : RecyclerView.Adapter<DashboardServiceRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ServiceRecylcerItemBinding =
            ServiceRecylcerItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return serviceList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ServiceRecylcerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding?.apply {
                Glide.with(context).load(Constants.BASEURL + serviceList[position].VehicleImage)
                    .diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(R.color.app_bg_color)
                    .into(imageItemVehicle)
                textItemServiceCardTitle.text = serviceList[position].CustomerName
                textItemVehicleNumber.text = serviceList[position].RegNum
                textItemCardDistance.text = serviceList[position].OdometerReading
                textItemServiceType.text = serviceList[position].ServiceType

                root.setOnClickListener {
                    itemClickListener.onServiceItemClicked(serviceList[position])
                }
            }
        }
    }
}