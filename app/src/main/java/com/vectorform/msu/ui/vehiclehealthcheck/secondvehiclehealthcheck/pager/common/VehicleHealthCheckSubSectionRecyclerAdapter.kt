package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.common

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vectorform.msu.R
import com.vectorform.msu.databinding.VehicleHealthCheckSubSectionItemBinding
import com.vectorform.msu.ui.vehiclehealthcheck.IVehicleImageClicked
import com.vectorform.msu.ui.vehiclehealthcheck.PhotoFieldRecyclerAdapter
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.SubSection
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.SectionStatusEnum

class VehicleHealthCheckSubSectionRecyclerAdapter(
    val context: Context, var itemList: List<SubSection>,
    val listener: IVehicleHealthCheckSubSectionRecyclerAdapter
) : RecyclerView.Adapter<VehicleHealthCheckSubSectionRecyclerAdapter.ViewHolder>(),
    IVehicleImageClicked {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: VehicleHealthCheckSubSectionItemBinding =
            VehicleHealthCheckSubSectionItemBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun setHasStableIds(hasStableIds: Boolean) {
        super.setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: VehicleHealthCheckSubSectionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.apply {
                textCheckSubSection.text = itemList[position].Name

                itemList[position].Assets?.let { assets ->
                    if (assets.isNotEmpty()) {
                        val adapter = PhotoFieldRecyclerAdapter(
                            context,
                            assets,
                            this@VehicleHealthCheckSubSectionRecyclerAdapter
                        )
                        recyclerCheckPhoto.layoutManager =
                            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        recyclerCheckPhoto.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }
                }

                when (itemList[position].Status) {
                    SectionStatusEnum.GREEN.getStatusId() -> {
                        imageCheckGreen.setImageResource(R.drawable.ic_checked_green)
                        imageCheckYellow.setImageResource(R.drawable.ic_unchecked_yellow)
                        imageCheckRed.setImageResource(R.drawable.ic_unchecked_red)
                        layoutCheckOptionContainer.visibility = View.GONE
                    }
                    SectionStatusEnum.RED.getStatusId() -> {
                        imageCheckRed.setImageResource(R.drawable.ic_checked_red)
                        imageCheckGreen.setImageResource(R.drawable.ic_unchecked_green)
                        imageCheckYellow.setImageResource(R.drawable.ic_unchecked_yellow)
                        if (!TextUtils.isEmpty(itemList[position].RType)) {
                            layoutCheckOptionContainer.visibility = View.VISIBLE
                            layoutCheckOptionContainer.requestFocus()
                            if (itemList[position].RFields.Radios != null && itemList[position].RFields.Radios.isNotEmpty()) {
                                if(itemList[position].RType == Constants.TYPE_FOUR || itemList[position].RType == Constants.TYPE_TWO) {
                                    val adapter = RadioFieldRecyclerAdapter(context,
                                        itemList[position].RFields.Radios,
                                        itemList[position].Value,
                                        position,
                                        object :
                                            RadioFieldRecyclerAdapter.IRadioFieldRecyclerAdapter {
                                            override fun onValueSelected(
                                                value: String,
                                                index: Int
                                            ) {
                                                itemList[index].Value = value
                                                itemList[position].Validated = true
                                            }
                                        }
                                    )
                                    recyclerCheckRadios.layoutManager =
                                        GridLayoutManager(context, 2)
                                    recyclerCheckRadios.adapter = adapter
                                }else if(itemList[position].RType == Constants.TYPE_FIVE || itemList[position].RType == Constants.TYPE_SIX) {
                                    val adapter = MultiSelectRadioFieldRecyclerAdapter(context,
                                        itemList[position].RFields.Radios,
                                        itemList[position].Value,
                                        position,
                                        object :
                                            MultiSelectRadioFieldRecyclerAdapter.IMultiSelectorRadioFieldRecyclerAdapter {
                                            override fun onValueSelected(
                                                value: String,
                                                index: Int
                                            ) {
                                                itemList[index].Value = value
                                                itemList[position].Validated = true
                                            }
                                        }
                                    )
                                    recyclerCheckRadios.layoutManager =
                                        GridLayoutManager(context, 2)
                                    recyclerCheckRadios.adapter = adapter
                                }
                            }
                            when (itemList[position].RType) {
                                Constants.TYPE_ONE -> {
                                    checkCheckRemark.visibility = View.VISIBLE
                                    checkCheckRemark.text = itemList[position].RFields.Checkbox
                                    editEnterMielage.visibility = View.GONE
                                    layoutCheckUploadPhoto.visibility = View.GONE
                                    checkCheckRemark.isChecked =
                                        !TextUtils.isEmpty(itemList[position].Remarks)
                                }
                                Constants.TYPE_TWO, Constants.TYPE_SIX -> {
                                    layoutCheckUploadPhoto.visibility = View.VISIBLE
                                    checkCheckRemark.visibility = View.GONE
                                    editEnterMielage.visibility = View.GONE
                                }
                                Constants.TYPE_THREE -> {
                                    layoutCheckUploadPhoto.visibility = View.VISIBLE
                                    editEnterMielage.visibility = View.VISIBLE
                                    checkCheckRemark.visibility = View.GONE
                                    editEnterMielage.restoreLayoutTransition()
                                    editEnterMielage.setHintTitle(
                                        itemList[position].RFields.Hint ?: ""
                                    )
                                    editEnterMielage.setValue(itemList[position].Remarks ?: "")
                                }
                                Constants.TYPE_FOUR, Constants.TYPE_FIVE -> {
                                    checkCheckRemark.visibility = View.GONE
                                    editEnterMielage.visibility = View.GONE
                                    layoutCheckUploadPhoto.visibility = View.GONE
                                }
                                Constants.TYPE_SEVEN->{
                                    editEnterMielage.visibility = View.VISIBLE
                                    layoutCheckUploadPhoto.visibility = View.GONE
                                    checkCheckRemark.visibility = View.GONE
                                    editEnterMielage.restoreLayoutTransition()
                                    editEnterMielage.setHintTitle(
                                        itemList[position].RFields.Hint ?: ""
                                    )
                                    editEnterMielage.setValue(itemList[position].Remarks ?: "")
                                }
                            }
                        } else {
                            layoutCheckOptionContainer.visibility = View.GONE
                        }
                    }
                    SectionStatusEnum.YELLOW.getStatusId() -> {
                        imageCheckYellow.setImageResource(R.drawable.ic_checked_yellow)
                        imageCheckGreen.setImageResource(R.drawable.ic_unchecked_green)
                        imageCheckRed.setImageResource(R.drawable.ic_unchecked_red)
                        if (!TextUtils.isEmpty(itemList[position].YType)) {
                            layoutCheckOptionContainer.visibility = View.VISIBLE
                            layoutCheckOptionContainer.requestFocus()
                            if (itemList[position].YFields.Radios != null && itemList[position].YFields.Radios.isNotEmpty()) {
                                if(itemList[position].YType == Constants.TYPE_FOUR || itemList[position].YType == Constants.TYPE_TWO) {
                                    val adapter = RadioFieldRecyclerAdapter(context,
                                        itemList[position].YFields.Radios,
                                        itemList[position].Value,
                                        position,
                                        object :
                                            RadioFieldRecyclerAdapter.IRadioFieldRecyclerAdapter {
                                            override fun onValueSelected(
                                                value: String,
                                                index: Int
                                            ) {
                                                itemList[index].Value = value
                                                itemList[index].Validated = true
                                            }
                                        }
                                    )
                                    recyclerCheckRadios.layoutManager =
                                        GridLayoutManager(context, 2)
                                    recyclerCheckRadios.adapter = adapter
                                }else if(itemList[position].YType == Constants.TYPE_FIVE || itemList[position].YType == Constants.TYPE_SIX) {
                                    val adapter = MultiSelectRadioFieldRecyclerAdapter(context,
                                        itemList[position].YFields.Radios,
                                        itemList[position].Value,
                                        position,
                                        object :
                                            MultiSelectRadioFieldRecyclerAdapter.IMultiSelectorRadioFieldRecyclerAdapter {
                                            override fun onValueSelected(
                                                value: String,
                                                index: Int
                                            ) {
                                                itemList[index].Value = value
                                                itemList[index].Validated = true
                                            }
                                        }
                                    )
                                    recyclerCheckRadios.layoutManager =
                                        GridLayoutManager(context, 2)
                                    recyclerCheckRadios.adapter = adapter
                                }
                            }

                            when (itemList[position].YType) {
                                Constants.TYPE_ONE -> {
                                    checkCheckRemark.visibility = View.VISIBLE
                                    checkCheckRemark.text = itemList[position].YFields.Checkbox
                                    editEnterMielage.visibility = View.GONE
                                    layoutCheckUploadPhoto.visibility = View.GONE
                                    checkCheckRemark.isChecked =
                                        !TextUtils.isEmpty(itemList[position].Remarks)
                                }
                                Constants.TYPE_TWO, Constants.TYPE_SIX -> {
                                    layoutCheckUploadPhoto.visibility = View.VISIBLE
                                    checkCheckRemark.visibility = View.GONE
                                    editEnterMielage.visibility = View.GONE
                                }
                                Constants.TYPE_THREE -> {
                                    layoutCheckUploadPhoto.visibility = View.VISIBLE
                                    editEnterMielage.visibility = View.VISIBLE
                                    checkCheckRemark.visibility = View.GONE
                                    editEnterMielage.restoreLayoutTransition()
                                    editEnterMielage.setHintTitle(
                                        itemList[position].YFields.Hint ?: ""
                                    )
                                    editEnterMielage.setValue(itemList[position].Remarks ?: "")
                                }
                                Constants.TYPE_FOUR, Constants.TYPE_FIVE -> {
                                    checkCheckRemark.visibility = View.GONE
                                    editEnterMielage.visibility = View.GONE
                                    layoutCheckUploadPhoto.visibility = View.GONE
                                }
                                Constants.TYPE_SEVEN->{
                                    editEnterMielage.visibility = View.VISIBLE
                                    layoutCheckUploadPhoto.visibility = View.GONE
                                    checkCheckRemark.visibility = View.GONE
                                    editEnterMielage.restoreLayoutTransition()
                                    editEnterMielage.setHintTitle(
                                        itemList[position].YFields.Hint ?: ""
                                    )
                                    editEnterMielage.setValue(itemList[position].Remarks ?: "")
                                }
                            }
                        } else {
                            layoutCheckOptionContainer.visibility = View.GONE
                        }
                    }
                    else -> {
                        layoutCheckOptionContainer.visibility = View.GONE
                    }
                }

                imageCheckGreen.setOnClickListener {
                    if (itemList[position].Status != SectionStatusEnum.GREEN.getStatusId()) {
                        itemList[position].Value = ""
                        itemList[position].Remarks = ""
                        itemList[position].Status = SectionStatusEnum.GREEN.getStatusId()
                        itemList[position].Validated = true
                        notifyItemChanged(position)
                    }
                }
                imageCheckRed.setOnClickListener {
                    if (itemList[position].Status != SectionStatusEnum.RED.getStatusId()) {
                        itemList[position].Value = ""
                        itemList[position].Remarks = ""
                        itemList[position].Status = SectionStatusEnum.RED.getStatusId()
                        itemList[position].Validated = itemList[position].RType == Constants.TYPE_FOUR && itemList[position].RFields.Radios.isEmpty()
                        notifyItemChanged(position)
                    }
                }
                imageCheckYellow.setOnClickListener {
                    if (itemList[position].Status != SectionStatusEnum.YELLOW.getStatusId()) {
                        itemList[position].Value = ""
                        itemList[position].Remarks = ""
                        itemList[position].Status = SectionStatusEnum.YELLOW.getStatusId()
                        itemList[position].Validated = itemList[position].YType == Constants.TYPE_FOUR && itemList[position].YFields.Radios.isEmpty()
                        notifyItemChanged(position)
                    }
                }

                checkCheckRemark.setOnCheckedChangeListener { buttonView, isChecked ->
                    when (isChecked) {
                        true -> {
                            itemList[position].Remarks = buttonView.text.toString()
                            itemList[position].Validated = true
                        }
                        false -> {
                            itemList[position].Remarks = ""
                            itemList[position].Validated = false
                        }
                    }
                }
                editEnterMielage.addWatcher(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence?,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable?) {
                        itemList[position].Value = editEnterMielage?.value
                        itemList[position].Remarks = editEnterMielage?.value

                        itemList[position].Validated = s?.length ?: -1 > 0
                    }
                })

                layoutCheckUploadPhoto.setOnClickListener {
                    listener.onUploadPhotoClicked(position)
                }
            }
        }
    }

    //Region IVehicleImageClicked
    override fun onImageItemClicked(images: List<String>, selectedIndex: Int) {
        listener?.onImageItemClicked(images, selectedIndex)
    }
    //endregion
}