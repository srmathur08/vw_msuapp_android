package com.vectorform.msu.ui.splash

import android.app.NotificationManager
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ActivitySplashBinding
import com.vectorform.msu.databinding.ForceUpdateDialogLayoutBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.ui.login.LoginActivity
import com.vectorform.msu.utils.UtilsActivity

class SplashActivity : UtilsActivity(), SplashContract.ISplashActivity,
    SplashContract.SplashRouter {

    private var binding: ActivitySplashBinding? = null
    private var presenter: SplashContract.ISplashPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        supportActionBar?.hide()
        presenter = SplashPresenter(this)
        presenter?.checkRootedDevice()
    }

    //Region ISplashActivity
    override fun navigateToLogin() {
        Handler(Looper.getMainLooper()).postDelayed({
            binding?.apply {
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }
        }, 500)
    }

    override fun showPermissionMessage(message: String) {
        showMessage("Alert", message,
            DialogInterface.OnClickListener { dialog, which -> presenter?.checkPermissions() })
    }

    override fun showRootedDeviceMessage(message: String) {
        showMessage("Alert", message,
            DialogInterface.OnClickListener { dialog, which -> /*finish()*/presenter?.checkPermissions() })
    }

    override fun removeNotificationFromTray() {
        val mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager?.let { manager ->
            try {
                manager.cancelAll()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun navigateToHome() {
        startActivity(Intent(this, HomeActivity::class.java)).apply { finish() }
    }

    override fun showLoading() {
        showProgress()
    }

    override fun hideLoading() {
        dismissProgress()
    }

    override fun showForceUpdate(playStoreLink: String) {
        var dialogBinding = ForceUpdateDialogLayoutBinding.inflate(layoutInflater)
        dialogBinding?.apply {
            val builder = android.app.AlertDialog.Builder(this@SplashActivity)
            builder.setView(root)
            builder.setCancelable(false)
            val ad = builder.create()
            btnGotIt.setOnClickListener {
                ad.dismiss()
                val viewIntent = Intent(
                    "android.intent.action.VIEW",
                    Uri.parse(playStoreLink)
                )
                startActivity(viewIntent)
            }
            ad.show()
        }
    }

    override fun showConcreteMessage(message: String) {
       showMessage(getString(R.string.alert), message) { dialog, which ->
           dialog.dismiss()
           finish()
       }
    }

    //endregion


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        presenter?.onPermissionResult(requestCode, permissions, grantResults)
    }
}