package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview

import com.vectorform.msu.data.input.MarkersValueInput
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.BaseFragmentContract

interface ThreeSixtyViewPagerItemContract {

    interface IThreeSixtyViewPagerItemFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will update the page title
         * @param title : String (Section name)
         */
        fun updateTitle(title: String)

        /**
         * This function will show the Three Sixty view images in Car view
         * @param imagePaths : List<String>
         * @param index : Int default image index
         */
        fun showThreeSixtyImages(imagePaths: List<String>, index: Int)

        /**
         * This function will update the markers in Car view for each image
         * @param markers : ArrayList<ImageMarkerModel>
         */
        fun updateScratchAndDamages(markers: ArrayList<ImageMarkerModel>)

        /**
         * This function will show CleaningRemark
         * @param remark : String
         */
        fun showCleaningRemark(remark: String)

        /**
         * This function will show DamageRemark
         * @param remark : String
         */
        fun showDamageRemark(remark: String)

        /**
         * This function will show Notes
         * @param notes : String
         */
        fun showNotes(notes: String)

        /**
         * This function will open Marker Dialog Fragment to draw markers
         * @param dataItem : VehicleHealthCheckUIModelItem
         * @param index: Int
         */
        fun openMarkerDialog(dataItem: VehicleHealthCheckUIModelItem, index: Int)

    }

    interface IThreeSixtyViewPagerItemPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         * @param index : Int car view image index
         */
        fun viewCreated(index: Int)

        /**
         * This function will append the marker on selected image
         * @param image : String (path to image)
         * @param marker: MarkersValueInput
         */
        fun updateMarkers(image: String, marker: MarkersValueInput)

        /**
         *This function will update cleaning remark in data model
         * @param remark : String
         */
        fun updateCleaningRemark(remark: String)

        /**
         *This function will update cleaning damage in data model
         * @param remark : String
         */
        fun updateDamageRemark(remark: String)


        /**
         *This function will update other notes in data model
         * @param notes : String
         */
        fun updateNotes(notes: String)

        /**
         * This function will handle click on Car View
         */
        fun onCarViewClicked(index: Int)

        fun onImageAdded()
    }

}