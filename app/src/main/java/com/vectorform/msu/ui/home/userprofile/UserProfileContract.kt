package com.vectorform.msu.ui.home.userprofile

import com.vectorform.msu.data.responses.vf.LoginResponse
import com.vectorform.msu.utils.BaseFragmentContract

interface UserProfileContract {

    interface IUserProfileFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will show user information in UI
         */
        fun bindUserData(loginResponse: LoginResponse, mobile : String)

        fun showChooser()
        fun hideChooser()
        fun showProfileImage(imagePath : String)
    }

    interface IUserProfilePresenter {

        /**
         * This function will notify presenter that view is created
         */
        fun OnViewCreated()
        /**
         * This function will handle the back icon click
         */
        fun onBackClicked()

        /**
         * This function will handle logout button click
         */
        fun onLogoutClicked()

        /**
         * This function will handle profile icon click
         */
        fun onProifleIconClicked()

        /**
         * This function will open Camera
         */
        fun onOpenCamera()

        /**
         * This funtion will open Gallery
         */
        fun onOpenGallery()

        fun processCameraResult(cameraImagePath : String)

        fun onNotificationIconClicked()
    }

}