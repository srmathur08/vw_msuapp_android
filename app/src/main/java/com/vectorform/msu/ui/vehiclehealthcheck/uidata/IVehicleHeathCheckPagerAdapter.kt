package com.vectorform.msu.ui.vehiclehealthcheck.uidata

interface IVehicleHeathCheckPagerAdapter {
    fun uploadPhoto(sectionIndex: Int, subSectionIndex: Int)
}