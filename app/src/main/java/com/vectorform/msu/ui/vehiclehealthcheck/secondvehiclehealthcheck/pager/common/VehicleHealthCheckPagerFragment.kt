package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.vectorform.msu.databinding.FragmentVehicleHealthCheckPagerItemBinding
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.IVehicleHeathCheckPagerAdapter
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.SubSection
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.BaseFragment

class VehicleHealthCheckPagerFragment : BaseFragment(),
    VehicleHealthCheckPagerContract.IVehicleHealthCheckPagerFragment, View.OnClickListener,
    IVehicleHealthCheckSubSectionRecyclerAdapter {

    private var binding: FragmentVehicleHealthCheckPagerItemBinding? = null
    private lateinit var item: VehicleHealthCheckUIModelItem
    private var presenter: VehicleHealthCheckPagerPresenter? = null
    private lateinit var listener: IVehicleHeathCheckPagerAdapter
    private var sectionIndex: Int = 0

    fun setPage(
        pagerItem: VehicleHealthCheckUIModelItem,
        listener: IVehicleHeathCheckPagerAdapter,
        sectionIndex: Int
    ) {
        this.item = pagerItem
        this.listener = listener
        this.sectionIndex = sectionIndex
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = VehicleHealthCheckPagerPresenter(
                safeContext,
                item,
                this,
                activity as VehicleHealthCheckActivity
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVehicleHealthCheckPagerItemBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
    }//endregion

    //Region IVehicleHealthCheckPagerFragment
    override fun updateTitle(title: String) {
        binding?.apply {
            textPagerItemTitle.text = title
        }
    }

    override fun binRecycler(subSectionList: List<SubSection>) {
        activity?.let { safeContext ->
            binding?.apply {
                val adapter = VehicleHealthCheckSubSectionRecyclerAdapter(
                    safeContext,
                    subSectionList,
                    this@VehicleHealthCheckPagerFragment
                )
                recyclerPagerItemSubSection.layoutManager = LinearLayoutManager(activity)
                recyclerPagerItemSubSection.adapter = adapter
            }
        }
    }
    //endregion

    //Region IVehicleHealthCheckSubSectionRecyclerAdapter
    override fun onUploadPhotoClicked(subSectionPosition: Int) {
        listener.uploadPhoto(sectionIndex, subSectionPosition)
    }

    override fun onImageItemClicked(images: List<String>, selectedIndex: Int) {
        presenter?.onImageItemClicked(images, selectedIndex)
    }
    //endregion

    //Region Public functions
    /**
     * This function will notify the subsection recycler adapter
     * @param index : Int
     */
    fun notifySubSection(index: Int) {
        binding?.apply {
            recyclerPagerItemSubSection.adapter?.notifyItemChanged(index)
        }
    }
    //endregion
}