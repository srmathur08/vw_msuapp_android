package com.vectorform.msu.ui.home.workinprogress

import android.content.Context
import android.location.Location
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.utils.CommonUtils
import com.google.android.gms.common.api.ResolvableApiException
import com.vectorform.msu.R
import com.vectorform.msu.data.input.SrStatusInput
import com.vectorform.msu.data.input.UpdateServiceDetailsInput
import com.vectorform.msu.data.responses.vf.SRMarkResponse
import com.vectorform.msu.ui.home.HomeContract
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.ServiceComplainsEnum

class WorkInProgressPresenter(
    private val context: Context,
    private val view: WorkInProgressContract.IWorkInProgressFragment,
    private val router: HomeContract.HomeRouter
) : WorkInProgressContract.IWorkInProgressPresenter {

    private var serviceComplains: ArrayList<ServiceComplainModel>? = null
    private var isVehicleHealthCheckCompleted: Boolean = false

    override fun fetchServiceComplains() {

        router.getWorkInProgress()?.let { wip ->
            view.bindUserInfo(wip)
            when (serviceComplains) {
                null -> {
                    serviceComplains = ArrayList()
                    serviceComplains?.let { complains ->
                        if(wip.IsOilChange) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "1"
                                complain = ServiceComplainsEnum.IsOilChange
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsAirFilter ) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "4"
                                complain = ServiceComplainsEnum.IsAirFilter
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsBrakes) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "2"
                                complain = ServiceComplainsEnum.IsBrakes
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsBrakesDisk) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "4"
                                complain = ServiceComplainsEnum.IsBrakesDisk
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsSuspensionStrut) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "4"
                                complain = ServiceComplainsEnum.IsSuspensionStrut
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsShockAbsorber) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "4"
                                complain = ServiceComplainsEnum.IsShockAbsorber
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsControlArm) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "4"
                                complain = ServiceComplainsEnum.IsControlArm
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsBatteryCheck) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "4"
                                complain = ServiceComplainsEnum.IsBatteryCheck
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsBulbReplace ) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "4"
                                complain = ServiceComplainsEnum.IsBulbReplace
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsHornReplace) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "4"
                                complain = ServiceComplainsEnum.IsHornReplace
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsTyreCheck) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "3"
                                complain = ServiceComplainsEnum.IsTyreCheck
                                isChecked = false
                                isMandatory = true
                            })
                        }
                        if(wip.IsTyreReplace) {
                            complains.add(ServiceComplainModel().apply {
                                complainId = "4"
                                complain = ServiceComplainsEnum.IsTyreReplace
                                isChecked = false
                                isMandatory = true
                            })
                        }
                    }
                }
                else -> {
                }
            }
        }
    }

    //Region IWorkInProgressPresenter
    override fun onCloseClicked() {
        router?.popBack()
    }

    override fun onComplainCheckChanged(complain: ArrayList<ServiceComplainModel>) {
        serviceComplains?.clear()
        serviceComplains?.addAll(complain)
        serviceComplains?.let { allComplains ->

            if (isVehicleHealthCheckCompleted) {
                view?.complainCompleted()
            } else {
                view?.vehicleHealthCheckNotCompleted()
            }
        }
    }

    override fun onViewServiceHistoryClicked() {
        router?.navigateWorkInProgressToServiceHistory()
    }

    override fun onVehicleHealthCheckClicked() {
        router?.navigateWorkInProgressToFirstVehicleHealthCheck()
    }

    override fun serviceCompleted(otherService : String?, repairEstimate : String?) {
        router?.getWorkInProgress()?.let { data ->
            view.showLoading()
            val srDetailsInput = UpdateServiceDetailsInput().apply {
                Srid = data.Srid.toString()
                serviceComplains?.forEach {c->
                    when(c.complain){
                        ServiceComplainsEnum.IsOilChange->{
                            IsOilChange = c.isChecked
                        }
                        ServiceComplainsEnum.IsAirFilter->{
                            IsAirFilter = c.isChecked
                        }
                        ServiceComplainsEnum.IsBrakes->{
                            IsBrakes = c.isChecked
                        }
                        ServiceComplainsEnum.IsBrakesDisk->{
                            IsBrakesDisk = c.isChecked
                        }
                        ServiceComplainsEnum.IsSuspensionStrut->{
                            IsSuspensionStrut = c.isChecked
                        }
                        ServiceComplainsEnum.IsShockAbsorber->{
                            IsShockAbsorber = c.isChecked
                        }
                        ServiceComplainsEnum.IsControlArm->{
                            IsControlArm = c.isChecked
                        }
                        ServiceComplainsEnum.IsBatteryCheck->{
                            IsBatteryCheck = c.isChecked
                        }
                        ServiceComplainsEnum.IsBulbReplace->{
                            IsBulbReplace = c.isChecked
                        }
                        ServiceComplainsEnum.IsHornReplace->{
                            IsHornReplace = c.isChecked
                        }
                        ServiceComplainsEnum.IsTyreCheck->{
                            IsTyreCheck = c.isChecked
                        }
                        ServiceComplainsEnum.IsTyreReplace->{
                            IsTyreReplace = c.isChecked
                        }
                    }
                }
            }
            VfCalls(context).UpdateServiceDetailSR(srDetailsInput, object : VfCalls.OnResponse<SRMarkResponse>{
                override fun onSucess(response: SRMarkResponse) {

                    val input = SrStatusInput(data.Srid.toString(), Constants.SERVICE_COMPLETED_STATUS, otherService, repairEstimate)
                    VfCalls(context).updateSrStatus(input, object : VfCalls.OnResponse<SRMarkResponse>{
                        override fun onSucess(response: SRMarkResponse) {
                            view?.hideLoading()
                            router?.popBack()
                        }
                        override fun onFailure(message: String) {
                            view?.hideLoading()
                            view?.showCustomMessage(R.string.failure, message, null)
                        }
                    })
                }

                override fun onFailure(message: String) {
                    view?.hideLoading()
                    view?.showCustomMessage(R.string.failure, message, null)
                }
            })
            

        }
    }

    override fun onResumed() {
        router?.getWorkInProgress()?.let { serviceData ->
            isVehicleHealthCheckCompleted = serviceData.IsVCCompleted
            if (isVehicleHealthCheckCompleted) {
                view.vehicleHealthCheckCompleted()
                serviceComplains?.let {
                    view?.bindComplains(it)
                }
            } else {
                view.vehicleHealthCheckNotCompleted()
            }
        }
    }

    override fun onMapReady(location: Location?) {
        router?.getWorkInProgress()?.let { wip ->
            var customerAddress = "Unknown"
            val address = CommonUtils.getAddress(context, wip.Lat, wip.Lng)
            address?.apply {
                customerAddress = getAddressLine(0)
            }
            view.drawRouteToCustomerLocation(wip.Lat, wip.Lng, customerAddress, location)
        }
    }

    override fun handleLocationResolution(exception: ResolvableApiException) {
        router.showLocationResolution(exception)
    }

    override fun onServiceComplainClicked() {
        serviceComplains?.let {
            view?.bindComplains(it)
        }
    }

    override fun onNavigateToUserLocationClicked() {
        router.getWorkInProgress()?.let { wip->
            view?.launchGoogleMapDirections(wip.Lat, wip.Lng)
        }
    }

    //endregion

}