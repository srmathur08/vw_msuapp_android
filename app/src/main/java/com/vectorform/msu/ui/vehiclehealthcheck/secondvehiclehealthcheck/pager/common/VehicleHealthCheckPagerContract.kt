package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.common

import com.vectorform.msu.ui.vehiclehealthcheck.uidata.SubSection
import com.vectorform.msu.utils.BaseFragmentContract

interface VehicleHealthCheckPagerContract {

    interface IVehicleHealthCheckPagerFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will update the page title
         * @param title : String (Section name)
         */
        fun updateTitle(title: String)

        /**
         * This function will bind the sub-sections in recycler view
         * @param subSectionList: List<SubSection> inside a section
         */
        fun binRecycler(subSectionList: List<SubSection>)
    }

    interface IVehicleHealthCheckPagerPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will handle the subsection image click to show in full screen
         * @param images: List<String> in subsection
         * @param selectedIndex : Int index of clicked image
         */
        fun onImageItemClicked(images: List<String>, selectedIndex: Int)
    }

}