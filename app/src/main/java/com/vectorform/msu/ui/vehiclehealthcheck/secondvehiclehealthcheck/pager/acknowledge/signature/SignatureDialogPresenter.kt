package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.acknowledge.signature

import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract

class SignatureDialogPresenter(
    private val view: SignatureDialogContract.ISignatureDialogFragment,
    private val capturedSignature: String?,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter
) : SignatureDialogContract.ISignatureDialogPresenter {

    //Region ISignatureDialogPresenter
    override fun viewCreated() {
        view?.showCapturedSignature(capturedSignature)
    }

    override fun onAcceptClicked() {
        view?.sendBitmapToListener()
    }

    override fun onClearClicked() {
        view?.clearSignatureView()
    }

    override fun onCloseClicked() {
        view?.dismissSignatureDialog()
    }
    //endregion


}