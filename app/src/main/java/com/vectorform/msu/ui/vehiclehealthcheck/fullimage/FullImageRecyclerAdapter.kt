package com.vectorform.msu.ui.vehiclehealthcheck.fullimage

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.vectorform.msu.databinding.ItemFullImageBinding
import com.vectorform.msu.utils.Constants

class FullImageRecyclerAdapter(
    private val context: Context,
    private val itemList: List<String>,
    private val isDynamicUrl: Boolean
) : PagerAdapter() {

    override fun getCount(): Int {
        return itemList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding: ItemFullImageBinding =
            ItemFullImageBinding.inflate(LayoutInflater.from(context))
        binding?.apply {
            when (isDynamicUrl) {
                true -> {
                    Glide.with(context).load(Constants.BASEURL + itemList[position])
                        .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageItemFullImage)
                }
                false -> {
                    Glide.with(context).load(itemList[position])
                        .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageItemFullImage)
                }
            }
        }
        container.addView(binding.root)
        return binding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}