package com.vectorform.msu.ui.home.workinprogress

interface IServiceComplainRecyclerAdapter {
    /**
     * This function will pass the Service Complain to UI to handle logic
     * @param complain : ServiceComplainModel
     */
    fun OnServiceComplainItemCheckChanged(complain: ServiceComplainModel)
}