package com.vectorform.msu.ui.home.notificationdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vectorform.msu.databinding.FragmentNotificationDetailsBinding
import com.vectorform.msu.databinding.FragmentNotificationsBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.BaseFragment

class NotificationDetailsFragment : BaseFragment(), NotificationDetailsContract.INotificationDetailsFragment,
    View.OnClickListener {

    private var binding: FragmentNotificationDetailsBinding? = null
    private var presenter: NotificationDetailsContract.INotificationDetailsPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = NotificationDetailsPresenter(safeContext, this, activity as HomeActivity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationDetailsBinding.inflate(inflater, container, false)
        presenter?.fetchNotifications()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageNotificationDetailsBack.setOnClickListener(this@NotificationDetailsFragment)
        }
    }

    override fun onClick(p0: View?) {
        binding?.apply {
            when(p0){
                imageNotificationDetailsBack->{
                    presenter?.onBackClicked()
                }
            }
        }
    }

    //Region INotificationPresenter
    override fun bindNotifications() {
//        TODO("Not yet implemented")
    }
    //endregion

}