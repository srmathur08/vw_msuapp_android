package com.vectorform.msu.ui.home.servicehistory

import android.content.Context
import com.fcaindia.drive.network.VfCalls
import com.vectorform.msu.R
import com.vectorform.msu.data.input.SRsBySRIdInput
import com.vectorform.msu.data.responses.sfdc.InstanceUrlResponse
import com.vectorform.msu.data.responses.sfdc.services.SFDCMasterDataResponse
import com.vectorform.msu.data.responses.sfdc.services.ServiceHistoryData
import com.vectorform.msu.data.responses.vf.SRMarkResponse
import com.vectorform.msu.network.sfdc.SfdcCalls
import com.vectorform.msu.ui.home.HomeContract

class ServiceHistoryPresenter(
    private val context: Context,
    private val view: ServiceHistoryContract.IServiceHistoryFragment,
    private val router: HomeContract.HomeRouter
) : ServiceHistoryContract.IServiceHistoryPresenter {

    private var serviceHistoryData: List<ServiceHistoryData>? = null
    private var overAllRating: String? = null
    private var lessRatingPoint: String? = null

    //Region IServiceHistoryPresenter
    override fun onBackClicked() {
        router?.popBack()
    }

    override fun fetchServiceHistory() {
        router?.getWorkInProgress()?.let { data ->
            serviceHistoryData?.let { history ->
                view.hideNoData()
                view.bindServiceHistoryData(history)
            } ?: run {
                view.showLoading()
                SfdcCalls(context).getInstanceUrlandToken(object :
                    SfdcCalls.OnResponse<InstanceUrlResponse> {
                    override fun onSucess(response: InstanceUrlResponse) {

                        SfdcCalls(context).getSfdcServicesData(
                            response.instance_url,
                            response.access_token,
                            data.Vinnumber,
                            object : SfdcCalls.OnResponse<SFDCMasterDataResponse> {
                                override fun onSucess(response: SFDCMasterDataResponse) {
                                    overAllRating = response.Overall_customer_satisfaction_score
                                    lessRatingPoint =
                                        response.Dealer_PSF_Question_rated_less_than_4_star?.toString()
                                    if (response.Service_History_Data?.isNotEmpty() == true) {
                                        serviceHistoryData = response.Service_History_Data
                                        view.hideNoData()
                                        response.Service_History_Data?.let {
                                            view.bindServiceHistoryData(it)
                                        }

                                    } else {
                                        view.showNoData()
                                    }
                                    view.hideLoading()
                                }

                                override fun onFailure(message: String) {
                                    view.hideLoading()
                                    view.showCustomMessage(
                                        R.string.failure,
                                        message
                                    ) { dialog, which -> router?.popBack() }
                                }
                            })

                    }

                    override fun onFailure(message: String) {
                        view.hideLoading()
                        view.showCustomMessage(
                            R.string.failure,
                            message
                        ) { dialog, which -> router?.popBack() }
                    }
                })
            }
        }
    }

    override fun onServiceHistoryCardClicked(serviceHitoryModel: ServiceHistoryData) {
        router?.navigateServiceHistoryToServiceDetails(
            serviceHitoryModel,
            overAllRating,
            lessRatingPoint
        )
    }

    override fun onSendServiceHistory() {
        router?.getWorkInProgress()?.Srid?.let { srId->
            view?.showLoading()
            VfCalls(context).SendSRHistory(SRsBySRIdInput(srId), object : VfCalls.OnResponse<SRMarkResponse>{
                override fun onSucess(response: SRMarkResponse) {
                    view?.hideLoading()
                    view?.showCustomMessage(R.string.alert, "Service History sent successfully", null)
                }

                override fun onFailure(message: String) {
                    view?.hideLoading()
                    view?.showCustomMessage(R.string.alert, message, null)
                }
            })
        }
    }
    //endregion


}