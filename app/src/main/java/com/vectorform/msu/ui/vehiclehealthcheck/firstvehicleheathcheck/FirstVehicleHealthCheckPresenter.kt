package com.vectorform.msu.ui.vehiclehealthcheck.firstvehicleheathcheck

import android.content.Context
import android.text.TextUtils
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.observables.ObservableObject
import com.vectorform.msu.R
import com.vectorform.msu.data.input.CreateVehicleHealthCheckInput
import com.vectorform.msu.data.input.SRMarkInput
import com.vectorform.msu.data.input.SrStatusInput
import com.vectorform.msu.data.responses.vf.CreateVehicleHealthCheckResponse
import com.vectorform.msu.data.responses.vf.SRMarkResponse
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.Logger
import com.vectorform.msu.utils.MandatoryServiceTypeEnums
import java.text.SimpleDateFormat
import java.util.*

class FirstVehicleHealthCheckPresenter(
    private val context: Context,
    private val view: FirstVehicleHealthCheckContract.IFirstVehicleHealthCheckFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter,
) : FirstVehicleHealthCheckContract.IFirstVehicleHealthCheckPresenter {

    private var serviceData: SRsByDateResponse.ServiceRequest? = null

    init {
        serviceData = router?.getServiceRequestMasterData()
    }

    //Region IFirstVehicleHealthCheckPresenter

    override fun viewCreated() {
        view.preFillServiceDate(SimpleDateFormat("dd MMM yyyy").format(Date().time))
        serviceData?.let { data ->
            view.preFillCustomerData(data)
        }
    }

    override fun onCloseClicked() {
        router?.closeActivity()
    }

    override fun onCreateVehicleHealthCheck(fuelLevelPercent: Float, vehicleMileage: String) {
        view?.showLoading()
        serviceData?.let { data ->
            val request =
                CreateVehicleHealthCheckInput(fuelLevelPercent, data.Srid, vehicleMileage)
            VfCalls(context).createVehicleHealthCheck(
                request,
                object : VfCalls.OnResponse<CreateVehicleHealthCheckResponse> {
                    override fun onSucess(response: CreateVehicleHealthCheckResponse) {
                        view?.hideLoading()
                        router?.navigateFirstVehicleHealthCheckToVehicleAsets(
                            response.VehicleCheckupId,
                            fuelLevelPercent.toInt()
                        )
                    }

                    override fun onFailure(message: String) {
                        view?.hideLoading()
                        view?.showCustomMessage(
                            R.string.failure,
                            message
                        ) { which, dialog -> router.closeActivity() }
                    }
                })
        }
    }

    override fun onNextClicked(fuelLevelPercent: Float, vehicleMileage: String) {
        serviceData?.let { data ->
            try{
                val mileage = vehicleMileage.toInt()
                val sfdcMileage = data.SFDCOdometerReading?.toInt() ?: 0
                if(mileage < sfdcMileage){
                    view?.showMileageError("Mileage cannot be less than $sfdcMileage")
                    return
                }
            }catch (e : Exception){
               Logger.log("Odometer :: ", "Parse Exception")
                view?.showMileageError("Invalid mileage")
                return
            }

            view.showLoading()
            val statusInput = SrStatusInput(data.Srid.toString(), Constants.SERVICE_ACTIVE_STATUS, "", "")
            VfCalls(context).updateSrStatus(statusInput, object : VfCalls.OnResponse<SRMarkResponse>{
                override fun onSucess(response: SRMarkResponse) {
                    val startRequest = SRMarkInput(data.Srid)
                    VfCalls(context).markServiceStart(
                        startRequest,
                        object : VfCalls.OnResponse<SRMarkResponse> {
                            override fun onSucess(response: SRMarkResponse) {
                                when(data.ServiceType){
                                    MandatoryServiceTypeEnums.INSPECTION_SERVICE.getMandatoryServiceType(), MandatoryServiceTypeEnums.MAINTENANCE.getMandatoryServiceType()->{
                                        onCreateVehicleHealthCheck(fuelLevelPercent, vehicleMileage)
                                    }
                                    else->{
                                        view?.hideLoading()
                                        view?.showSkipVehicleHealthCheck()
                                    }
                                }
                            }

                            override fun onFailure(message: String) {
                                view.hideLoading()
                                view.showCustomMessage(
                                    R.string.failure,
                                    message
                                ) { which, dialog -> router.closeActivity() }
                            }
                        })
                }

                override fun onFailure(message: String) {
                    view.hideLoading()
                    view.showCustomMessage(
                        R.string.failure,
                        message
                    ) { which, dialog -> router.closeActivity() }
                }
            })
        }
    }

    override fun skipVehicleHealthCheck() {
        router.getServiceRequestMasterData()?.IsVCCompleted = true
        ObservableObject.getInstance()?.updateValue(true)
        router.closeActivity()
    }

    //endregion


}