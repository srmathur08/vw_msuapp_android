package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.acknowledge.signature

interface SignatureDialogContract {

    interface ISignatureDialogFragment {
        /**
         * This function will show captured signature
         * @param signature : String (optional as initially no signature is captured by default)
         */
        fun showCapturedSignature(signature: String?)

        /**
         * This function will dismiss the signature dialog fragment
         */
        fun dismissSignatureDialog()

        /**
         * This function will pass the captured signature to parent
         */
        fun sendBitmapToListener()

        /**
         * This function will clear signature view
         */
        fun clearSignatureView()

    }

    interface ISignatureDialogPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will handle the accept button click
         */
        fun onAcceptClicked()

        /**
         * This function will handle the clear button click
         */
        fun onClearClicked()

        /**
         * This function will handle the close button click
         */
        fun onCloseClicked()
    }

}