package com.vectorform.msu.ui.splash

import android.Manifest
import android.content.pm.PackageManager
import android.text.TextUtils
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.utils.CommonUtils
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.messaging.FirebaseMessaging
import com.scottyab.rootbeer.RootBeer
import com.vectorform.msu.BuildConfig
import com.vectorform.msu.data.responses.vf.SettingsResponse
import com.vectorform.msu.utils.Constants

class SplashPresenter(private val activity: SplashActivity) : SplashContract.ISplashPresenter {

    private var view: SplashContract.ISplashActivity = activity
    private val PERMISSION_REQUEST_CODE: Int = 1234

    override fun checkRootedDevice() {
        val rootBeer = RootBeer(activity)
        if (rootBeer.isRootedWithoutBusyBoxCheck) {
            view.showRootedDeviceMessage("Unsupported Device")
//            checkPermissions()
        } else {
            checkPermissions()
        }
//        checkPermissions()
    }

    override fun checkPermissions() {
        if (permissionNeeded()) {
            ActivityCompat.requestPermissions(
                activity, arrayOf(
                    Manifest.permission.INTERNET,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.FOREGROUND_SERVICE
                ), PERMISSION_REQUEST_CODE
            )
        } else {
            generateFireBaseToken()
        }
    }

    override fun onPermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
            && grantResults[1] == PackageManager.PERMISSION_GRANTED
            && grantResults[2] == PackageManager.PERMISSION_GRANTED
            && grantResults[3] == PackageManager.PERMISSION_GRANTED
            && grantResults[4] == PackageManager.PERMISSION_GRANTED
            && grantResults[5] == PackageManager.PERMISSION_GRANTED
            && grantResults[6] == PackageManager.PERMISSION_GRANTED
            && grantResults[7] == PackageManager.PERMISSION_GRANTED
        ) {
            checkPermissions()
        } else {
            view.showPermissionMessage("Please grant all permissions")
        }
    }

    /**
     * This function will check which permission required or not
     * @return Boolean
     * true if any permission is not granted.
     * false if all permissions are granted.
     */
    private fun permissionNeeded(): Boolean {
        return ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.INTERNET
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.READ_PHONE_STATE
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.CAMERA
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
    }

    private fun generateFireBaseToken() {
        view?.showLoading()
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(object : OnCompleteListener<String?> {
                override fun onComplete(task: Task<String?>) {
                    if (!task.isSuccessful) {
                        Log.w(
                            "TOKEN ",
                            "Fetching FCM registration token failed",
                            task.exception
                        )
                        view?.hideLoading()
                        fetchSettings()
                        return
                    }
                    // Get new FCM registration token
                    val token: String? = task.result
                    CommonUtils.saveFirebaseToken(activity, Constants.FIREBASE_DEVICE_TOKEN, token)
                    view.removeNotificationFromTray()

                    fetchSettings()
                }
            })
    }

    private fun fetchSettings(){

        VfCalls(activity).GetSettings(object: VfCalls.OnResponse<SettingsResponse>{
            override fun onSucess(response: SettingsResponse) {

                var androidVersion: String? = null
                var storeLink: String? = null
                response?.data?.forEach { setting->
                    when(setting.Key){
                        Constants.SETTINGS_ANDROID_VERSION->{
                            androidVersion = setting.Value
                        }
                        Constants.SETTINGS_PLAY_STORE_LINK->{
                            storeLink = setting.Value
                        }
                    }
                }

                when(checkForUpdate(BuildConfig.VERSION_NAME, androidVersion)){
                    true->{
                        view?.hideLoading()
                        view?.showForceUpdate(storeLink ?: "https://play.google.com/store/apps/details?id=${activity.packageName}")
                    }
                    false->{
                        view?.hideLoading()
                        when (CommonUtils.getString(activity, Constants.USER_LOGGED_IN)) {
                            Constants.MTRUE -> {
                                view.navigateToHome()
                            }
                            else -> {
                                view.navigateToLogin()
                            }
                        }
                    }
                }
            }

            override fun onFailure(message: String) {
                view?.hideLoading()
                view?.showConcreteMessage(message)
            }
        })
    }

    private fun checkForUpdate(
        existingVersion: String?,
        newVersion: String?
    ): Boolean {
        var existingVersion = existingVersion
        var newVersion = newVersion
        if (TextUtils.isEmpty(existingVersion) || TextUtils.isEmpty(newVersion)) {
            return false
        }
        existingVersion = existingVersion?.replace("\\.".toRegex(), "")
        newVersion = newVersion?.replace("\\.".toRegex(), "")
        val existingVersionLength = existingVersion?.length ?: 0
        val newVersionLength = newVersion?.length ?: 0
        val versionBuilder = StringBuilder()
        if (newVersionLength > existingVersionLength) {
            versionBuilder.append(existingVersion)
            for (i in existingVersionLength until newVersionLength) {
                versionBuilder.append("0")
            }
            existingVersion = versionBuilder.toString()
        } else if (existingVersionLength > newVersionLength) {
            versionBuilder.append(newVersion)
            for (i in newVersionLength until existingVersionLength) {
                versionBuilder.append("0")
            }
            newVersion = versionBuilder.toString()
        }
        return newVersion?.toInt() ?: 0 > existingVersion?.toInt() ?: 0
    }

}