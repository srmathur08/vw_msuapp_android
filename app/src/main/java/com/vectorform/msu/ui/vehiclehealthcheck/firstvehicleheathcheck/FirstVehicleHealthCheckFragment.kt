package com.vectorform.msu.ui.vehiclehealthcheck.firstvehicleheathcheck

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.databinding.DialogSkipVehicleHealthCheckBinding
import com.vectorform.msu.databinding.FragmentFirstVehicleHealthCheckBinding
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.utils.BaseFragment
import com.vectorform.msu.utils.Logger

class FirstVehicleHealthCheckFragment : BaseFragment(),
    FirstVehicleHealthCheckContract.IFirstVehicleHealthCheckFragment, View.OnClickListener,
    MotionLayout.TransitionListener {

    private var binding: FragmentFirstVehicleHealthCheckBinding? = null
    private var presenter: FirstVehicleHealthCheckContract.IFirstVehicleHealthCheckPresenter? = null
    private var fuelLevelPercent: Float = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = FirstVehicleHealthCheckPresenter(
                safeContext,
                this,
                activity as VehicleHealthCheckActivity
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFirstVehicleHealthCheckBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageVHCClose.setOnClickListener(this@FirstVehicleHealthCheckFragment)
            imageVHCDirection.setOnClickListener(this@FirstVehicleHealthCheckFragment)
            layoutFVHCFuelMotion.setTransitionListener(this@FirstVehicleHealthCheckFragment)
//            layoutFVHCFuelMotion.progress = fuelLevelPercent
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageVHCClose -> {
                    presenter?.onCloseClicked()
                }
                imageVHCDirection -> {
                    presenter?.onNextClicked(fuelLevelPercent, inputVHCVehicleMileage.value)
                }
            }
        }
    }
    //endregion

    //Region Motion Layout Transition Listener
    override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
        binding?.apply {
            fuelLevelPercent = layoutFVHCFuelMotion.progress * 100
            Logger.log("onTransitionStarted :: ", "$fuelLevelPercent%")
        }
    }

    override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
        binding?.apply {
            fuelLevelPercent = layoutFVHCFuelMotion.progress * 100
            Logger.log("onTransitionChange :: ", "$fuelLevelPercent%")
        }

    }

    override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
        binding?.apply {
            fuelLevelPercent = layoutFVHCFuelMotion.progress * 100
            Logger.log("onTransitionCompleted :: ", "$fuelLevelPercent%")
        }
    }

    override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {
        binding?.apply {
            fuelLevelPercent = layoutFVHCFuelMotion.progress * 100
            Logger.log("onTransitionTrigger :: ", "$fuelLevelPercent%")
        }
    }
    //endregion

    //Region IFirstVehicleHealthCheckFragment
    override fun preFillServiceDate(date: String) {
        binding?.apply {
            editVHCDate.setValue(date)
        }
    }

    override fun preFillCustomerData(srData: SRsByDateResponse.ServiceRequest) {
        binding?.apply {
            editVHCRepairService.setValue(srData.Srnumber)
            inputVHCCustomerName.setValue(srData.CustomerName)
            inputVHCRegistration.setValue(srData.RegNum)
            inputVHCVin.setValue(srData.Vinnumber)
            inputVHCVehicleMileage.setValue(srData.OdometerReading)
        }
    }

    override fun showMileageError(error: String) {
        binding?.apply {
            inputVHCVehicleMileage.setError(error)
        }
    }

    override fun showSkipVehicleHealthCheck() {
        activity?.let { safeContext->
            binding?.apply {
                val dialogBinding = DialogSkipVehicleHealthCheckBinding.inflate(layoutInflater, null, false)
                dialogBinding?.let {dialog->
                    val alertDialog = AlertDialog.Builder(safeContext)
                    alertDialog.setCancelable(false)
                    alertDialog.setView(dialog.root)
                    val ad = alertDialog.create()
                    ad.show()

                    dialog.buttonSkipDialogSkip.setOnClickListener {
                        ad.dismiss()
                        presenter?.skipVehicleHealthCheck()
                    }
                    dialog.buttonSkipDialogVehicleHealthCheck.setOnClickListener {
                        ad.dismiss()
                        presenter?.onCreateVehicleHealthCheck(fuelLevelPercent, inputVHCVehicleMileage.value)
                    }
                }
            }
        }
    }
    //endregion
}