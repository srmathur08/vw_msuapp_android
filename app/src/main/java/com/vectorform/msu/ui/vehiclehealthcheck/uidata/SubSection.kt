package com.vectorform.msu.ui.vehiclehealthcheck.uidata

import java.io.Serializable

data class SubSection(
    val Name: String,
    val RFields: RFields,
    val RType: String,
    val YFields: YFields,
    val YType: String,
    var VehicleChekUpDataId: Int = 0,
    var Status: Int,
    var Value: String?,
    var Remarks: String?,
    var Assets: ArrayList<String>?,
    var Validated : Boolean = false
) : Serializable