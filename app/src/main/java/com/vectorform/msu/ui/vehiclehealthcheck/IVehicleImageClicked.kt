package com.vectorform.msu.ui.vehiclehealthcheck

interface IVehicleImageClicked {
    /**
     * This function will pass the list of images and selected index to View to show in full screen
     * @param images : List<String>
     * @param selectedIndex : Int
     */
    fun onImageItemClicked(images: List<String>, selectedIndex: Int)
}