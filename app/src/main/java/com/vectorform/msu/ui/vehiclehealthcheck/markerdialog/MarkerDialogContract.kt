package com.vectorform.msu.ui.vehiclehealthcheck.markerdialog

import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview.ImageMarkerModel

interface MarkerDialogContract {

    interface IMarkerDialogFragment {
        /**
         * This function will dismiss/close the Marker Dialog Fragment
         */
        fun closeMarkerDialog()

        /**
         * This function will bind the images in Car View with markers and bring the selected index in front
         * @param images : List<String>
         * @param markers : List<ImageMarkerModel>
         * @param selectedIndex : Int
         */
        fun bindImages(
            images: List<String>,
            markers: ArrayList<ImageMarkerModel>,
            selectedIndex: Int
        )
    }

    interface IMarkerDialogPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will handle the Close icon click
         */
        fun onCloseClicked()
    }

}