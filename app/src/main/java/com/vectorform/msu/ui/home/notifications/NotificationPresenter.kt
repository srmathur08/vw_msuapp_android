package com.vectorform.msu.ui.home.notifications

import android.content.Context
import android.util.Log
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.utils.CommonUtils
import com.google.gson.Gson
import com.vectorform.msu.R
import com.vectorform.msu.data.input.NotificationInput
import com.vectorform.msu.data.input.NotificationReadInput
import com.vectorform.msu.data.responses.vf.NotificationResponse
import com.vectorform.msu.data.responses.vf.Notifications
import com.vectorform.msu.ui.home.HomeContract
import com.vectorform.msu.ui.home.notifications.NotificationsContract
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.Logger

class NotificationPresenter(
    private val context: Context,
    private val view: NotificationsContract.INotificationFragment,
    private val router: HomeContract.HomeRouter
) : NotificationsContract.INotificationPresenter {

    override fun onBackClicked() {
        router?.popBack()
    }

    override fun fetchNotifications() {
        view?.showLoading()
        val input = NotificationInput(CommonUtils.getString(context, Constants.LOGGED_IN_USER_ID))
        VfCalls(context).GetNotification(input, object : VfCalls.OnResponse<NotificationResponse> {
            override fun onSucess(response: NotificationResponse) {
                view?.hideLoading()
                response.data?.let {data->
                    when (data.isNotEmpty()) {
                        true-> {
                            view?.hideNoData()
                            view?.bindNotifications(data)
                        }
                        false->{
                            view?.showNoData()
                        }
                    }
                } ?: run {
                    view?.showNoData()
                }
            }
            override fun onFailure(message: String) {
                view?.hideLoading()
                view?.showNoData()
            }
        })
    }

    override fun onNotificationClicked(notification: Notifications) {
        val input = NotificationReadInput(CommonUtils.getString(context, Constants.LOGGED_IN_USER_ID), notification.Id.toString())
        VfCalls(context).MarkNotification(input, object : VfCalls.OnResponse<Any>{
            override fun onSucess(response: Any) {
               Logger.log("NotificationMarked :: ", "Success")
            }

            override fun onFailure(message: String) {
                Logger.log("NotificationMarked :: ", "Failed")
            }
        })
    }

}