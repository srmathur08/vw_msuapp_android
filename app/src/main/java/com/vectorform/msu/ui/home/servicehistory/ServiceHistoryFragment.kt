package com.vectorform.msu.ui.home.servicehistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.vectorform.msu.data.responses.sfdc.services.ServiceHistoryData
import com.vectorform.msu.databinding.FragmentServiceHistoryBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.BaseFragment

class ServiceHistoryFragment : BaseFragment(), ServiceHistoryContract.IServiceHistoryFragment,
    View.OnClickListener,
    IServiceHistoryRecyclerAdapter {

    private var binding: FragmentServiceHistoryBinding? = null
    private var presenter: ServiceHistoryContract.IServiceHistoryPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = ServiceHistoryPresenter(safeContext, this, activity as HomeActivity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentServiceHistoryBinding.inflate(inflater, container, false)
        presenter?.fetchServiceHistory()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageSHBack.setOnClickListener(this@ServiceHistoryFragment)
            buttonServiceHistorySMS.setOnClickListener(this@ServiceHistoryFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageSHBack -> {
                    presenter?.onBackClicked()
                }
                buttonServiceHistorySMS->{
                    presenter?.onSendServiceHistory()
                }
            }
        }
    }
    //endregion

    //Region IServiceHistoryFragment
    override fun bindServiceHistoryData(serviceHistory: List<ServiceHistoryData>) {
        activity?.let { safeContext ->
            binding?.apply {
                val adapter = ServiceHistoryRecyclerAdapter(
                    safeContext,
                    serviceHistory,
                    this@ServiceHistoryFragment
                )
                recyclerServiceHistory.layoutManager = LinearLayoutManager(safeContext)
                recyclerServiceHistory.adapter = adapter
            }
        }
    }

    override fun showNoData() {
        binding?.apply {
            recyclerServiceHistory.visibility = View.INVISIBLE
            textServiceNoData.visibility = View.VISIBLE
            buttonServiceHistorySMS.visibility = View.GONE
        }
    }

    override fun hideNoData() {
        binding?.apply {
            recyclerServiceHistory.visibility = View.VISIBLE
            textServiceNoData.visibility = View.INVISIBLE
            buttonServiceHistorySMS.visibility = View.VISIBLE
        }
    }
    //endRegion

    //Region IServiceHistoryRecyclerAdapter
    override fun OnServiceHistoryItemClicked(service: ServiceHistoryData) {
        presenter?.onServiceHistoryCardClicked(service)
    }
    //end region
}