package com.vectorform.msu.ui.vehiclehealthcheck.vehiclehealthreport

import androidx.annotation.DrawableRes
import com.vectorform.msu.utils.SectionStatusEnum

data class VehicleHealthReportSectionModel(
    @DrawableRes val drawable: Int,
    val section: String,
    val percentage: String,
    val status: SectionStatusEnum?,
    val showPercentSign: Boolean
)
