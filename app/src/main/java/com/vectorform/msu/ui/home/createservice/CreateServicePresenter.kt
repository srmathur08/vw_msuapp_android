package com.vectorform.msu.ui.home.createservice

import android.content.Context
import com.vectorform.msu.R
import com.vectorform.msu.ui.home.HomeContract
import com.vectorform.msu.utils.Logger

class CreateServicePresenter(
    private val context: Context,
    private val view: CreateServiceContract.ICreateServiceFragment,
    private val router: HomeContract.HomeRouter
) : CreateServiceContract.ICreateServicePresenter {

    private var serviceList: List<ServiceDetailsModel> = listOf(
        ServiceDetailsModel(
            context.getString(R.string.text_oil_change),
            R.drawable.ic_oil_icon,
            false
        ),
        ServiceDetailsModel(
            context.getString(R.string.text_breakes),
            R.drawable.ic_break_icon,
            false
        ),
        ServiceDetailsModel(
            context.getString(R.string.text_tyre_check),
            R.drawable.ic_tyre_icon,
            false
        ),
        ServiceDetailsModel(
            context.getString(R.string.text_battery_check),
            R.drawable.ic_battery_icon,
            false
        )
    )

    //Region ICreateServicePresenter
    override fun viewCreated() {
        view?.bindServiceDetailsItem(serviceList)
    }

    override fun onCloseClicked() {
        router?.popBack()
    }

    override fun onCreateServiceButtonClicked() {
        val selectedItems = serviceList.filter { s -> s.isSelected }
        Logger.log("Selected Items", "${selectedItems.size}")
    }
    //endregion


}