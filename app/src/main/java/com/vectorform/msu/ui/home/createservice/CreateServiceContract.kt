package com.vectorform.msu.ui.home.createservice

import com.vectorform.msu.utils.BaseFragmentContract

interface CreateServiceContract {

    interface ICreateServiceFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will update Service Details in recycler view
         * @param itemList : List<ServiceDetailsModel>
         */
        fun bindServiceDetailsItem(itemList: List<ServiceDetailsModel>)
    }

    interface ICreateServicePresenter {
        /**
         * This function will be called from fragment OnViewCreated()
         */
        fun viewCreated()

        /**
         * This function will handle the close button click
         */
        fun onCloseClicked()

        /**
         * This function will handle the 'next' button click
         */
        fun onCreateServiceButtonClicked()
    }

}