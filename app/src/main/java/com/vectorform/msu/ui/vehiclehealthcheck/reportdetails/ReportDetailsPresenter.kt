package com.vectorform.msu.ui.vehiclehealthcheck.reportdetails

import android.content.Context
import androidx.core.content.ContextCompat
import com.vectorform.msu.R
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.ui.vehiclehealthcheck.fullimage.FullImageModel
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.SectionStatusEnum

class ReportDetailsPresenter(
    private val context: Context,
    private val view: ReportDetailsContract.IReportDetailsFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter,
    private val section: VehicleHealthCheckUIModelItem
) : ReportDetailsContract.IReportDetailsPresenter {

    //Region IReportDetailsContract
    override fun viewCreated() {
        view?.updateTitle(section.SectionName)
        view?.updatePercent("${section.Score}%")
        view?.updateIcon(section.Icon)

        val reds =
            section.SubSection.filter { s -> s.Status == SectionStatusEnum.RED.getStatusId() }
        val yellows =
            section.SubSection.filter { s -> s.Status == SectionStatusEnum.YELLOW.getStatusId() }
        val greens =
            section.SubSection.filter { s -> s.Status == SectionStatusEnum.GREEN.getStatusId() }

        if (reds.isNotEmpty()) {
            view?.setHeaderBackground(R.drawable.red_report_bg)
            view?.applyColor(ContextCompat.getColor(context, R.color.app_red))
        } else if (yellows.isNotEmpty()) {
            view?.setHeaderBackground(R.drawable.yellow_report_bg)
            view?.applyColor(ContextCompat.getColor(context, R.color.app_yellow))
        } else if (greens.isNotEmpty()) {
            view?.setHeaderBackground(R.drawable.green_report_bg)
            view?.applyColor(ContextCompat.getColor(context, R.color.app_green))
        } else {
            view?.setHeaderBackground(R.drawable.red_report_bg)
            view?.applyColor(ContextCompat.getColor(context, R.color.app_red))
        }
        view?.bindDetails(section.SubSection)

        val images = ArrayList<String>()
        for (s in section.SubSection) {
            s.Assets?.let { assets ->
                if (assets.isNotEmpty()) {
                    images.addAll(assets)
                }
            }
        }
        if (images.isNotEmpty()) {
            view?.bindImages(images)
        } else {
            view?.hideImageContainer()
        }
    }

    override fun onBackClicked() {
        router?.popBack()
    }

    override fun onImageItemClicked(images: List<String>, selectedIndex: Int) {
        router.navigateToFullImageView(FullImageModel(images, selectedIndex, false))
    }
    //endregion


}