package com.vectorform.msu.ui.vehiclehealthcheck.uidata

import java.io.Serializable

data class RFields(
    val Radios: List<String>,
    val Checkbox: String? = null,
    val Hint: String? = null
) : Serializable