package com.vectorform.msu.ui.home.createservice

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ItemCreateServiceDetailsCellBinding

class CreateServiceDetailsRecyclerAdapter(
    val context: Context,
    var itemList: List<ServiceDetailsModel>
) : RecyclerView.Adapter<CreateServiceDetailsRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ItemCreateServiceDetailsCellBinding =
            ItemCreateServiceDetailsCellBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ItemCreateServiceDetailsCellBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.apply {
                imageServiceDetailsItemIcon.setImageResource(itemList[position].icon)
                textServiceDetailsItemName.text = itemList[position].name

                when (itemList[position].isSelected) {
                    true -> {
                        imageServiceDetailsItemCheck.setImageResource(R.drawable.ic_checked_blue)
                    }
                    false -> {
                        imageServiceDetailsItemCheck.setImageResource(R.drawable.ic_unchecked_blue)
                    }
                }

                root.setOnClickListener {
                    itemList[position].isSelected = !itemList[position].isSelected
                    notifyDataSetChanged()
                }
            }
        }
    }
}