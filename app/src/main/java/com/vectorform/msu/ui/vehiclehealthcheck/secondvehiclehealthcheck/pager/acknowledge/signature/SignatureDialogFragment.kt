package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.acknowledge.signature

import android.content.pm.ActivityInfo
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.fcaindia.drive.utils.CommonUtils
import com.vectorform.msu.R
import com.vectorform.msu.databinding.FragmnetSignatureDialogBinding
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.views.SignatureView

class SignatureDialogFragment : DialogFragment(), SignatureDialogContract.ISignatureDialogFragment,
    View.OnClickListener, SignatureView.ISignatureEvents {

    private var binding: FragmnetSignatureDialogBinding? = null
    private var presenter: SignatureDialogPresenter? = null
    private lateinit var listener: ISignatureDialogFragment
    private var capturedSignature: String? = null

    fun setListener(listener: ISignatureDialogFragment, capturedSignature: String?) {
        this.listener = listener
        this.capturedSignature = capturedSignature
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = SignatureDialogPresenter(
                this,
                capturedSignature,
                activity as VehicleHealthCheckActivity
            )
            setStyle(STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.let {
            it.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }
    }

    override fun onPause() {
        super.onPause()
        activity?.let {
            it.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmnetSignatureDialogBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            buttonSignatureAccept.setOnClickListener(this@SignatureDialogFragment)
            buttonSignatureClear.setOnClickListener(this@SignatureDialogFragment)
            imageSignatureClose.setOnClickListener(this@SignatureDialogFragment)
            signatureDialogView.setSignatureEventListener(this@SignatureDialogFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                buttonSignatureAccept -> {
                    presenter?.onAcceptClicked()
                }
                buttonSignatureClear -> {
                    presenter?.onClearClicked()
                }
                imageSignatureClose -> {
                    presenter?.onCloseClicked()
                }
            }
        }
    }
    //endregion

    //Region ISignatureDialogFragment
    override fun showCapturedSignature(signature: String?) {
        binding?.apply {
            signature?.let { bitmap ->
                signatureDialogView.background = BitmapDrawable(resources, bitmap)
            }
        }
    }

    override fun dismissSignatureDialog() {
        binding?.apply {
            dismiss()
        }
    }

    override fun sendBitmapToListener() {
        activity?.let { safeContext ->
            binding?.apply {
                val bitmap = CommonUtils.getBitmapFromViewBackground(signatureDialogView)
                val file = CommonUtils.createImageFile(safeContext)
                CommonUtils.saveSignature(bitmap!!, file)
                bitmap?.let { signature ->
                    listener.signatureCaptured(file.absolutePath)
                    dismiss()
                }
            }
        }
    }

    override fun clearSignatureView() {
        binding?.apply {
            signatureDialogView.clear()
            signatureDialogView.setBackgroundResource(R.color.white)
            listener.clearSignature()
            buttonSignatureAccept.isEnabled = false
        }
    }
    //endregion

    //SignatureView.ISignature Events
    override fun onSigning() {
        binding?.apply {
            buttonSignatureAccept.isEnabled = true
        }
    }
    //endregion


    //Interface to send callback to parent Activity or Fragment
    interface ISignatureDialogFragment {
        /**
         * This function will pass the captured signature to parent view
         * @param signature : String(local path to captured signature)
         */
        fun signatureCaptured(signature: String)

        /**
         * This function will pass the clear signature event to parent
         */
        fun clearSignature()
    }
}