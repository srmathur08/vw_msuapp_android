package com.vectorform.msu.ui.home.dashboard

import com.vectorform.msu.data.responses.vf.SRsByDateResponse

interface IDashBoardServiceRecylcerAdapter {

    /**
     * This function will pass the service item to the view to handle clicked item in service recycler
     * @param service : SRsByDateResponse.ServiceRequest
     */
    fun onServiceItemClicked(service: SRsByDateResponse.ServiceRequest)
}