package com.vectorform.msu.ui.home.workinprogress

import android.location.Location
import com.google.android.gms.common.api.ResolvableApiException
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.utils.BaseFragmentContract

interface WorkInProgressContract {

    interface IWorkInProgressFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will bind the service complains in UI
         * @param serviceComplains : List<ServiceComplainModel>
         */
        fun bindComplains(serviceComplains: List<ServiceComplainModel>)

        /**
         * This function will handle the views to represent vehicle health check completed status
         */
        fun vehicleHealthCheckCompleted()

        /**
         * This function will handle the views to represent vehicle health check not completed status
         */
        fun vehicleHealthCheckNotCompleted()

        /**
         * This function will handle the views to represent vehicle complains completed status
         */
        fun complainCompleted()

        /**
         * This function will handle the views to represent vehicle complains not completed status
         */
        fun complainNotCompleted()

        /**
         * This function will update the userInfo in UI from OnGoing Service instance
         * @param workInProgress : SRsByDateResponse.ServiceRequest
         */
        fun bindUserInfo(workInProgress: SRsByDateResponse.ServiceRequest)

        /**
         * This function will draw route to Customer Address on Google Maps
         * @param latitude : Double
         * @param longitude : Double
         * @param address : String
         * @param location : Location (Logged in user location)
         */
        fun drawRouteToCustomerLocation(
            latitude: Double,
            longitude: Double,
            address: String,
            location: Location?
        )

        fun launchGoogleMapDirections(lat : Double, lng : Double)
    }

    interface IWorkInProgressPresenter {
        /**
         * This function will fetch the service complains
         */
        fun fetchServiceComplains()

        /**
         * This function will handle the close button click
         */
        fun onCloseClicked()

        /**
         * This function will handle the click on service complains
         * @param complain : ServiceComplainModel
         */
        fun onComplainCheckChanged(complain: ArrayList<ServiceComplainModel>)

        /**
         * This function will handle the View service History click
         */
        fun onViewServiceHistoryClicked()

        /**
         * This function will handle the Vehicle Health Check Button click
         */
        fun onVehicleHealthCheckClicked()

        /**
         * This function will handle the complete service event
         * @param otherService String entered by user
         * @param repairEstimate String entered repair estimate value
         */
        fun serviceCompleted(otherService : String?, repairEstimate : String?)

        /**
         * This function will handle the onResume life cycle call back of view
         */
        fun onResumed()

        /**
         * This function will fetch customer location to show on maps
         * @param location : Location logged in user location
         */
        fun onMapReady(location: Location?)

        /**
         * This function will handle resolution for Location
         * @param exception : ResolvableApiException
         */
        fun handleLocationResolution(exception: ResolvableApiException)

        /**
         * This function will handle click for service complains
         */
        fun onServiceComplainClicked()

        fun onNavigateToUserLocationClicked()

    }
}