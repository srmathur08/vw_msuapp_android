package com.vectorform.msu.ui.vehiclehealthcheck.vehiclehealthreport

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ItemVehicleHealthReportPercentCellBinding
import com.vectorform.msu.utils.SectionStatusEnum

class VehicleHealthReportSectionRecyclerAdapter(
    val context: Context,
    var itemList: List<VehicleHealthReportSectionModel>,
    val listener: IVehicleHealthReportSectionRecyclerAdapter
) : RecyclerView.Adapter<VehicleHealthReportSectionRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ItemVehicleHealthReportPercentCellBinding =
            ItemVehicleHealthReportPercentCellBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ItemVehicleHealthReportPercentCellBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding?.apply {
                imageVHRSectionIcon.setImageResource(itemList[position].drawable)
                textVHRSectionTitle.text = itemList[position].section
                when (itemList[position].showPercentSign) {
                    true -> {
                        textVHRSectionPercent.text = "${itemList[position].percentage}%"
                        when (itemList[position].status) {
                            SectionStatusEnum.RED -> {
                                textVHRSectionPercent.setTextColor(
                                    ContextCompat.getColor(
                                        context,
                                        R.color.app_red
                                    )
                                )
                            }
                            SectionStatusEnum.YELLOW -> {
                                textVHRSectionPercent.setTextColor(
                                    ContextCompat.getColor(
                                        context,
                                        R.color.app_yellow
                                    )
                                )
                            }
                            SectionStatusEnum.GREEN -> {
                                textVHRSectionPercent.setTextColor(
                                    ContextCompat.getColor(
                                        context,
                                        R.color.app_green
                                    )
                                )
                            }
                            else -> {
                                textVHRSectionPercent.setTextColor(
                                    ContextCompat.getColor(
                                        context,
                                        R.color.app_red
                                    )
                                )
                            }
                        }
                    }
                    false -> {
                        textVHRSectionPercent.text = "${itemList[position].percentage}"
                        textVHRSectionPercent.setTextColor(
                            ContextCompat.getColor(
                                context,
                                R.color.app_green
                            )
                        )
                    }
                }

                root.setOnClickListener {
                    listener.onItemClicked(itemList[position].section)
                }
            }
        }
    }

    /**
     * Interface for passing events to parent
     */
    interface IVehicleHealthReportSectionRecyclerAdapter {
        /**
         * This function will pass the clicked section name to parent
         * @param sectionName : String
         */
        fun onItemClicked(sectionName: String)
    }
}