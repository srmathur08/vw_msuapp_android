package com.vectorform.msu.ui.login.loginfrag

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vectorform.msu.R
import com.vectorform.msu.databinding.FragmentLoginBinding
import com.vectorform.msu.ui.login.LoginActivity
import com.vectorform.msu.utils.BaseFragment

class LoginFragment : BaseFragment(), LoginContract.ILoginFragment, View.OnClickListener,
    TextWatcher {

    private var binding: FragmentLoginBinding? = null
    private var presenter: LoginContract.ILoginPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = LoginPresenter(safeContext, this, safeContext as LoginActivity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initiateListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    /**
     * This function will initialize the listeners for the view
     */
    private fun initiateListeners() {
        binding?.apply {
            buttonLoginLogin.setOnClickListener(this@LoginFragment)
            buttonLoginOTP.setOnClickListener(this@LoginFragment)
            imageLoginPasswordToggle.setOnClickListener(this@LoginFragment)
            linkLoginOtpResend.setOnClickListener(this@LoginFragment)

            inputLoginMobile.addWatcher(this@LoginFragment)
            inputLoginPassword.addWatcher(this@LoginFragment)

            layoutLoginOtpContainer.setOnTouchListener { v, event ->
                true
            }
        }
    }

    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                buttonLoginOTP -> {
                    presenter?.onLoginClicked(inputLoginMobile.value, inputLoginPassword.value)
                }
                buttonLoginLogin -> {
                    presenter?.submitOTP(inputLoginOtp.value, inputLoginMobile.value)
                }
                imageLoginPasswordToggle -> {
                    presenter?.onTogglePasswordText()
                }
                linkLoginOtpResend->{
                    presenter?.resendOtp(inputLoginMobile.value)
                }
            }
        }
    }

    override fun afterTextChanged(s: Editable?) {
        binding?.apply {
            if (inputLoginMobile.value.trim().length == 10) {
                Log.e("TransitionShow :: ", "Called")
                layoutLoginContainer.transitionToState(R.id.constraintShowTick)
            } else {
                Log.e("TransitionHide :: ", "Called")
                layoutLoginContainer.transitionToState(R.id.constraintHideTick)
            }
            buttonLoginOTP.isEnabled =
                !TextUtils.isEmpty(inputLoginMobile.value) && !TextUtils.isEmpty(inputLoginPassword.value) && inputLoginMobile.value.trim().length == 10
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }


    //Region ILoginPresenter
    override fun showPasswordError(error: String) {
        binding?.apply {
            inputLoginPassword.setError(error)
        }
    }

    override fun updateTogglePasswordIcon(imageId: Int) {
        binding?.apply {
            imageLoginPasswordToggle.setImageResource(imageId)
        }
    }

    override fun showEnteredPassword() {
        binding?.apply {
            inputLoginPassword.setTransformationMethod(PasswordTransformationMethod())
        }
    }

    override fun hideEnteredPassword() {
        binding?.apply {
            inputLoginPassword.setTransformationMethod(null)
        }
    }

    override fun showOtpError(otpErrorMessage: String) {
        binding?.apply {
            inputLoginOtp.setError(otpErrorMessage)
        }
    }

    override fun showOTPView() {
        binding?.apply {
            layoutLoginContainer.transitionToState(R.id.constraintLoginOtpView)
        }
    }

    override fun enableResendOTP() {
        binding?.apply {
            linkLoginOtpResend.isEnabled = true
            linkLoginOtpResend.alpha = 1f
        }
    }

    override fun disableResendOTP() {
        binding?.apply {
            linkLoginOtpResend.isEnabled = false
            linkLoginOtpResend.alpha = .6f
        }
    }

    override fun updateTimerLabel(label: String) {
        binding?.apply {
            textLoginOtpTimer.text = label
        }
    }
    //endregion

}