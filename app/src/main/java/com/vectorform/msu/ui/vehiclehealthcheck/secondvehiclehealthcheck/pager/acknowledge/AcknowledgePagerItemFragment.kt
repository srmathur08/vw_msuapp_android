package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.acknowledge

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vectorform.msu.R
import com.vectorform.msu.databinding.FragmentAcknowledgePagerItemBinding
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.acknowledge.signature.SignatureDialogFragment
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.IVehicleHeathCheckPagerAdapter
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.BaseFragment

class AcknowledgePagerItemFragment : BaseFragment(),
    AcknowledgePagerItemContract.IAcknowledgePagerItemFragment, View.OnClickListener {

    private var binding: FragmentAcknowledgePagerItemBinding? = null
    private lateinit var item: VehicleHealthCheckUIModelItem
    private var presenter: AcknowledgePagerItemPresenter? = null
    private lateinit var listener: IVehicleHeathCheckPagerAdapter
    private var sectionIndex: Int = 0

    fun setPage(
        pagerItem: VehicleHealthCheckUIModelItem,
        listener: IVehicleHeathCheckPagerAdapter,
        sectionIndex: Int
    ) {
        this.item = pagerItem
        this.listener = listener
        this.sectionIndex = sectionIndex
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = AcknowledgePagerItemPresenter(
                safeContext,
                item,
                this,
                activity as VehicleHealthCheckActivity
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAcknowledgePagerItemBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                viewAcknowledgeSASignature -> {
                    presenter?.onSaSignatureClicked()
                }
                viewAcknowledgeCustomerSignature -> {
                    presenter?.onCustomerSignatureClicked()
                }
            }
        }
    }//endregion

    //Region IThreeSixtyViewPagerFragment
    override fun updateTitle(title: String) {
        binding?.apply {
            textAcknowledgePagerTitle.text = title
        }
    }

    override fun initSignatureView() {
        binding?.apply {
            viewAcknowledgeSASignature.setOnClickListener(this@AcknowledgePagerItemFragment)
            viewAcknowledgeCustomerSignature.setOnClickListener(this@AcknowledgePagerItemFragment)
        }
    }

    override fun captureSaSignature(signature: String?) {
        binding?.apply {
            val signatureDialog = SignatureDialogFragment()
            signatureDialog.setListener(saSignatureListener, signature)
            signatureDialog.show(childFragmentManager, "SA Signature")
        }
    }

    override fun captureCustomerSignature(signature: String?) {
        binding?.apply {
            val signatureDialog = SignatureDialogFragment()
            signatureDialog.setListener(customerSignatureListener, signature)
            signatureDialog.show(childFragmentManager, "Customer Signature")
        }
    }

    override fun setSaSignature(signature: String) {
        binding?.apply {
            viewAcknowledgeSASignature.background = BitmapDrawable(resources, signature)
        }
    }

    override fun setCustomerSignature(signature: String) {
        binding?.apply {
            viewAcknowledgeCustomerSignature.background = BitmapDrawable(resources, signature)
        }
    }

    override fun clearSaSignature() {
        binding?.apply {
            viewAcknowledgeSASignature.setBackgroundResource(R.color.white)
        }
    }

    override fun clearCustomerSignature() {
        binding?.apply {
            viewAcknowledgeCustomerSignature.setBackgroundResource(R.color.white)
        }
    }
    //endregion

    //Region ISignatureDialogFragment for SA
    /**
     * This is the listener for SA signature capture or clear
     */
    val saSignatureListener = object : SignatureDialogFragment.ISignatureDialogFragment {
        override fun signatureCaptured(signature: String) {
            binding?.apply {
                presenter?.updateSignature(signature, false)
            }
        }

        override fun clearSignature() {
            presenter?.clearSaSignature()
        }
    }
    //endregion

    //Region ISignatureDialogFragment for Customer
    /**
     * This is the listener for Customer signature capture or clear
     */
    val customerSignatureListener = object : SignatureDialogFragment.ISignatureDialogFragment {
        override fun signatureCaptured(signature: String) {
            presenter?.updateSignature(signature, true)
        }

        override fun clearSignature() {
            presenter?.clearCustomerSignature()
        }
    }
    //endregion
}