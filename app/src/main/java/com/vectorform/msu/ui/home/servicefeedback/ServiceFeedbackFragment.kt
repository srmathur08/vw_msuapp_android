package com.vectorform.msu.ui.home.servicefeedback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vectorform.msu.databinding.FragmentServiceFeedbackBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.BaseFragment

class ServiceFeedbackFragment : BaseFragment(), ServiceFeedbackContract.IServiceFeedbackFragment,
    View.OnClickListener {

    private var binding: FragmentServiceFeedbackBinding? = null
    private var presenter: ServiceFeedbackContract.IServiceFeedbackPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = ServiceFeedbackPresenter(safeContext, this, activity as HomeActivity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentServiceFeedbackBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageFeedbackBack.setOnClickListener(this@ServiceFeedbackFragment)
            buttonFeedbackSubmit.setOnClickListener(this@ServiceFeedbackFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageFeedbackBack -> {
                    presenter?.onBackClicked()
                }
                buttonFeedbackSubmit -> {
                    presenter?.onSubmitClicked()
                }
            }
        }
    }
    //endregion

    //Region IServiceFeedbackFragment

    //endregion
}