package com.vectorform.msu.ui.home

import com.vectorform.msu.data.responses.vf.SRsByDateResponse

class HomePresenter(private val activity: HomeActivity) : HomeContract.IHomePresenter {

    private var workInProgress: SRsByDateResponse.ServiceRequest? = null

    //Region IHomePresenter
    override fun setWorkInProgress(wip: SRsByDateResponse.ServiceRequest) {
        workInProgress = wip
    }

    override fun getWorkInProgress(): SRsByDateResponse.ServiceRequest? {
        return workInProgress
    }

    override fun markVehicleHealthCheckCompleted() {
        workInProgress?.IsVCCompleted = true
    }
    //endregion
}