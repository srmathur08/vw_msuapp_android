package com.vectorform.msu.ui.vehiclehealthcheck.vehicleassets

import android.content.Context
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.utils.CommonUtils
import com.vectorform.msu.data.input.CreateVHCAsset
import com.vectorform.msu.data.responses.vf.CreateVHCAssetResponse
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.ui.vehiclehealthcheck.fullimage.FullImageModel
import com.vectorform.msu.utils.ImageCompression

class VehicleAssetsPresenter(
    private val context: Context,
    private val view: VehicleAssetsContract.IVehicleAssetsFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter
) : VehicleAssetsContract.IVehicleAssetsPresenter {


    val vehicleImages = ArrayList<String>()
    override fun onViewCreated() {
        view.bindRecycler(vehicleImages)
    }

    //Region IVehicleAssetsPresenter
    override fun onBackClicked() {
        router.popBack()
    }

    override fun onNextClicked() {
        if (vehicleImages.isNotEmpty()) {
            for (image in vehicleImages) {
                Thread {
                    val input = CreateVHCAsset(
                        CommonUtils.getFileName(image),
                        CommonUtils.convertBase64(image),
                        true,
                        router?.getVehicleChecckUpId()
                    )
                    VfCalls(context).createVHCAsset(
                        input,
                        object : VfCalls.OnResponse<CreateVHCAssetResponse> {
                            override fun onSucess(response: CreateVHCAssetResponse) {
//                            TODO("Not yet implemented")
                            }

                            override fun onFailure(message: String) {
//                            TODO("Not yet implemented")
                            }
                        })
                }.start()
            }
            router.navigateVehicleAssetsToSecondVehicleHealthCheck()
        } else {
            view?.setValidationError("Please capture vehicle images")
        }
    }

    override fun uploadImageClicked() {
        router?.openCameraToTakePhoto()
    }

    override fun processCameraResult(imagePath: String) {
        view?.showLoading()
        ImageCompression(object : ImageCompression.FileCompressed {
            override fun onSuccess(path: String) {
                vehicleImages.add(path)
                view?.notifyAdapter()
                view?.setValidationError("")
                view?.hideLoading()
            }
        }).execute(imagePath)
    }

    override fun onImageItemClicked(images: List<String>, selectedIndex: Int) {
        router?.navigateToFullImageView(FullImageModel(images, selectedIndex, false))
    }
    //endregion
}