package com.vectorform.msu.ui.home.notificationdetails

import android.content.Context
import com.vectorform.msu.ui.home.HomeContract
import com.vectorform.msu.ui.home.notifications.NotificationsContract

class NotificationDetailsPresenter(
    private val context: Context,
    private val view: NotificationDetailsContract.INotificationDetailsFragment,
    private val router: HomeContract.HomeRouter
) : NotificationDetailsContract.INotificationDetailsPresenter {

    private var presenter: NotificationDetailsContract.INotificationDetailsPresenter? = null
    
    override fun onBackClicked() {
        router?.popBack()
    }

    override fun fetchNotifications() {
        view?.bindNotifications()
    }

}