package com.vectorform.msu.ui.vehiclehealthcheck.reportdetails

import androidx.annotation.DrawableRes
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.SubSection
import com.vectorform.msu.utils.BaseFragmentContract

interface ReportDetailsContract {

    interface IReportDetailsFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will update the section title
         * @param title : String
         */
        fun updateTitle(title: String)

        /**
         * This function will update the section icon
         * @param icon : Int Drawable Resource
         */
        fun updateIcon(@DrawableRes icon: Int)

        /**
         * This function will update the section score
         * @param percent : String
         */
        fun updatePercent(percent: String)

        /**
         * This function will update the header background based on section score
         * @param drawableRes : Int Drawable Resource
         */
        fun setHeaderBackground(@DrawableRes drawableRes: Int)

        /**
         * This function will update the text and icon color based on section score
         * @param color : Int Color Resource
         */
        fun applyColor(color: Int)

        /**
         * This function will show the subsection name and value
         * @param subSections : List<SubSection>
         */
        fun bindDetails(subSections: List<SubSection>)

        /**
         * This function will show the section images
         * @param imageList : List<String>
         */
        fun bindImages(imageList: List<String>)

        /**
         * This function will hide the image container if no images for sections
         */
        fun hideImageContainer()
    }

    interface IReportDetailsPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will handle back icon click
         */
        fun onBackClicked()

        /**
         * This function will handle the section image click
         */
        fun onImageItemClicked(images: List<String>, selectedIndex: Int)
    }

}