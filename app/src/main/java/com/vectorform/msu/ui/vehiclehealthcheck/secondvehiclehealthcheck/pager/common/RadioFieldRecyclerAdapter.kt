package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.common

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ItemVehileHealthCheckRadiosBinding

class RadioFieldRecyclerAdapter(
    val context: Context, var itemList: List<String>, var value: String?,
    val parentPosition: Int, val listener: IRadioFieldRecyclerAdapter
) : RecyclerView.Adapter<RadioFieldRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ItemVehileHealthCheckRadiosBinding =
            ItemVehileHealthCheckRadiosBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ItemVehileHealthCheckRadiosBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.apply {
                textRadioOption.text = itemList[position]
                when (value) {
                    itemList[position] -> {
                        imageRadioOption.setImageResource(R.drawable.ic_checked_blue)
                    }
                    else -> {
                        imageRadioOption.setImageResource(R.drawable.ic_unchecked_blue)
                    }
                }

                imageRadioOption.setOnClickListener {
                    value = textRadioOption.text.toString()
                    listener.onValueSelected(textRadioOption.text.toString(), parentPosition)
                    notifyDataSetChanged()
                }
                textRadioOption.setOnClickListener {
                    value = textRadioOption.text.toString()
                    listener.onValueSelected(textRadioOption.text.toString(), parentPosition)
                    notifyDataSetChanged()
                }
            }
        }
    }

    interface IRadioFieldRecyclerAdapter {
        /**
         * This function will pass the selected value to parent
         * @param value : String (The selected option)
         * @param index : Int (Subsection index)
         */
        fun onValueSelected(value: String, index: Int)
    }
}