package com.vectorform.msu.ui.home.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.vectorform.msu.data.responses.vf.Notifications
import com.vectorform.msu.databinding.FragmentNotificationsBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.BaseFragment

class NotificationFragment : BaseFragment(), NotificationsContract.INotificationFragment,
    View.OnClickListener, INotificationRecylcerAdapter {

    private var binding: FragmentNotificationsBinding? = null
    private var presenter: NotificationsContract.INotificationPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = NotificationPresenter(safeContext, this, activity as HomeActivity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        presenter?.fetchNotifications()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageNotificationBack.setOnClickListener(this@NotificationFragment)
        }
    }

    override fun onClick(p0: View?) {
        binding?.apply {
            when(p0){
                imageNotificationBack->{
                    presenter?.onBackClicked()
                }
            }
        }
    }

    //Region INotificationPresenter
    override fun bindNotifications(notifications: List<Notifications>) {
        activity?.let { safeContext->
            binding?.apply {
                val adapter = NotificationRecyclerAdapter(safeContext, notifications, this@NotificationFragment)
                recyclerNotifications.layoutManager = LinearLayoutManager(safeContext)
                recyclerNotifications.adapter = adapter
            }
        }
    }

    override fun showNoData() {
        binding?.apply {
            textNotificationNoData.visibility = View.VISIBLE
        }
    }

    override fun hideNoData() {
        binding?.apply {
            textNotificationNoData.visibility = View.GONE
        }
    }

    override fun onServiceItemClicked(notification: Notifications) {
        presenter?.onNotificationClicked(notification)
    }
    //endregion

}