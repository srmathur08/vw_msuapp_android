package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview

import com.vectorform.msu.data.input.MarkersValueInput
import java.io.Serializable

data class ImageMarkerModel(
    val angle: String,
    val imagePath: String,
    var markers: ArrayList<MarkersValueInput>
) : Serializable
