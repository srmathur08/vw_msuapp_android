package com.vectorform.msu.ui.vehiclehealthcheck.vehicleimagesdetails

import androidx.annotation.DrawableRes
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview.ImageMarkerModel
import com.vectorform.msu.utils.BaseFragmentContract

interface VehicleImagesDetailsContract {

    interface IVehicleImagesDetailsFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will show Customer Signature in UI
         * @param signature : String (local storage path for Customer signature)
         */
        fun showCustomerSignature(signature: String)

        /**
         * This function will show SA Signature in UI
         * @param signature : String (local storage path for SA signature)
         */
        fun showSASignature(signature: String)

        /**
         * This function will show section score
         * @param percent : String
         */
        fun updatePercent(percent: String)

        /**
         * This function will update the header background based on section score
         * @param drawableRes : Int Drawable Resource
         */
        fun setHeaderBackground(@DrawableRes drawableRes: Int)

        /**
         * This function will update the text and icon color based on section score
         * @param color : Int Color Resource
         */
        fun applyColor(color: Int)

        /**
         * This function will show vehicle images
         * @param images : List<String>
         */
        fun setVehicleImages(images: List<String>)

        /**
         * This function will show the Repair/Damage marker in Car view images
         * @param markers : ArrayList<ImageMarkerModel>
         */
        fun setMarkers(markers: ArrayList<ImageMarkerModel>)

        /**
         * This function will update Repair mark counts on vehicle
         * @param repairCountText : String
         */
        fun setRepairCount(repairCountText: String)

        /**
         * This function will update Damage mark counts on vehicle
         * @param repairCountText : String
         */
        fun setDamageCount(repairCountText: String)

        /**
         * This function will update the subTitle
         * @param subTitle : String
         */
        fun updateSubTitle(subTitle : String)
    }

    interface IVehicleImagesDetailsPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will handle back icon click
         */
        fun onBackClicked()

        fun onImageAdded()
    }

}