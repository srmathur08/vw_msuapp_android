package com.vectorform.msu.ui.splash

/**
 * Contract between Splash Presenter and View
 */
interface SplashContract {

    /**
     * Contract to update view from presenter
     */
    interface ISplashActivity {
        /**
         * This function will navigate user from Splash Activity to Login Activity
         */
        fun navigateToLogin()

        /**
         * This function will show alert messages for permission
         * @param message String
         */
        fun showPermissionMessage(message: String)

        /**
         * This function will show alert message for RootedDevice
         * @param message String
         */
        fun showRootedDeviceMessage(message: String)

        /**
         * This function will remove notifications from system tray
         */
        fun removeNotificationFromTray()


        /**
         * This function will redirect logged-in user to HomePage
         */
        fun navigateToHome()

        fun showLoading()

        fun hideLoading()

        fun showForceUpdate(playStoreLink : String)

        fun showConcreteMessage(message : String)
    }

    /**
     * Contract to update presenter from view
     */
    interface ISplashPresenter {
        /**
         * This function will ask user for initial permissions
         */
        fun checkPermissions()

        /**
         * This function will check the permission results
         * @param requestCode Int
         * @param permissions Array<out String>
         * @param grantResults IntArray
         */
        fun onPermissionResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
        )

        /**
         * This function will detect the device is rooted or not
         */
        fun checkRootedDevice()
    }

    /**
     * his tells the router to handle view events
     */
    interface SplashRouter {
    }

}