package com.vectorform.msu.ui.vehiclehealthcheck.vehicleimagesdetails

import android.content.Context
import androidx.core.content.ContextCompat
import com.vectorform.msu.R
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.MarkerEnum

class VehicleImagesDetailsPresenter(
    private val context: Context,
    private val view: VehicleImagesDetailsContract.IVehicleImagesDetailsFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter,
    private val sectionScratchAndDamage: VehicleHealthCheckUIModelItem,
    private val sectionAcknowledge: VehicleHealthCheckUIModelItem
) : VehicleImagesDetailsContract.IVehicleImagesDetailsPresenter {

    //Region IVehicleImagesPresenter
    override fun viewCreated() {
        sectionAcknowledge.CustomerSignature?.let { signature ->
            view.showCustomerSignature(signature)
        }
        sectionAcknowledge.SaSignature?.let { signature ->
            view.showSASignature(signature)
        }
        sectionScratchAndDamage?.let {
            view.updateSubTitle(it.SectionName)
        }

        val score = /*(sectionAcknowledge.score + sectionScratchAndDamage.score)/2*/ 100
        if (score < 50) {
            view?.setHeaderBackground(R.drawable.red_report_bg)
            view?.applyColor(ContextCompat.getColor(context, R.color.app_red))
        } else {
            view?.setHeaderBackground(R.drawable.green_report_bg)
            view?.applyColor(ContextCompat.getColor(context, R.color.app_green))
        }
        if (sectionScratchAndDamage?.ScratchAndDamage.isNotEmpty()) {
            val images = sectionScratchAndDamage?.ScratchAndDamage.map { it.imagePath }
            if (images.isNotEmpty()) {
                view.setVehicleImages(images)
            }



            val allMarkers =
                sectionScratchAndDamage.ScratchAndDamage.map { i -> i.markers.map { it.mark } }
            var repairCount = 0
            var damageCount = 0
            for (m in allMarkers) {
                repairCount += m.filter { it == MarkerEnum.Repair.name }.size
                damageCount += m.filter { it == MarkerEnum.Damage.name }.size
            }
            view.setRepairCount("Scratches ($repairCount)")
            view.setDamageCount("Dents ($damageCount)")

            view.updatePercent("${repairCount + damageCount}")
        }
    }

    override fun onBackClicked() {
        router?.popBack()
    }

    override fun onImageAdded() {
        if (sectionScratchAndDamage?.ScratchAndDamage.isNotEmpty()) {
            view.setMarkers(sectionScratchAndDamage.ScratchAndDamage)
        }
    }
    //endregion


}