package com.vectorform.msu.ui.home.servicehistory

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vectorform.msu.data.responses.sfdc.services.ServiceHistoryData
import com.vectorform.msu.databinding.ServiceHistoryItemBinding
import java.text.SimpleDateFormat

class ServiceHistoryRecyclerAdapter(
    val context: Context,
    val serviceHistory: List<ServiceHistoryData>,
    val itemClickListener: IServiceHistoryRecyclerAdapter
) : RecyclerView.Adapter<ServiceHistoryRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ServiceHistoryItemBinding =
            ServiceHistoryItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        if(serviceHistory.size > 3){
            return 3
        }else{
          return serviceHistory.size
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ServiceHistoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding?.apply {
                var date =
                    SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.S").parse(serviceHistory[position].CreatedDate)
                textSHIDateTime.text = SimpleDateFormat("dd MMMM yyyy | hh:mm a").format(date.time)
                textSHIServiceType.text = serviceHistory[position].RO_Type__c

                root.setOnClickListener {
                    itemClickListener.OnServiceHistoryItemClicked(serviceHistory[position])
                }
            }
        }
    }
}