package com.vectorform.msu.ui.vehiclehealthcheck.vehicleimagesdetails

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.vectorform.msu.databinding.FragmentVehicleImagesBinding
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview.ImageMarkerModel
import com.vectorform.msu.utils.BaseFragment

class VehicleImagesDetailsFragment : BaseFragment(),
    VehicleImagesDetailsContract.IVehicleImagesDetailsFragment, View.OnClickListener {

    private var binding: FragmentVehicleImagesBinding? = null
    private var presenter: VehicleImagesDetailsContract.IVehicleImagesDetailsPresenter? = null
    private val args: VehicleImagesDetailsFragmentArgs by navArgs()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = VehicleImagesDetailsPresenter(
                safeContext,
                this,
                activity as VehicleHealthCheckActivity,
                args.scratchAndDamage,
                args.acknowledge
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVehicleImagesBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageVehicleImagesBack.setOnClickListener(this@VehicleImagesDetailsFragment)
            imageVehicleImagesLeftArrow.setOnClickListener(this@VehicleImagesDetailsFragment)
            imageVehicleImagesRightArrow.setOnClickListener(this@VehicleImagesDetailsFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageVehicleImagesBack -> {
                    presenter?.onBackClicked()
                }
                imageVehicleImagesLeftArrow -> {
                    carVehicleImagesCar.previousImage()
                }
                imageVehicleImagesRightArrow -> {
                    carVehicleImagesCar.nextImage()
                }
            }
        }
    }
    //end region

    //Region IVehicleImagesDetailsFragment
    override fun showCustomerSignature(signature: String) {
        binding?.apply {
            viewVehicleImagesCustomerSignature.background = BitmapDrawable(resources, signature)
        }
    }

    override fun showSASignature(signature: String) {
        binding?.apply {
            viewVehicleImagesSASignature.background = BitmapDrawable(resources, signature)
        }
    }

    override fun updatePercent(percent: String) {
        binding?.apply {
            textVehicleImagesSectionPercent.text = percent
        }
    }

    override fun setHeaderBackground(drawableRes: Int) {
        binding?.apply {
            layoutVehicleImagesHeaderContainer.setBackgroundResource(drawableRes)
        }
    }

    override fun applyColor(color: Int) {
        binding?.apply {
            imageVehicleImagesSectionIcon.setColorFilter(color)
            textVehicleImagesSectionTitle.setTextColor(color)
            textVehicleImagesSectionPercent.setTextColor(color)
        }
    }

    override fun setVehicleImages(images: List<String>) {
        binding?.apply {
            carVehicleImagesCar.post {
                carVehicleImagesCar.setImageList(images)
                carVehicleImagesCar.setContainerHeightWidth(
                    carVehicleImagesCar.width,
                    carVehicleImagesCar.height
                )
            }.apply {
                presenter?.onImageAdded()
            }
        }
    }

    override fun setMarkers(markers: ArrayList<ImageMarkerModel>) {
        binding?.apply {
            carVehicleImagesCar.setMarkers(markers)
        }
    }

    override fun setRepairCount(repairCountText: String) {
        binding?.apply {
            textVehicleImagesMarkRepair.text = repairCountText
        }
    }

    override fun setDamageCount(damageCountText: String) {
        binding?.apply {
            textVehicleImagesMarkDamage.text = damageCountText
        }
    }

    override fun updateSubTitle(subTitle: String) {
        binding?.apply {
            textVehicleImagesSectionTitle.text = subTitle
        }
    }
    //endregion
}