package com.vectorform.msu.ui.vehiclehealthcheck.vehiclehealthreport

import com.vectorform.msu.data.responses.vf.VehicleAssetItem
import com.vectorform.msu.utils.BaseFragmentContract

interface VehicleHealthReportContract {

    interface IVehicleHealthReportFragment : BaseFragmentContract.IBaseFragment {

        /**
         * This function will update the vehicle model name in UI
         * @param modelName : String optional
         */
        fun updateVehicleModelName(modelName : String?)

        /**
         * This function will show the vehicle images in UI
         * @param imageList : List<VehicleAssetItem>
         */
        fun bindVehiclePhotos(imageList: List<VehicleAssetItem>)

        /**
         * This function will show the subsection and its values
         * @param descriptoins : List<VehicleHealthReportDescriptionModel>
         */
        fun bindDescription(descriptoins: List<VehicleHealthReportDescriptionModel>)

        /**
         * This function will show the section and score
         * @param sections : List<VehicleHealthReportSectionModel>
         */
        fun bindSectionAndPercent(sections: List<VehicleHealthReportSectionModel>)
    }

    interface IVehicleHealthReportPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will handle close icon click
         */
        fun onCloseClicked()

        /**
         * This function will handle the click on section to show details
         * @param sectionName: String
         */
        fun onSectionClickedForDetails(sectionName: String)

        /**
         * This function will handle the vehicle asset image click
         */
        fun onImageItemClicked(images: List<String>, selectedIndex: Int)

    }

}