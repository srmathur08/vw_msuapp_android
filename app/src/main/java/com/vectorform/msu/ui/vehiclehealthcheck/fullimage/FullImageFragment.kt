package com.vectorform.msu.ui.vehiclehealthcheck.fullimage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.viewpager.widget.ViewPager
import com.vectorform.msu.databinding.FragmentFullImageBinding
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.utils.BaseFragment

class FullImageFragment : BaseFragment(), FullImageContract.IFullImageFragment,
    View.OnClickListener,
    ViewPager.OnPageChangeListener {

    private var binding: FragmentFullImageBinding? = null
    private var presenter: FullImageContract.IFullImagePresenter? = null
    private val args: FullImageFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = FullImagePresenter(this, activity as VehicleHealthCheckActivity, args)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFullImageBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageFullImageClose.setOnClickListener(this@FullImageFragment)
            pagerFullImageImage.addOnPageChangeListener(this@FullImageFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageFullImageClose -> {
                    presenter?.onCloseClicked()
                }
            }
        }
    }
    //endregion

    //Region IFullImageFragment
    override fun bindImages(images: List<String>, selectedIndex: Int, isDynamicUrl: Boolean) {
        activity?.let { safeContext ->
            binding?.apply {
                val gridAdapter = PagerIndicatorGridAdapter(layoutInflater, images)
                gridFullImageIndicator.adapter = gridAdapter
                gridAdapter.setSelection(selectedIndex)
                gridFullImageIndicator.numColumns = images.size

                val adapter = FullImageRecyclerAdapter(safeContext, images, isDynamicUrl)
                pagerFullImageImage.adapter = adapter
                pagerFullImageImage.currentItem = selectedIndex
            }
        }
    }
    //endregion

    //Region ViewPager PageChangeListener
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        binding?.apply {
            (gridFullImageIndicator.adapter as PagerIndicatorGridAdapter).setSelection(position)
        }
    }

    override fun onPageScrollStateChanged(state: Int) {
    }
    //endregion

}