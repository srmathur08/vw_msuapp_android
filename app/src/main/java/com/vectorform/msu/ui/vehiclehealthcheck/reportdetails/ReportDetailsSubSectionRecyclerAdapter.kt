package com.vectorform.msu.ui.vehiclehealthcheck.reportdetails

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ItemReportDetailsSubSectionCellBinding
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.SubSection
import com.vectorform.msu.utils.SectionStatusEnum

class ReportDetailsSubSectionRecyclerAdapter(val context: Context, var itemList: List<SubSection>) :
    RecyclerView.Adapter<ReportDetailsSubSectionRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ItemReportDetailsSubSectionCellBinding =
            ItemReportDetailsSubSectionCellBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ItemReportDetailsSubSectionCellBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding?.apply {
                textItemKey.text = itemList[position].Name
                textItemValue.text = itemList[position].Value

                itemList[position].Status?.let { status ->
                    when (status) {
                        SectionStatusEnum.GREEN.getStatusId() -> {
                            imageItemStatus.setImageResource(R.drawable.ic_checked_green)
                        }
                        SectionStatusEnum.RED.getStatusId() -> {
                            imageItemStatus.setImageResource(R.drawable.ic_checked_red)
                        }
                        SectionStatusEnum.YELLOW.getStatusId() -> {
                            imageItemStatus.setImageResource(R.drawable.ic_checked_yellow)
                        }
                        else -> {
                            imageItemStatus.setImageResource(R.drawable.ic_checked_green)
                        }
                    }
                } ?: run {
                    imageItemStatus.setImageResource(R.drawable.unchecked_gray)
                }

                when (position % 2) {
                    0 -> {
                        root.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                    }
                    1 -> {
                        root.setBackgroundColor(
                            ContextCompat.getColor(
                                context,
                                R.color.shadow_color
                            )
                        )
                    }
                }

            }
        }
    }
}