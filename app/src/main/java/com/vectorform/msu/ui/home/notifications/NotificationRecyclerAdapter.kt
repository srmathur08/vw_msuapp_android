package com.vectorform.msu.ui.home.notifications

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.fcaindia.drive.utils.CommonUtils
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.vf.Notifications
import com.vectorform.msu.databinding.ItemNotificationBinding
import com.vectorform.msu.databinding.ServiceRecylcerItemBinding
import com.vectorform.msu.utils.Constants

class NotificationRecyclerAdapter(
    val context: Context,
    var dataList: List<Notifications>,
    val itemClickListener: INotificationRecylcerAdapter
) : RecyclerView.Adapter<NotificationRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ItemNotificationBinding =
            ItemNotificationBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ItemNotificationBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding?.apply {

                textItemNotificationTitle.text = dataList[position].Title
                textItemNotificationDescription.text = dataList[position].Description
                textItemNotificationDate.text = CommonUtils.getFormattedDate(dataList[position].CreatedDate)

                when(dataList[position].IsRead){
                    true->{
                        root.alpha = .6f
                    }
                    false->{
                        root.alpha = 1f
                    }
                }

                root.setOnClickListener {
                    when(dataList[position].IsRead){
                        false->{
                            itemClickListener.onServiceItemClicked(dataList[position])
                            notifyDataSetChanged()
                        }
                    }
                }
            }
        }
    }
}