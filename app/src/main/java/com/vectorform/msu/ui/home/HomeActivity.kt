package com.vectorform.msu.ui.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.fcaindia.drive.observables.ObservableObject
import com.fcaindia.drive.utils.CommonUtils
import com.google.android.gms.common.api.ResolvableApiException
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.sfdc.services.ServiceHistoryData
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.databinding.ActivityHomeBinding
import com.vectorform.msu.services.LocationService
import com.vectorform.msu.ui.home.dashboard.DashboardFragmentDirections
import com.vectorform.msu.ui.home.servicehistory.ServiceHistoryFragmentDirections
import com.vectorform.msu.ui.home.workinprogress.WorkInProgressFragment
import com.vectorform.msu.ui.home.workinprogress.WorkInProgressFragmentDirections
import com.vectorform.msu.ui.login.LoginActivity
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.GpsUtils
import com.vectorform.msu.utils.UtilsActivity
import java.util.*
import android.net.Uri
import android.provider.MediaStore
import androidx.core.content.FileProvider
import com.vectorform.msu.ui.home.notifications.NotificationFragmentDirections
import com.vectorform.msu.ui.home.userprofile.UserProfileFragment
import com.vectorform.msu.ui.home.userprofile.UserProfileFragmentDirections
import java.io.File

class HomeActivity : UtilsActivity(), HomeContract.IHomeActivity, HomeContract.HomeRouter,
    Observer, GpsUtils.OnGpsListener {

    private var binding: ActivityHomeBinding? = null
    private var presenter: HomePresenter? = null
    private var navController: NavController? = null
    private var cameraPhotoPath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        presenter = HomePresenter(this)
        binding?.let { safeBinding ->
            navController = findNavController(R.id.nav_host_home_fragment)
            supportActionBar?.hide()
        }
        GpsUtils(this).turnGPSOn(this)
    }

    //Region IHomeRouter
    override fun popBack() {
        navController?.navigateUp()
    }

    override fun navigateDashboardToWorkInProgressDirection(wip: SRsByDateResponse.ServiceRequest) {
        presenter?.setWorkInProgress(wip)
        val action = DashboardFragmentDirections.actionFragmentDashboardToFragmentWorkInProgress()
        navController?.navigate(action)
    }

    override fun navigateWorkInProgressToServiceHistory() {
        val action =
            WorkInProgressFragmentDirections.actionFragmentWorkInProgressToFragmentServiceHistory()
        navController?.navigate(action)
    }

    override fun navigateServiceHistoryToServiceDetails(
        serviceHistory: ServiceHistoryData,
        overAllRating: String?,
        lessRatingPoint: String?
    ) {
        val action =
            ServiceHistoryFragmentDirections.actionFragmentServiceHistoryToFragmentServiceDetails(
                serviceHistory, overAllRating, lessRatingPoint
            )
        navController?.navigate(action)
    }

    override fun navigateDashBoardToFirstVehicleHealthCheck(service: SRsByDateResponse.ServiceRequest) {
        ObservableObject.getInstance()?.addObserver(this)
        startActivity(Intent(this, VehicleHealthCheckActivity::class.java).apply {
            putExtra(Constants.SERVICE_REQUEST_DATA, service)
        })
    }

    override fun navigateWorkInProgressToFirstVehicleHealthCheck() {
        ObservableObject.getInstance()?.addObserver(this)
        presenter?.getWorkInProgress()?.let { serviceData ->
            startActivity(Intent(this, VehicleHealthCheckActivity::class.java).apply {
                putExtra(Constants.SERVICE_REQUEST_DATA, serviceData)

            })
        }
    }

    override fun navigateDashBoardToCreateServiceRequest() {
        val action =
            DashboardFragmentDirections.actionFragmentDashboardToFragmentCreateServiceRequest()
        navController?.navigate(action)
    }

    override fun navigateDashboardToUserProfile() {
        val action = DashboardFragmentDirections.actionFragmentDashboardToFragmentUserProfile()
        navController?.navigate(action)
    }

    override fun navigateToLogin() {
        startActivity(Intent(this, LoginActivity::class.java)).also { finish() }
    }

    override fun getWorkInProgress(): SRsByDateResponse.ServiceRequest? {
        return presenter?.getWorkInProgress()
    }

    override fun showLocationResolution(exception: ResolvableApiException) {
        val intentSenderRequest = IntentSenderRequest.Builder(exception.resolution).build()
        resolutionForResult.launch(intentSenderRequest)
    }

    override fun gpsStatus(isGPSEnable: Boolean) {
        when(isGPSEnable){
            true->{
                LocationService.startService(this, CommonUtils.getString(this, Constants.LOGGED_IN_USER_ID))
            }
        }
    }

    override fun openCameraToTakePhoto() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                val file = CommonUtils.createImageFile(this)
                cameraPhotoPath = file.absolutePath
                val photoFile: File? = try {
                    file
                } catch (ex: Exception) {
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.vectorform.msu.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    resultLauncher.launch(takePictureIntent)
                }
            }
        }
    }

    override fun openGalleryToChoosePhoto() {
        cameraPhotoPath = null
        val mimeTypes = arrayOf("image/*")
        val intent = Intent()
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        intent.action = Intent.ACTION_GET_CONTENT
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
        resultLauncher.launch(intent)
    }

    override fun navigateUserProfileToNotifications() {
        val action = UserProfileFragmentDirections.actionFragmentUserProfileToFragmentNotifications()
        navController?.navigate(action)
    }

    override fun navigateUserNotificationsToNotificationDetails() {
        val action = NotificationFragmentDirections.actionFragmentNotificationToFragmentNotificationDetails()
        navController?.navigate(action)
    }
    //endregion

    /**
     * This will handle the ActivityResult and pass the result to child fragments in navController
     */
    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                cameraPhotoPath?.let { imagePath ->
                    supportFragmentManager?.primaryNavigationFragment?.let { parentNavigation ->
                        parentNavigation.childFragmentManager.primaryNavigationFragment?.let { childFragment ->
                            when (childFragment) {
                                is UserProfileFragment -> {
                                    childFragment.imageChooserReult(imagePath)
                                }
                            }
                        }
                    }
                }?: run{
                    result?.data?.let { data->
                        var _path = CommonUtils.getPathFromIntent(
                            this,
                            data
                        )
                        supportFragmentManager?.primaryNavigationFragment?.let { parentNavigation ->
                            parentNavigation.childFragmentManager.primaryNavigationFragment?.let { childFragment ->
                                when (childFragment) {
                                    is UserProfileFragment -> {
                                        childFragment.imageChooserReult(_path)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    /**
     * This will handle startActivityForResolution result and pass it to calling child in navigation controller
     */
    private val resolutionForResult =
        registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { activityResult ->
            supportFragmentManager?.primaryNavigationFragment?.let { parentNavigation ->
                parentNavigation.childFragmentManager.primaryNavigationFragment?.let { childFragment ->
                    when (childFragment) {
                        is WorkInProgressFragment -> {
                            childFragment.onLocationResolutionResult(activityResult.resultCode == RESULT_OK)
                        }
                        else->{
                            when (activityResult.resultCode == RESULT_OK){
                                true->{
                                    GpsUtils(this).turnGPSOn(this)
                                }
                                false->{
                                    showMessage(
                                        getString(R.string.alert),
                                        "Please enable your location to see route to customer location"
                                    ) { which, dialog -> GpsUtils(this).turnGPSOn(this) }
                                }
                            }
                        }
                    }
                }
            }
        }


    //Region Observable Object
    override fun update(o: Observable?, arg: Any?) {
        arg?.let { presenter?.markVehicleHealthCheckCompleted() }
    }
    //endRegion



}