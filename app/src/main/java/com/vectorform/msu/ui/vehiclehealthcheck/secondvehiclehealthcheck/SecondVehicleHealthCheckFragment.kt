package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.vectorform.msu.databinding.FragmentSecondVehicleHealthCheckBinding
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.IVehicleHeathCheckPagerAdapter
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.BaseFragment

class SecondVehicleHealthCheckFragment : BaseFragment(),
    SecondVehicleHealthCheckContract.ISecondVehicleHealthCheckFragment, View.OnClickListener,
    IVehicleHeathCheckPagerAdapter {

    private var binding: FragmentSecondVehicleHealthCheckBinding? = null
    private var presenter: SecondVehicleHealthCheckContract.ISecondVehicleHealthCheckPresenter? =
        null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = SecondVehicleHealthCheckPresenter(
                safeContext,
                this,
                activity as VehicleHealthCheckActivity
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSecondVehicleHealthCheckBinding.inflate(inflater, container, false)
        presenter?.getHealthCheckList()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageSVHCBack.setOnClickListener(this@SecondVehicleHealthCheckFragment)
            textSVHCBack.setOnClickListener(this@SecondVehicleHealthCheckFragment)
            imageSVHCDirections.setOnClickListener(this@SecondVehicleHealthCheckFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageSVHCBack, textSVHCBack -> {
                    presenter?.onBackClicked(viewpager.currentItem)
                }
                imageSVHCDirections -> {
                    presenter?.onNextClicked(viewpager.currentItem)
                }
            }
        }
    }
    //endregion

    //Region ISecondVehicleHealthCheckFragment
    override fun bindViewPager(itemList: List<VehicleHealthCheckUIModelItem>) {
        binding?.apply {
            val adapter = VehicleHeathCheckPagerAdapter(
                itemList,
                childFragmentManager,
                lifecycle,
                this@SecondVehicleHealthCheckFragment
            )
            viewpager.adapter = adapter
            viewpager.isUserInputEnabled = false

            val tabGridAdapter = PagerTabGridAdapter(layoutInflater, itemList, 0)
            gridSVHCTab.adapter = tabGridAdapter
        }
    }

    override fun bindTab(itemList: List<VehicleHealthCheckUIModelItem>, index: Int) {
        binding?.apply {
            viewpager.setCurrentItem(index, false)
            val tabAdapter = gridSVHCTab.adapter as PagerTabGridAdapter
            tabAdapter.setSelectedIndex(index)
        }
    }

    override fun updateImageInSubSection(
        sectionIndex: Int,
        photoSubSectionIndex: Int
    ) {
        binding?.apply {
            (viewpager.adapter as VehicleHeathCheckPagerAdapter).notify(
                sectionIndex,
                photoSubSectionIndex
            )
        }
    }

    override fun showValidationMessage(message: String) {
        binding?.apply {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }
    }
    //endregion


    //Region Public functions
    /**
     * This function will be invoked from router activity to show the captured image in subsection
     * @param imagePath : String (local path of captured image)
     */
    fun cameraResult(imagePath: String) {
        presenter?.processCameraResult(imagePath)
    }
    //endregion

    //Region IVehicleHealthCheckPagerAdapter
    override fun uploadPhoto(sectionIndex: Int, subSectionIndex: Int) {
        presenter?.uploadPhotoClicked(sectionIndex, subSectionIndex)
    }
    //endregion

}