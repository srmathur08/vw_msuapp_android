package com.vectorform.msu.ui.vehiclehealthcheck.uidata

import androidx.annotation.DrawableRes
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview.ImageMarkerModel
import com.vectorform.msu.utils.SectionStatusEnum
import java.io.Serializable

data class VehicleHealthCheckUIModelItem(
    val Index: Int,
    val SectionName: String,
    @DrawableRes var Icon: Int,
    val SubSection: List<SubSection>,
    var Score: Int,
    var ScoreEnum: SectionStatusEnum,
    var SaSignature: String?,
    var CustomerSignature: String?,
    var ScratchAndDamage: ArrayList<ImageMarkerModel>,
    var VehicleAssets: ArrayList<String>,
    var CleaningRemark: String?,
    var DamageRemark: String?,
    var OffersAndNotes: String?
) : Serializable