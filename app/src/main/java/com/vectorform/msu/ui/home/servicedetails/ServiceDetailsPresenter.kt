package com.vectorform.msu.ui.home.servicedetails

import android.content.Context
import com.vectorform.msu.ui.home.HomeContract
import java.text.SimpleDateFormat

class ServiceDetailsPresenter(
    private val context: Context,
    private val view: ServiceDetailsContract.IServiceDetailsFragment,
    private val router: HomeContract.HomeRouter,
    private val args: ServiceDetailsFragmentArgs
) : ServiceDetailsContract.IServiceDetailsPresenter {

    //Region IServiceDetailsPresenter
    override fun onViewCreated() {
        val service = args.data
        service?.let { data ->

            data.CreatedDate?.let {
                var createdDate = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.S").parse(it)
                view.updateServiceDate(SimpleDateFormat("dd MMMM yyyy").format(createdDate))
            }
            data.RO_Closed_Date__c?.let {
                val closeDate = SimpleDateFormat("yyyy-MM-dd").parse(it)
                view.updateServiceCompletedDate(SimpleDateFormat("dd MMMM yyyy").format(closeDate))
            }
            data?.Repair_Order__c?.let { view.updateRepairOrderNo(it) }
            data.RO_Type__c?.let { view.updateRoType(it) }
            data.Customer_Voice__c?.let { view.updateServiceInstruction(it) }
            data.Amount__c?.let { view.updateInvoice(it) }

//            view.updateServiceEngineer("")
//            view.updateSAInstruction("")
            args?.ratings?.let {
                view.updateRatings(it)
            } ?: run { view.hideRatingFields() }
        }
    }

    override fun onBackClicked() {
        router?.popBack()
    }
    //endregion


}