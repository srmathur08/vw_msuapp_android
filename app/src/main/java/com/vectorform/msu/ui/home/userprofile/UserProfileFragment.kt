package com.vectorform.msu.ui.home.userprofile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.vectorform.msu.BuildConfig
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.vf.LoginResponse
import com.vectorform.msu.databinding.FragmentUserProfileBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.BaseFragment

class UserProfileFragment : BaseFragment(), UserProfileContract.IUserProfileFragment,
    View.OnClickListener {

    private var binding: FragmentUserProfileBinding? = null
    private var presenter: UserProfileContract.IUserProfilePresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = UserProfilePresenter(safeContext, this, activity as HomeActivity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUserProfileBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            presenter?.OnViewCreated()
            imageProfileBack.setOnClickListener(this@UserProfileFragment)
            textProfileLogout.setOnClickListener(this@UserProfileFragment)
            cardProfileIcon.setOnClickListener(this@UserProfileFragment)
            textProfileCameraChooser.setOnClickListener(this@UserProfileFragment)
            textProfileGalleryChooser.setOnClickListener(this@UserProfileFragment)
            imageProfileNotifications.setOnClickListener(this@UserProfileFragment)
            textProfileCacelChooser.setOnClickListener(this@UserProfileFragment)

            layoutChooserContainer.setOnTouchListener { view, motionEvent ->
                true
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageProfileBack -> {
                    presenter?.onBackClicked()
                }
                textProfileLogout -> {
                    presenter?.onLogoutClicked()
                }
                cardProfileIcon, textProfileCacelChooser->{
                    presenter?.onProifleIconClicked()
                }
                textProfileCameraChooser->{
                    presenter?.onOpenCamera()
                }
                textProfileGalleryChooser->{
                    presenter?.onOpenGallery()
                }
                imageProfileNotifications->{
                    presenter?.onNotificationIconClicked()
                }
            }
        }
    }
    //endregion

    //Region IUserProfileFragment
        override fun bindUserData(loginResponse: LoginResponse, mobile : String){
            binding?.apply {
                textProfileName.setValue(loginResponse.Fullname)
                textProfileEmail.setValue(loginResponse.Email)
                textProfileMobile.setValue(mobile)
                textProfileRole.setValue(loginResponse.Role)
                textProfileDealership.setValue(loginResponse.Dealership)
                textVersion.text = "V${BuildConfig.VERSION_NAME}"
            }
        }

    override fun showChooser() {
        binding?.apply {
            layoutProfileContainer.transitionToEnd()
        }
    }

    override fun hideChooser() {
        binding?.apply {
            layoutProfileContainer.transitionToStart()
        }
    }

    override fun showProfileImage(imagePath: String) {
        activity?.let { safeContext->
            binding?.apply {
                Glide.with(safeContext).load(imagePath).skipMemoryCache(true).diskCacheStrategy(
                    DiskCacheStrategy.NONE).into(imageProfileUser)
            }
        }
    }
    //endregion

    //Region Public functions
    /**
     * This function will be invoked from router activity to handle captured image from camera
     * @param imagePath : String (local storage path for image)
     */
    fun imageChooserReult(imagePath: String) {
        presenter?.processCameraResult(imagePath)
    }
    //end region
}