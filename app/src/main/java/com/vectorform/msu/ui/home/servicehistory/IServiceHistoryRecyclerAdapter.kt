package com.vectorform.msu.ui.home.servicehistory

import com.vectorform.msu.data.responses.sfdc.services.ServiceHistoryData

interface IServiceHistoryRecyclerAdapter {
    /**
     * This function will pass the Service History Item to View when clicked on card inside Recycler view
     * @param service : ServiceHistoryData
     */
    fun OnServiceHistoryItemClicked(service: ServiceHistoryData)
}