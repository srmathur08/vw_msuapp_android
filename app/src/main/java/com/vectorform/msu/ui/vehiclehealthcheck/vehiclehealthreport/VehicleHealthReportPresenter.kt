package com.vectorform.msu.ui.vehiclehealthcheck.vehiclehealthreport

import android.content.Context
import android.content.DialogInterface
import com.fcaindia.drive.network.VfCalls
import com.fcaindia.drive.observables.ObservableObject
import com.fcaindia.drive.utils.CommonUtils
import com.vectorform.msu.R
import com.vectorform.msu.data.input.MarkVehicleHealthCheckCompletedInput
import com.vectorform.msu.data.input.SRsBySRIdInput
import com.vectorform.msu.data.responses.vf.SRMarkResponse
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.data.responses.vf.VehicleCheckUpDataBySrIdResponse
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.ui.vehiclehealthcheck.fullimage.FullImageModel
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.Logger
import com.vectorform.msu.utils.MarkerEnum
import com.vectorform.msu.utils.SectionStatusEnum
import java.text.SimpleDateFormat
import java.util.*

class VehicleHealthReportPresenter(
    private val context: Context,
    private val view: VehicleHealthReportContract.IVehicleHealthReportFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter,
    private val args: VehicleHealthReportFragmentArgs
) : VehicleHealthReportContract.IVehicleHealthReportPresenter {

    private var serviceData: SRsByDateResponse.ServiceRequest? = null

    init {
        serviceData = router?.getServiceRequestMasterData()
    }

    //Region IVehicleHealthReportPresenter
    override fun viewCreated() {

        serviceData?.let { data ->
            view?.showLoading()
            view?.updateVehicleModelName(data.VehicleModel)

            VfCalls(context).getVehicleCheckUpDataBySRId(SRsBySRIdInput(data.Srid), object :
                VfCalls.OnResponse<VehicleCheckUpDataBySrIdResponse> {
                override fun onSucess(response: VehicleCheckUpDataBySrIdResponse) {
                    if (response.VehicleCheckup.isNotEmpty() && response.VehicleCheckup[0].VehicleCheckupAssets.isNotEmpty()) {
                        view?.bindVehiclePhotos(response.VehicleCheckup[0].VehicleCheckupAssets)
                    }
                    markVehicleHealthCheckCompleted()
                }

                override fun onFailure(message: String) {
                    Logger.log("VHC DATA :: ", "${serviceData?.Srid ?: ""} $message")
                    view.hideLoading()
                }
            })
        }
        val description = listOf(
            VehicleHealthReportDescriptionModel(
                R.string.text_date,
                SimpleDateFormat("dd MMM yyyy").format(Date().time)
            ),
            VehicleHealthReportDescriptionModel(R.string.text_ro_, serviceData?.Srnumber),
            VehicleHealthReportDescriptionModel(
                R.string.text_customer_name,
                serviceData?.CustomerName
            ),
            VehicleHealthReportDescriptionModel(R.string.text_registration_no, serviceData?.RegNum),
            VehicleHealthReportDescriptionModel(R.string.text_vin, serviceData?.Vinnumber),
            VehicleHealthReportDescriptionModel(
                R.string.text_fuel_level,
                "${router?.getFuelLevel()}%"
            ),
            VehicleHealthReportDescriptionModel(R.string.text_service_advisor, CommonUtils.getString(context, Constants.LOGGED_IN_USER_FULLNAME))
        )
        view?.bindDescription(description)

        var scratchDamageCount = 0
        val scratchAndDamage =
            args.item.filter { i -> i.SectionName.equals(Constants.SECTION_SCRATCH_DENT_AND_DAMAGE) }
        if (scratchAndDamage.isNotEmpty()) {
            val allMarkers =
                scratchAndDamage[0].ScratchAndDamage.map { i -> i.markers.map { it.mark } }
            var repairCount = 0
            var damageCount = 0
            for (m in allMarkers) {
                repairCount += m.filter { it == MarkerEnum.Repair.name }.size
                damageCount += m.filter { it == MarkerEnum.Damage.name }.size
            }
            scratchDamageCount = repairCount + damageCount
        }

        val sections = listOf(
            VehicleHealthReportSectionModel(
                args.item[0].Icon,
                args.item[0].SectionName,
                args.item[0].Score.toString(),
                args.item[0].ScoreEnum,
                true
            ),
            VehicleHealthReportSectionModel(
                args.item[1].Icon,
                args.item[1].SectionName,
                args.item[1].Score.toString(),
                args.item[1].ScoreEnum,
                true
            ),
            VehicleHealthReportSectionModel(
                args.item[2].Icon,
                args.item[2].SectionName,
                args.item[2].Score.toString(),
                args.item[2].ScoreEnum,
                true
            ),
            VehicleHealthReportSectionModel(
                args.item[3].Icon,
                args.item[3].SectionName,
                args.item[3].Score.toString(),
                args.item[3].ScoreEnum,
                true
            ),
            VehicleHealthReportSectionModel(
                args.item[4].Icon,
                args.item[4].SectionName,
                args.item[4].Score.toString(),
                args.item[4].ScoreEnum,
                true
            ),
            VehicleHealthReportSectionModel(
                args.item[5].Icon,
                args.item[5].SectionName,
                args.item[5].Score.toString(),
                args.item[5].ScoreEnum,
                true
            ),
            VehicleHealthReportSectionModel(
                R.drawable.ic_vehicle_images,
                Constants.JOIN_SECTION_VEHICLE_IMAGES,
                "$scratchDamageCount",
                SectionStatusEnum.GREEN,
                false
            )
        )
        view?.bindSectionAndPercent(sections)
    }

    override fun onCloseClicked() {
        serviceData?.let { data->
            view?.showLoading()
            VfCalls(context).SendVHCSms(SRsBySRIdInput(data.Srid), object : VfCalls.OnResponse<SRMarkResponse>{
                override fun onSucess(response: SRMarkResponse) {
                    view?.hideLoading()
                    view?.showCustomMessage(R.string.alert, "Vehicle Health Check sent successfully"
                    ) { p0, p1 -> router?.closeActivity()}
                }

                override fun onFailure(message: String) {
                    view?.hideLoading()
//                    view?.showCustomMessage(R.string.alert, message, null)
                    view?.showCustomMessage(R.string.alert, "Vehicle Health Check sent successfully"
                    ) { p0, p1 -> router?.closeActivity()}
                }
            })
        }
    }

    override fun onSectionClickedForDetails(sectionName: String) {
        when (sectionName) {
            Constants.JOIN_SECTION_VEHICLE_IMAGES -> {
                val scratchAndDamage =
                    args.item.filter { i -> i.SectionName.equals(Constants.SECTION_SCRATCH_DENT_AND_DAMAGE) }
                val acknowledge =
                    args.item.filter { i -> i.SectionName.equals(Constants.SECTION_ACKNOWLEDGE) }
                if (scratchAndDamage.isNotEmpty() && acknowledge.isNotEmpty()) {
                    router?.navigateReportsToVehicleImages(scratchAndDamage[0], acknowledge[0])
                }
            }
            else -> {
                val section = args.item.filter { i -> i.SectionName.equals(sectionName, true) }
                if (section.isNotEmpty()) {
                    router?.navigateReportsToDetailedReport(section[0])
                }
            }
        }
    }

    override fun onImageItemClicked(images: List<String>, selectedIndex: Int) {
        router.navigateToFullImageView(FullImageModel(images, selectedIndex, true))
    }
    //endregion


    /**
     * This function will mark vehicle health check completed flag on server
     */
    private fun markVehicleHealthCheckCompleted() {
        view?.showLoading()
        serviceData?.let {wip->
            VfCalls(context).markVehicleHealthCheckCompleted(MarkVehicleHealthCheckCompletedInput(router.getVehicleChecckUpId()), object : VfCalls.OnResponse<SRMarkResponse>{
                override fun onSucess(response: SRMarkResponse) {
                    router.getServiceRequestMasterData()?.IsVCCompleted = true
                    ObservableObject.getInstance()?.updateValue(true)
                    view.hideLoading()
                }

                override fun onFailure(message: String) {
                    Logger.log("VehiclleHealthCompleteStatusFailure ::", message)
                    view.hideLoading()
                }
            })
        }
    }
}