package com.vectorform.msu.ui.vehiclehealthcheck

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.fcaindia.drive.utils.CommonUtils
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.databinding.ActivityVehicleHealthCheckBinding
import com.vectorform.msu.ui.vehiclehealthcheck.firstvehicleheathcheck.FirstVehicleHealthCheckFragmentDirections
import com.vectorform.msu.ui.vehiclehealthcheck.fullimage.FullImageFragmentDirections
import com.vectorform.msu.ui.vehiclehealthcheck.fullimage.FullImageModel
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.SecondVehicleHealthCheckFragment
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.SecondVehicleHealthCheckFragmentDirections
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModel
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.ui.vehiclehealthcheck.vehicleassets.VehicleAssetsFragment
import com.vectorform.msu.ui.vehiclehealthcheck.vehicleassets.VehicleAssetsFragmentDirections
import com.vectorform.msu.ui.vehiclehealthcheck.vehiclehealthreport.VehicleHealthReportFragmentDirections
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.UtilsActivity
import java.io.File

class VehicleHealthCheckActivity : UtilsActivity(),
    VehicleHealthCheckContract.IVehicleHealthCheckActivity,
    VehicleHealthCheckContract.VehicleHealthCheckRouter {

    private var binding: ActivityVehicleHealthCheckBinding? = null
    private var presenter: VehicleHealthCheckPresenter? = null
    private var navController: NavController? = null

    private var cameraPhotoPath: String? = null
    private var serviceData: SRsByDateResponse.ServiceRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intentData = intent.getSerializableExtra(Constants.SERVICE_REQUEST_DATA)
        intentData?.let {
            serviceData = intentData as SRsByDateResponse.ServiceRequest
        }

        binding = ActivityVehicleHealthCheckBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        binding?.apply {
            navController = findNavController(R.id.nav_host_vehicle_health_check_fragment)
            supportActionBar?.hide()
            presenter = VehicleHealthCheckPresenter(
                this@VehicleHealthCheckActivity,
                this@VehicleHealthCheckActivity,
                serviceData?.Srid ?: 0
            )
            presenter?.viewCreated()
        }
    }

    //Region IVehicleHealthCheckRouter
    override fun popBack() {
        navController?.navigateUp()
    }

    override fun closeActivity() {
        finish()
    }

    override fun navigateFirstVehicleHealthCheckToVehicleAsets(
        vehicleCheckupId: Int,
        fuelLevelPercent: Int
    ) {
        presenter?.setVehicleChecpId(vehicleCheckupId)
        presenter?.setFuelLevel(fuelLevelPercent.toString())

        val action =
            FirstVehicleHealthCheckFragmentDirections.actionNavigationFirstVehicleHealthCheckToNavigationVehicleAssets()
        navController?.navigate(action)
    }

    override fun navigateVehicleAssetsToSecondVehicleHealthCheck() {
        val action =
            VehicleAssetsFragmentDirections.actionNavigationVehicleAssetsToNavigationSecondVehicleHealthCheck()
        navController?.navigate(action)
    }


    override fun navigateVehicleHealthCheckToVehicleHealthReport(
        itemList: ArrayList<VehicleHealthCheckUIModelItem>,
    ) {

        val model = VehicleHealthCheckUIModel()
        model.addAll(itemList)
        val action =
            SecondVehicleHealthCheckFragmentDirections.actionNavigationSecondVehicleHealthCheckToNavigationVehcileHealthReport(
                model
            )
        navController?.navigate(action)
    }

    override fun navigateReportsToDetailedReport(
        section: VehicleHealthCheckUIModelItem
    ) {

        val action =
            VehicleHealthReportFragmentDirections.actionNavigationVehcileHealthReportToNavigationVehcileHealthReportDetails(
                section
            )
        navController?.navigate(action)
    }

    override fun navigateReportsToVehicleImages(
        scratchAndDamage: VehicleHealthCheckUIModelItem,
        acknowledge: VehicleHealthCheckUIModelItem
    ) {

        val action =
            VehicleHealthReportFragmentDirections.actionNavigationVehcileHealthReportToNavigationVehcileImagesDetails(
                scratchAndDamage,
                acknowledge
            )
        navController?.navigate(action)
    }

    override fun navigateToFullImageView(fullImageModel: FullImageModel) {
        val action = FullImageFragmentDirections.globalFullImageNavigation(fullImageModel)
        navController?.navigate(action)
    }

    override fun getServiceRequestMasterData(): SRsByDateResponse.ServiceRequest? {
        return serviceData
    }

    override fun getVehicleChecckUpId(): Int {
        return presenter?.getVehicleChecckUpId() ?: 0
    }

    override fun getFuelLevel(): String {
        return presenter?.getFuelLevel() ?: ""
    }

    override fun openCameraToTakePhoto() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                val file = CommonUtils.createImageFile(this)
                cameraPhotoPath = file.absolutePath
                val photoFile: File? = try {
                    file
                } catch (ex: Exception) {
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.vectorform.msu.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    resultLauncher.launch(takePictureIntent)
                }
            }
        }
    }
    //endregion

    /**
     * This will handle the ActivityResult and pass the result to child fragments in navController
     */
    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                cameraPhotoPath?.let { imagePath ->

                    supportFragmentManager?.primaryNavigationFragment?.let { parentNavigation ->
                        parentNavigation.childFragmentManager.primaryNavigationFragment?.let { childFragment ->
                            when (childFragment) {
                                is VehicleAssetsFragment -> {
                                    childFragment.cameraResult(imagePath)
                                }
                                is SecondVehicleHealthCheckFragment -> {
                                    childFragment.cameraResult(imagePath)
                                }
                            }
                        }
                    }
                }
            }
        }

    override fun onBackPressed() {
//        super.onBackPressed()
    }

}