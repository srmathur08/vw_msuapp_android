package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.common

import android.content.Context
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.ui.vehiclehealthcheck.fullimage.FullImageModel
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem

class VehicleHealthCheckPagerPresenter(
    private val context: Context, private val dataItem: VehicleHealthCheckUIModelItem,
    private val view: VehicleHealthCheckPagerContract.IVehicleHealthCheckPagerFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter
) : VehicleHealthCheckPagerContract.IVehicleHealthCheckPagerPresenter {

    //Region IVehicleHealthCheckPagerPresenter
    override fun viewCreated() {
        view?.updateTitle(dataItem.SectionName)
        view?.binRecycler(dataItem.SubSection)
    }

    override fun onImageItemClicked(images: List<String>, selectedIndex: Int) {
        router?.navigateToFullImageView(FullImageModel(images, selectedIndex, false))
    }
    //endregion


}