package com.vectorform.msu.ui.vehiclehealthcheck.fullimage

import java.io.Serializable

data class FullImageModel(
    val images: List<String>,
    val selectedIndex: Int,
    val dynamicUrl: Boolean
) : Serializable