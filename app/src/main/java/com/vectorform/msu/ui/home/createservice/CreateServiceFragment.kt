package com.vectorform.msu.ui.home.createservice

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.vectorform.msu.databinding.FragmentCreateServiceRequestBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.BaseFragment

class CreateServiceFragment : BaseFragment(), CreateServiceContract.ICreateServiceFragment,
    View.OnClickListener {

    private var binding: FragmentCreateServiceRequestBinding? = null
    private var presenter: CreateServiceContract.ICreateServicePresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = CreateServicePresenter(safeContext, this, activity as HomeActivity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCreateServiceRequestBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageNewServiceClose.setOnClickListener(this@CreateServiceFragment)
            buttonNewServiceCreate.setOnClickListener(this@CreateServiceFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageNewServiceClose -> {
                    presenter?.onCloseClicked()
                }
                buttonNewServiceCreate -> {
                    presenter?.onCreateServiceButtonClicked()
                }
            }
        }
    }

    override fun bindServiceDetailsItem(itemList: List<ServiceDetailsModel>) {
        activity?.let { safeContext ->
            binding?.apply {
                val adapter = CreateServiceDetailsRecyclerAdapter(safeContext, itemList)
                recyclerNewServiceDetails.layoutManager = GridLayoutManager(safeContext, 2)
                recyclerNewServiceDetails.adapter = adapter
            }
        }
    }
    //endregion
}