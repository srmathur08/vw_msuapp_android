package com.vectorform.msu.ui.vehiclehealthcheck.vehiclehealthreport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.vf.VehicleAssetItem
import com.vectorform.msu.databinding.FragmentVehicleHealthReportBinding
import com.vectorform.msu.ui.vehiclehealthcheck.IVehicleImageClicked
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.utils.BaseFragment


class VehicleHealthReportFragment : BaseFragment(),
    VehicleHealthReportContract.IVehicleHealthReportFragment, View.OnClickListener,
    VehicleHealthReportSectionRecyclerAdapter.IVehicleHealthReportSectionRecyclerAdapter,
    IVehicleImageClicked {

    private var binding: FragmentVehicleHealthReportBinding? = null
    private var presenter: VehicleHealthReportContract.IVehicleHealthReportPresenter? = null
    private val args: VehicleHealthReportFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = VehicleHealthReportPresenter(
                safeContext,
                this,
                activity as VehicleHealthCheckActivity,
                args
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVehicleHealthReportBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageVHRClose.setOnClickListener(this@VehicleHealthReportFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageVHRClose -> {
                    presenter?.onCloseClicked()
                }
            }
        }
    }
    //end region

    //Region IVehicleHealthReportFragment
    override fun updateVehicleModelName(modelName: String?) {
        binding?.apply {
            textVHRTitle.text = modelName
        }
    }

    override fun bindVehiclePhotos(imageList: List<VehicleAssetItem>) {
        activity?.let { safeCOntext ->
            binding?.apply {
                val adapter = VehicleHealthReportPhotoRecyclerAdapter(
                    safeCOntext,
                    imageList,
                    this@VehicleHealthReportFragment
                )
                recyclerVHRPhotos.layoutManager = LinearLayoutManager(
                    safeCOntext,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
                recyclerVHRPhotos.adapter = adapter
            }
        }
    }

    override fun bindDescription(descriptoins: List<VehicleHealthReportDescriptionModel>) {
        activity?.let { safeContext ->
            binding?.apply {
                val adapter = VehicleHealthReportDescriptionRecyclerAdapter(
                    safeContext,
                    descriptoins
                )
                recyclerVHRDescription.layoutManager = LinearLayoutManager(safeContext)
                recyclerVHRDescription.adapter = adapter
            }
        }
    }

    override fun bindSectionAndPercent(sections: List<VehicleHealthReportSectionModel>) {
        activity?.let { safeContext ->
            binding?.apply {
                val adapter = VehicleHealthReportSectionRecyclerAdapter(
                    safeContext,
                    sections,
                    this@VehicleHealthReportFragment
                )
                recyclerVHRSection.layoutManager = LinearLayoutManager(safeContext)

                val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
                val divider = ContextCompat.getDrawable(
                    safeContext,
                    R.color.textinput_borderline
                )
                divider?.let { line ->
                    itemDecorator.setDrawable(
                        line
                    )
                }

                recyclerVHRSection.addItemDecoration(itemDecorator)
                recyclerVHRSection.adapter = adapter
            }
        }
    }
    //endregion

    //Region IVehicleHealthReportSectionRecyclerAdapter
    override fun onItemClicked(sectionName: String) {
        presenter?.onSectionClickedForDetails(sectionName)
    }
    //end region

    //Region IVehicleImageClicked
    override fun onImageItemClicked(images: List<String>, selectedIndex: Int) {
        presenter?.onImageItemClicked(images, selectedIndex)
    }
    //endregion
}