package com.vectorform.msu.ui.vehiclehealthcheck.fullimage

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ItemPagerIndicatorGridBinding

class PagerIndicatorGridAdapter(
    private val layoutInflater: LayoutInflater,
    private val images: List<String>
) : BaseAdapter() {

    private var selection: Int = 0

    override fun getCount(): Int {
        return images.size
    }

    override fun getItem(position: Int): Any {
        return images[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val binding = ItemPagerIndicatorGridBinding.inflate(layoutInflater)
        when (selection) {
            position -> {
                binding?.viewItemFullImageIndicator.setBackgroundResource(R.color.button_enabled_color)
            }
            else -> {
                binding?.viewItemFullImageIndicator.setBackgroundResource(R.color.light_gray)
            }
        }
        return binding.root
    }

    /**
     * This function will highlight the selected tab indicator
     * @param selection : Int
     */
    fun setSelection(selection: Int) {
        this.selection = selection
        notifyDataSetChanged()
    }
}