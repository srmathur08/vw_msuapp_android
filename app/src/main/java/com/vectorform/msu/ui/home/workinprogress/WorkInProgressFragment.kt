package com.vectorform.msu.ui.home.workinprogress

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import com.fcaindia.drive.utils.CommonUtils
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.databinding.FragmentWorkInProgressBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.AppLocationProvider
import com.vectorform.msu.utils.BaseFragment
import com.vectorform.msu.utils.GpsUtils
import java.text.SimpleDateFormat
import java.util.*


class WorkInProgressFragment : BaseFragment(), WorkInProgressContract.IWorkInProgressFragment,
    OnMapReadyCallback, View.OnClickListener,
    MotionLayout.TransitionListener, GpsUtils.OnGpsListener,
    AppLocationProvider.UserLocationCallBack, FragmentServiceComplains.IFragmentServiceComplains {

    private var binding: FragmentWorkInProgressBinding? = null
    private var presenter: WorkInProgressContract.IWorkInProgressPresenter? = null
    private var googleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = WorkInProgressPresenter(safeContext, this, activity as HomeActivity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWorkInProgressBinding.inflate(inflater, container, false)
        presenter?.fetchServiceComplains()
        googleMap?.clear()
        activity?.let { safeCOntext ->
            val mapFragment: SupportMapFragment =
                childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
            mapFragment.getMapAsync(this)
        }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageWorkInProgressClose.setOnClickListener(this@WorkInProgressFragment)
            textWorkINProgressServiceHistoryLink.setOnClickListener(this@WorkInProgressFragment)
            layoutWIPDragButtonContainer?.setTransitionListener(this@WorkInProgressFragment)
            textWorkInProgressVehicleHealthCheck.setOnClickListener(this@WorkInProgressFragment)
            textWIPComplaints.setOnClickListener(this@WorkInProgressFragment)
            textGetDirections.setOnClickListener(this@WorkInProgressFragment)
            imageWipNavigation.setOnClickListener(this@WorkInProgressFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter?.onResumed()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    @SuppressLint("MissingPermission")
    //Region  OnMapReadyCallback
    override fun onMapReady(p0: GoogleMap) {
        p0?.let { mMap ->
            mMap.isMyLocationEnabled = true
            mMap.uiSettings.isMyLocationButtonEnabled = false
            googleMap = mMap
            activity?.let { safeContext ->
                GpsUtils(safeContext).turnGPSOn(this)
            }
        }
    }

    //Region GPSUtils.OnGpsListener
    override fun gpsStatus(isGPSEnable: Boolean) {
        if (isGPSEnable) {
            activity?.let { safeContext ->
                AppLocationProvider().getLocation(safeContext, this)
            }
        }
    }

    override fun showLocationResolution(exception: ResolvableApiException) {
        presenter?.handleLocationResolution(exception)
    }
    //endregion

    //Region AppLocationProvider.UserLocationCallBack
    override fun locationResult(location: Location?) {
        location?.let { loc ->
            presenter?.onMapReady(loc)
        }
    }
    //endregion

    //Region View.OnclickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageWorkInProgressClose -> {
                    presenter?.onCloseClicked()
                }
                textWorkINProgressServiceHistoryLink -> {
                    presenter?.onViewServiceHistoryClicked()
                }
                textWorkInProgressVehicleHealthCheck -> {
                    presenter?.onVehicleHealthCheckClicked()
                }
                textWIPComplaints -> {
                    presenter?.onServiceComplainClicked()
                }
                textGetDirections, imageWipNavigation -> {
                    presenter?.onNavigateToUserLocationClicked()
                }
            }
        }
    }
    //endregion

    override fun bindComplains(serviceComplains: List<ServiceComplainModel>) {
        binding?.apply {
            val fragment = FragmentServiceComplains().apply {
                setData(serviceComplains, this@WorkInProgressFragment)
            }
            childFragmentManager.beginTransaction().replace(R.id.complainFragContainer, fragment, "SRC").addToBackStack("SRC").commit()
        }
    }

    override fun vehicleHealthCheckCompleted() {
        binding?.apply {
            textWorkInProgressVehicleHealthCheck.visibility = View.GONE
            layoutWIPDragButtonContainer.visibility = View.VISIBLE
            layoutWIPDragButtonContainer.alpha = 0.6f
            layoutWIPDragButtonContainer.getTransition(R.id.transitionWIPButton).setEnable(false)
        }
    }

    override fun vehicleHealthCheckNotCompleted() {
        binding?.apply {
            textWorkInProgressVehicleHealthCheck.visibility = View.VISIBLE
            layoutWIPDragButtonContainer.visibility = View.GONE
        }
    }

    override fun complainCompleted() {
        binding?.apply {
            textWorkInProgressVehicleHealthCheck.visibility = View.GONE
            layoutWIPDragButtonContainer.visibility = View.VISIBLE
            layoutWIPDragButtonContainer.alpha = 1f
            layoutWIPDragButtonContainer.getTransition(R.id.transitionWIPButton).setEnable(true)
        }
    }

    override fun complainNotCompleted() {
        binding?.apply {
            layoutWIPDragButtonContainer.visibility = View.GONE
            textWorkInProgressServiceComplainTitle.visibility = View.INVISIBLE
        }
    }

    override fun bindUserInfo(workInProgress: SRsByDateResponse.ServiceRequest) {
        binding?.apply {
            textWorkInProgressName.text = workInProgress.CustomerName
            textWorkInProgressRegno.text = workInProgress.RegNum
            textWorkInProgressServiceType.text = workInProgress.ServiceType + "  " + workInProgress.Type
            textWorkInProgressDateTime.text =
                SimpleDateFormat("dd MMMM yyyy | hh:mm a").format(Date().time)
            inputOtherServices.setText(workInProgress.ServiceOther)
            inputWIPServiceEstimate.setValue(workInProgress.Revenue ?: "")
        }
    }

    override fun drawRouteToCustomerLocation(
        latitude: Double,
        longitude: Double,
        address: String,
        location: Location?
    ) {
        activity?.let { safeContext ->
            googleMap?.let { mMap ->

                val custoemrMarker = mMap.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            latitude,
                            longitude
                        )
                    ).title(address)
                )

                location?.let { userLocation ->
                    val userAddress = CommonUtils.getAddress(
                        safeContext,
                        userLocation.latitude,
                        userLocation.longitude
                    )
                    val userMarker = mMap.addMarker(
                        MarkerOptions().position(
                            LatLng(
                                userLocation.latitude,
                                userLocation.longitude
                            )
                        ).title(userAddress?.getAddressLine(0) ?: "Unknown")
                    )

                    val markers = ArrayList<Marker>()

                    markers.add(userMarker)
                    markers.add(custoemrMarker)

                    val builder = LatLngBounds.Builder()
                    for (marker in markers) {
                        builder.include(marker.position)
                    }
                    val bounds = builder.build()
                    val padding = 60 // offset from edges of the map in pixels
                    val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
                    mMap.moveCamera(cu)
                    mMap.animateCamera(cu)

                    mMap.addPolyline(
                        PolylineOptions()
                            .clickable(true)
                            .add(
                                LatLng(userLocation.latitude, userLocation.longitude),
                                LatLng(latitude, longitude)
                            )
                    )
                } ?: run {
                    mMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(latitude, longitude),
                            15f
                        )
                    )
                }
            }
        }
    }

    override fun launchGoogleMapDirections(lat: Double, lng: Double) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("http://maps.google.com/maps?daddr=$lat,$lng")
        )
        startActivity(intent)
    }

    //Region MotionLayout Transition Listener
    override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {

    }

    override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {

    }

    override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {

    }

    override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
        binding?.apply {
            when (p1) {
                R.id.EndDrag -> {
                    presenter?.serviceCompleted(inputOtherServices.text.toString(), inputWIPServiceEstimate.value)
                }
            }
        }
    }
    //endregion


    /**
     * This function will be invoked from activity to handle Location ResolutionResult
     * @param resultOk : Boolean
     */
    fun onLocationResolutionResult(resultOk: Boolean) {
        activity?.let { safeContext ->
            binding?.apply {
                when (resultOk) {
                    true -> {
                        GpsUtils(safeContext).turnGPSOn(this@WorkInProgressFragment)
                    }
                    false -> {
                        showCustomMessage(
                            R.string.alert,
                            "Please enable your location to see route to customer location"
                        ) { which, dialog -> presenter?.onMapReady(null) }
                    }
                }
            }
        }
    }

    override fun onServiceComplainsUpdated(list: ArrayList<ServiceComplainModel>) {
        presenter?.onComplainCheckChanged(list)
    }
}