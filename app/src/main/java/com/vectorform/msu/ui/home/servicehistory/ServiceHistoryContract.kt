package com.vectorform.msu.ui.home.servicehistory

import com.vectorform.msu.data.responses.sfdc.services.ServiceHistoryData
import com.vectorform.msu.utils.BaseFragmentContract

interface ServiceHistoryContract {

    interface IServiceHistoryFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will bind the service history list in UI
         * @param serviceHistory : List<ServiceHistoryData>
         */
        fun bindServiceHistoryData(serviceHistory: List<ServiceHistoryData>)

        /**
         * This function will show No Data text when there is no Service History data
         */
        fun showNoData()

        /**
         * This function will hide the No Data Text from UI
         */
        fun hideNoData()
    }

    interface IServiceHistoryPresenter {
        /**
         * This function will handle back click
         */
        fun onBackClicked()

        /**
         * This function will fetch the Service History data from UI
         */
        fun fetchServiceHistory()

        /**
         * This function will handle click on Service History Item
         * @param serviceHistoryModel: ServiceHistoryData
         */
        fun onServiceHistoryCardClicked(serviceHistoryModel: ServiceHistoryData)

        fun onSendServiceHistory()
    }

}