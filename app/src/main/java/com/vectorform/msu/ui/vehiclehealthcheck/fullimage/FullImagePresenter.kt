package com.vectorform.msu.ui.vehiclehealthcheck.fullimage

import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract

class FullImagePresenter(
    private val view: FullImageContract.IFullImageFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter,
    private val args: FullImageFragmentArgs
) : FullImageContract.IFullImagePresenter {

    //Region IFullImagePresenter
    override fun viewCreated() {
        args.imageItem?.let { item ->
            view.bindImages(item.images, item.selectedIndex, item.dynamicUrl)
        }
    }

    override fun onCloseClicked() {
        router.popBack()
    }

    //endregion
}