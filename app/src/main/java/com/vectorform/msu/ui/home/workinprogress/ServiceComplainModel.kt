package com.vectorform.msu.ui.home.workinprogress

import com.vectorform.msu.utils.MandatoryServiceTypeEnums
import com.vectorform.msu.utils.ServiceComplainsEnum

class ServiceComplainModel {
    var complainId: String? = null
    var complain: ServiceComplainsEnum? = null
    var isChecked: Boolean = false
    var isMandatory : Boolean = false
}