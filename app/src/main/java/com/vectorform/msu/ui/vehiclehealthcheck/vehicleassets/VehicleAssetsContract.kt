package com.vectorform.msu.ui.vehiclehealthcheck.vehicleassets

import com.vectorform.msu.utils.BaseFragmentContract

interface VehicleAssetsContract {

    interface IVehicleAssetsFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will bind the recycler view with existing images
         * @param images : ArrayList<String>
         */
        fun bindRecycler(images: ArrayList<String>)

        /**
         * This function will notify the recycler view adapter for newly added image
         */
        fun notifyAdapter()

        /**
         * This function will show the validation error message in UI
         * @param message : String
         */
        fun setValidationError(message: String)
    }

    interface IVehicleAssetsPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun onViewCreated()

        /**
         * This function will handle the back button click
         */
        fun onBackClicked()

        /**
         * This function will handle the NEXT button click
         */
        fun onNextClicked()

        /**
         * This function will handle the Upload Image click
         */
        fun uploadImageClicked()

        /**
         * This function will handle the camera image
         * @param imagePath : String (local storage path for captured image)
         */
        fun processCameraResult(imagePath: String)

        /**
         * This function will handle the image item click to show in full screen
         * @param images : List<String> (all images)
         * @param selectedIndex : Int (clicked image index for default selection)
         */
        fun onImageItemClicked(images: List<String>, selectedIndex: Int)
    }

}