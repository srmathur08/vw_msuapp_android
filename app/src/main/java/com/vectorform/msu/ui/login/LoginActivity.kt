package com.vectorform.msu.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ActivityLoginBinding
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.UtilsActivity

class LoginActivity : UtilsActivity(), LoginMainContract.ILoginMainActivity,
    LoginMainContract.LoginMainRouter {

    private var binding: ActivityLoginBinding? = null
    private var presenter: LoginMainPresenter? = null
    private var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        presenter = LoginMainPresenter(this)

        binding?.let { safeBinding ->
            navController = findNavController(R.id.nav_host_login_fragment)
            supportActionBar?.hide()
        }
    }

    override fun navigateToHome() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }


}