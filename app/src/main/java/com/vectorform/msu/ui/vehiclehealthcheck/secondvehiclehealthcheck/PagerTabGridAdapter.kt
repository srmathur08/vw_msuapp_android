package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.core.content.ContextCompat
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ItemPagerTabCellBinding
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem

class PagerTabGridAdapter(
    private val layoutInflater: LayoutInflater,
    private val itemList: List<VehicleHealthCheckUIModelItem>,
    private var pagerIndex: Int
) : BaseAdapter() {


    override fun getCount(): Int {
        return itemList.size
    }

    override fun getItem(position: Int): Any {
        return itemList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val binding = ItemPagerTabCellBinding.inflate(layoutInflater, parent, false)

        binding?.apply {
            textTabItemIndex.text = itemList[position].Index.toString()

            if (position == pagerIndex) {
                textTabItemIndex.setTextColor(Color.parseColor("#00B0F0"))
                viewTabItemLine.setBackgroundColor(Color.parseColor("#00B0F0"))
            } else if (position > pagerIndex) {
                textTabItemIndex.setTextColor(Color.parseColor("#DFE4E8"))
                viewTabItemLine.setBackgroundColor(Color.parseColor("#DFE4E8"))
            } else {
                textTabItemIndex.setTextColor(Color.parseColor("#001E50"))
                viewTabItemLine.setBackgroundColor(Color.parseColor("#001E50"))
            }
        }

        return binding?.root
    }

    /**
     * This function will highlight the selected image index
     * @param index : Int
     */
    fun setSelectedIndex(index: Int) {
        pagerIndex = index
        notifyDataSetChanged()
    }
}