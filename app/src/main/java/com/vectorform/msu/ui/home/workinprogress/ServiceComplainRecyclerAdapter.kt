package com.vectorform.msu.ui.home.workinprogress

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ServiceComplainRecyclerItemBinding

class ServiceComplainRecyclerAdapter(
    val context: Context,
    val serviceComplains: List<ServiceComplainModel>,
    val itemClickListener: IServiceComplainRecyclerAdapter
) : RecyclerView.Adapter<ServiceComplainRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ServiceComplainRecyclerItemBinding =
            ServiceComplainRecyclerItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return serviceComplains.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ServiceComplainRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding?.apply {

                if(serviceComplains[position].isMandatory) {
                    textSCRIComplain.text = "*" + serviceComplains[position].complain?.getComplainText()
                }else{
                    textSCRIComplain.text = serviceComplains[position].complain?.getComplainText()
                }
                when (serviceComplains[position].isChecked) {
                    true -> {
                        imageSCRIcheck.setImageResource(R.drawable.ic_checked_blue)
                    }
                    false -> {
                        imageSCRIcheck.setImageResource(R.drawable.unchecked_gray)
                    }
                }
                binding?.root.setOnClickListener {
                    serviceComplains[position].isChecked = !serviceComplains[position].isChecked
                    itemClickListener.OnServiceComplainItemCheckChanged(serviceComplains[position])
                    notifyDataSetChanged()
                }
            }
        }
    }
}