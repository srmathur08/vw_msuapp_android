package com.vectorform.msu.ui.vehiclehealthcheck.markerdialog

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.vectorform.msu.databinding.FragmentMarkerDialogBinding
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview.ImageMarkerModel
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem
import com.vectorform.msu.utils.MarkerEnum

class MarkerDialogFragment : DialogFragment(), MarkerDialogContract.IMarkerDialogFragment,
    View.OnClickListener {

    private var binding: FragmentMarkerDialogBinding? = null
    private var presenter: MarkerDialogPresenter? = null
    private lateinit var pagerItem: VehicleHealthCheckUIModelItem
    private var clickedImageIndex: Int = 0
    private var listener: IMarkerDialog? = null


    fun setPage(
        pagerItem: VehicleHealthCheckUIModelItem,
        clickedImageIndex: Int,
        listener: IMarkerDialog?
    ) {
        this.pagerItem = pagerItem
        this.clickedImageIndex = clickedImageIndex
        this.listener = listener
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter = MarkerDialogPresenter(
                this,
                activity as VehicleHealthCheckActivity,
                pagerItem,
                clickedImageIndex
            )
            setStyle(STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.let {
            it.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }
    }

    override fun onPause() {
        super.onPause()
        activity?.let {
            it.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMarkerDialogBinding.inflate(inflater, container, false)
        presenter?.viewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageMDClose.setOnClickListener(this@MarkerDialogFragment)
            imageMDLeftArrow.setOnClickListener(this@MarkerDialogFragment)
            imageMDRightArrow.setOnClickListener(this@MarkerDialogFragment)
            textMDMarkRepair.setOnClickListener(this@MarkerDialogFragment)
            textMDMarkDamage.setOnClickListener(this@MarkerDialogFragment)
            textTSVUndo.setOnClickListener(this@MarkerDialogFragment)
            imageTSVUndo.setOnClickListener(this@MarkerDialogFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageMDClose -> {
                    presenter?.onCloseClicked()
                }
                imageMDLeftArrow -> {
                    carViewMDcar.previousImage()
                }
                imageMDRightArrow -> {
                    carViewMDcar.nextImage()
                }
                imageTSVUndo, textTSVUndo -> {
                    carViewMDcar.undo()
                }
                textMDMarkRepair -> {
                    carViewMDcar.enableDrawing(true)
                    carViewMDcar.setPainColor(MarkerEnum.Repair.getMarkerColor())
                }
                textMDMarkDamage -> {
                    carViewMDcar.enableDrawing(true)
                    carViewMDcar.setPainColor(MarkerEnum.Damage.getMarkerColor())
                }
            }
        }
    }
    //endregion

    //Region IMarkerDialogFragment
    override fun closeMarkerDialog() {
        binding?.apply {
            listener?.onMarkerDrawComplete(carViewMDcar.getCurrentImageIndex())
            dismiss()
        }
    }

    override fun bindImages(
        images: List<String>,
        markers: ArrayList<ImageMarkerModel>,
        selectedIndex: Int
    ) {
        binding?.apply {
            carViewMDcar.post {
                carViewMDcar.setContainerHeightWidth(carViewMDcar.width, carViewMDcar.height)
                carViewMDcar.setImageList(images)
                carViewMDcar.setMarkers(markers)
                carViewMDcar.setImageIndex(selectedIndex)
            }
        }
    }
    //endregion


    interface IMarkerDialog {
        /**
         * This function will notify the parent for marker drawing completed event
         * @param index : Int index of current image selected in car view
         */
        fun onMarkerDrawComplete(index: Int)
    }

}