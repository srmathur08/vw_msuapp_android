package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.common

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vectorform.msu.R
import com.vectorform.msu.databinding.ItemVehileHealthCheckRadiosBinding

class MultiSelectRadioFieldRecyclerAdapter(
    val context: Context, var itemList: List<String>, var value: String?,
    val parentPosition: Int, val listener: IMultiSelectorRadioFieldRecyclerAdapter
) : RecyclerView.Adapter<MultiSelectRadioFieldRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ItemVehileHealthCheckRadiosBinding =
            ItemVehileHealthCheckRadiosBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ItemVehileHealthCheckRadiosBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.apply {
                textRadioOption.text = itemList[position]
                value?.let { selectedValue->
                 if(selectedValue.contains("|")){
                     val split = selectedValue.split("|")
                     if(split.contains(itemList[position])){
                         imageRadioOption.setImageResource(R.drawable.ic_checked_blue)
                     }else{
                         imageRadioOption.setImageResource(R.drawable.ic_unchecked_blue)
                     }
                 }else{
                     when (value) {
                         itemList[position] -> {
                             imageRadioOption.setImageResource(R.drawable.ic_checked_blue)
                         }
                         else -> {
                             imageRadioOption.setImageResource(R.drawable.ic_unchecked_blue)
                         }
                     }
                 }
                }?:run{
                    imageRadioOption.setImageResource(R.drawable.ic_unchecked_blue)
                }



                imageRadioOption.setOnClickListener {
                    if(TextUtils.isEmpty(value)){
                        value = textRadioOption.text.toString()
                    }else{
                        value = value + "|" +textRadioOption.text.toString()
                    }

                    listener.onValueSelected(textRadioOption.text.toString(), parentPosition)
                    notifyDataSetChanged()
                }
                textRadioOption.setOnClickListener {
                    if(TextUtils.isEmpty(value)){
                        value = textRadioOption.text.toString()
                    }else{
                        value = value + "|" +textRadioOption.text.toString()
                    }
                    listener.onValueSelected(textRadioOption.text.toString(), parentPosition)
                    notifyDataSetChanged()
                }
            }
        }
    }

    interface IMultiSelectorRadioFieldRecyclerAdapter {
        /**
         * This function will pass the selected value to parent
         * @param value : String (The selected option)
         * @param index : Int (Subsection index)
         */
        fun onValueSelected(value: String, index: Int)
    }
}