package com.vectorform.msu.ui.vehiclehealthcheck.vehiclehealthreport

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.vf.VehicleAssetItem
import com.vectorform.msu.databinding.VehicleReportPhotosRecyclerItemBinding
import com.vectorform.msu.ui.vehiclehealthcheck.IVehicleImageClicked
import com.vectorform.msu.utils.Constants

class VehicleHealthReportPhotoRecyclerAdapter(
    val context: Context,
    var itemList: List<VehicleAssetItem>,
    val listener: IVehicleImageClicked
) : RecyclerView.Adapter<VehicleHealthReportPhotoRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: VehicleReportPhotosRecyclerItemBinding =
            VehicleReportPhotosRecyclerItemBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: VehicleReportPhotosRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.apply {
                Glide.with(context).load(Constants.BASEURL + itemList[position].Asset)
                    .diskCacheStrategy(
                        DiskCacheStrategy.NONE
                    ).into(imageVHRPhotoItem)

                root.setOnClickListener {
                    listener.onImageItemClicked(
                        itemList.map { i -> i.Asset },
                        position
                    )
                }
            }
        }
    }
}