package com.vectorform.msu.ui.vehiclehealthcheck

import com.fcaindia.drive.network.VfCalls
import com.vectorform.msu.data.input.SRsBySRIdInput
import com.vectorform.msu.data.responses.vf.VehicleCheckUpDataBySrIdResponse

class VehicleHealthCheckPresenter(
    private val activity: VehicleHealthCheckActivity,
    private val view: VehicleHealthCheckContract.IVehicleHealthCheckActivity,
    val srid: Int
) : VehicleHealthCheckContract.IVehicleHealthCheckPresenter {


    private var vehicleCheckUpId: Int = 0
    private var fuelLevel: String = ""

    override fun viewCreated() {
        VfCalls(activity).getVehicleCheckUpDataBySRId(
            SRsBySRIdInput(srid),
            object : VfCalls.OnResponse<VehicleCheckUpDataBySrIdResponse> {
                override fun onSucess(response: VehicleCheckUpDataBySrIdResponse) {
//                view.startVehicleHealthCheck()
                }

                override fun onFailure(message: String) {
//                view.startVehicleHealthCheck()
                }
            })
    }

    override fun setVehicleChecpId(vehicleCheckUpId: Int) {
        this.vehicleCheckUpId = vehicleCheckUpId
    }

    override fun setFuelLevel(fuelLevelPercent: String) {
        this.fuelLevel = fuelLevelPercent
    }

    override fun getVehicleChecckUpId(): Int {
        return vehicleCheckUpId
    }

    override fun getFuelLevel(): String {
        return fuelLevel
    }
}