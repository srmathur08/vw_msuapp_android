package com.vectorform.msu.ui.home.servicedetails

import com.vectorform.msu.utils.BaseFragmentContract

interface ServiceDetailsContract {

    interface IServiceDetailsFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will update the service completion date in UI
         * @param date : String
         */
        fun updateServiceCompletedDate(date: String)

        /**
         * This function will update the Service Start Date in UI
         * @param date : String
         */
        fun updateServiceDate(date: String)

        /**
         * This function will show the repair order no
         * @param repairOrderNo : String
         */
        fun updateRepairOrderNo(repairOrderNo: String)

        /**
         * This function will show the Ro Type
         * @param roType : String
         */
        fun updateRoType(roType: String)

        /**
         * This function will update Ro Service Type in UI
         * @param roServiceType : String
         */
        fun updateRoServiceType(roServiceType: String)

        /**
         * This function will update Service Engineer name in UI
         * @param name : String
         */
        fun updateServiceEngineer(name: String)

        /**
         * This function will update service instructions in UI
         * @param instructions : String
         */
        fun updateServiceInstruction(instructions: String)

        /**
         * This function will show the Service Adviser instructions in UI
         * @param instructions : String
         */
        fun updateSAInstruction(instructions: String)

        /**
         * This function will show the invoice amount in UI
         * @param amount : String
         */
        fun updateInvoice(amount: String)

        /**
         * This function will update Customer rating in UI
         * @param rating : String
         */
        fun updateRatings(rating: String)

        /**
         * This function will hide the ratings fields from UI
         */
        fun hideRatingFields()

    }

    interface IServiceDetailsPresenter {
        /**
         * This function will be called from Fragments OnView Created life cycle call back.
         */
        fun onViewCreated()

        /**
         * This function will handle the back button click
         */
        fun onBackClicked()
    }

}