package com.vectorform.msu.ui.home.servicefeedback

import android.content.Context
import com.vectorform.msu.ui.home.HomeContract

class ServiceFeedbackPresenter(
    private val context: Context,
    private val view: ServiceFeedbackContract.IServiceFeedbackFragment,
    private val router: HomeContract.HomeRouter
) : ServiceFeedbackContract.IServiceFeedbackPresenter {

    //Region IServiceFeedbackPresenter
    override fun onBackClicked() {
        router?.popBack()
    }

    override fun onSubmitClicked() {
//        TODO("Not yet implemented")
    }
    //endregion


}