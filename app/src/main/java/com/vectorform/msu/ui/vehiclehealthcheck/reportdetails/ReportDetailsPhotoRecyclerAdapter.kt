package com.vectorform.msu.ui.vehiclehealthcheck.reportdetails

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.vectorform.msu.databinding.ItemDetailsImagesCellBinding
import com.vectorform.msu.ui.vehiclehealthcheck.IVehicleImageClicked

class ReportDetailsPhotoRecyclerAdapter(
    val context: Context,
    var itemList: List<String>,
    val listener: IVehicleImageClicked
) :
    RecyclerView.Adapter<ReportDetailsPhotoRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: ItemDetailsImagesCellBinding =
            ItemDetailsImagesCellBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: ItemDetailsImagesCellBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding?.apply {
                Glide.with(context).load(itemList[position])
                    .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageDetailsAsset)

                root.setOnClickListener { listener.onImageItemClicked(itemList, position) }
            }
        }
    }

}