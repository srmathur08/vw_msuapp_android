package com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview

import android.content.Context
import com.fcaindia.drive.utils.CommonUtils
import com.vectorform.msu.BuildConfig
import com.vectorform.msu.data.input.MarkersValueInput
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem

class ThreeSixtyViewPagerItemPresenter(
    private val context: Context, private val dataItem: VehicleHealthCheckUIModelItem,
    private val view: ThreeSixtyViewPagerItemContract.IThreeSixtyViewPagerItemFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter
) : ThreeSixtyViewPagerItemContract.IThreeSixtyViewPagerItemPresenter {

    val imageMarkers = ArrayList<ImageMarkerModel>()

    //Region IThreeSixtyViewPagerPresenter
    override fun viewCreated(index: Int) {
        imageMarkers.clear()
        view?.updateTitle(dataItem.SectionName)

        var imagePathFront = "android.resource://${BuildConfig.APPLICATION_ID}/drawable/car_angle_front"
        if(imagePathFront.isNullOrEmpty()){
            imagePathFront = "android.resource://${BuildConfig.APPLICATION_ID}/drawable-v24/car_angle_front"
        }

        var imagePathLeft = "android.resource://${BuildConfig.APPLICATION_ID}/drawable/car_angle_left"
        if(imagePathLeft.isNullOrEmpty()){
            imagePathLeft = "android.resource://${BuildConfig.APPLICATION_ID}/drawable-v24/car_angle_left"
        }
        var imagePathRight = "android.resource://${BuildConfig.APPLICATION_ID}/drawable/car_angle_right"
        if(imagePathRight.isNullOrEmpty()){
            imagePathRight = "android.resource://${BuildConfig.APPLICATION_ID}/drawable-v24/car_angle_right"
        }

        var imagePathBack = "android.resource://${BuildConfig.APPLICATION_ID}/drawable/car_angle_back"
        if(imagePathBack.isNullOrEmpty()){
            imagePathBack = "android.resource://${BuildConfig.APPLICATION_ID}/drawable-v24/car_angle_back"
        }

        imageMarkers.add(
            ImageMarkerModel(
                "Front",
                imagePathFront,
                ArrayList()
            )
        )
        imageMarkers.add(
            ImageMarkerModel(
                "Left",
                imagePathLeft,
                ArrayList()
            )
        )
        imageMarkers.add(
            ImageMarkerModel(
                "Right",
                imagePathRight,
                ArrayList()
            )
        )
        imageMarkers.add(
            ImageMarkerModel(
                "Back",
                imagePathBack,
                ArrayList()
            )
        )
        view?.showThreeSixtyImages(imageMarkers.map { it.imagePath }, index)

        dataItem.CleaningRemark?.let { remark ->
            view.showCleaningRemark(remark)
        }

        dataItem.DamageRemark?.let { remark ->
            view.showDamageRemark(remark)
        }

        dataItem.OffersAndNotes?.let { notes ->
            view.showNotes(notes)
        }

        dataItem.ScratchAndDamage?.let { markers ->
            for (m in markers) {
                for (im in imageMarkers) {
                    if (m.imagePath.equals(im.imagePath)) {
                        im.markers.addAll(m.markers)
                    }
                }
            }
        } ?: run {
            dataItem.ScratchAndDamage = ArrayList()
            dataItem.ScratchAndDamage.addAll(imageMarkers)
        }

    }

    override fun updateMarkers(image: String, marker: MarkersValueInput) {

        for (im in imageMarkers) {
            if (im.imagePath.equals(image)) {
                im.markers.add(marker)
            }
        }

        dataItem.ScratchAndDamage?.apply {
            clear()
            addAll(imageMarkers)
        } ?: run {
            dataItem.ScratchAndDamage = ArrayList()
            dataItem.ScratchAndDamage.addAll(imageMarkers)
        }
    }

    override fun updateCleaningRemark(remark: String) {
        dataItem.CleaningRemark = remark
    }

    override fun updateDamageRemark(remark: String) {
        dataItem.DamageRemark = remark
    }

    override fun updateNotes(notes: String) {
        dataItem.OffersAndNotes = notes
    }

    override fun onCarViewClicked(index: Int) {
        view.openMarkerDialog(dataItem, index)
    }

    override fun onImageAdded() {
        view?.updateScratchAndDamages(imageMarkers)
    }
    //endregion


}