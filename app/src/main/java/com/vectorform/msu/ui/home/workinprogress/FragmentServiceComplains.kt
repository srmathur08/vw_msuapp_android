package com.vectorform.msu.ui.home.workinprogress

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.vectorform.msu.R
import com.vectorform.msu.databinding.FragmentServiceComplaintsBinding
import com.vectorform.msu.utils.BaseFragment
import com.vectorform.msu.utils.ServiceComplainsEnum

class FragmentServiceComplains : BaseFragment(), IServiceComplainRecyclerAdapter {

    private var binding : FragmentServiceComplaintsBinding? = null

    private var serviceComplains = listOf<ServiceComplainModel>()
    private var callBack : IFragmentServiceComplains? = null

    private val serviceModel = ArrayList<ServiceComplainModel>()

    fun setData(serviceComplains : List<ServiceComplainModel>, callBack : IFragmentServiceComplains){
        this.serviceComplains = serviceComplains
        this.callBack = callBack
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentServiceComplaintsBinding.inflate(inflater, container, false)

        activity?.let { safeContext ->
            binding?.apply {
                var id = 0
                ServiceComplainsEnum.values().forEach { sC->
                    val cId = ++id
                    val sComplain = ServiceComplainModel().apply {
                        id = cId
                        complain = sC
                        isChecked = false
                        isMandatory = false
                    }
                    serviceModel.add(sComplain)
                }

                serviceComplains?.forEach { exComplain->
                    serviceModel?.forEach { allComplain->
                        if(exComplain.complain == allComplain.complain){
                            allComplain.isChecked = exComplain.isChecked
                            allComplain.isMandatory = exComplain.isMandatory
                        }
                    }
                }
                serviceModel.sortByDescending { x-> x.isMandatory }

                val adapter = ServiceComplainRecyclerAdapter(
                    safeContext,
                    serviceModel,
                    this@FragmentServiceComplains
                )
                recyclerComplaints.layoutManager = LinearLayoutManager(safeContext)
                recyclerComplaints.adapter = adapter
            }
        }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            buttonComplainsSubmit.setOnClickListener {
            var markedMandatoryFields = true
                for(s in serviceModel) {
                    if(s.isMandatory && !s.isChecked){
                        markedMandatoryFields = false
                    }
                }
                if(!markedMandatoryFields){
                    textComplainError.visibility = View.VISIBLE
                }else{
                    textComplainError.visibility = View.INVISIBLE
                    callBack?.onServiceComplainsUpdated(serviceModel)
                    parentFragmentManager.popBackStack()
                }
            }

            imageComplainDialogClose.setOnClickListener {
                parentFragmentManager.popBackStack()
            }

            layoutComplainsContainer.setOnTouchListener { v, event ->
                true
            }
        }
    }

    interface IFragmentServiceComplains{
        fun onServiceComplainsUpdated(list : ArrayList<ServiceComplainModel>)
    }

    override fun OnServiceComplainItemCheckChanged(complain: ServiceComplainModel) {
        serviceModel?.find { s-> s.complain == complain.complain }?.isChecked = complain.isChecked
    }

}