package com.vectorform.msu.ui.home.servicefeedback

import com.vectorform.msu.utils.BaseFragmentContract

interface ServiceFeedbackContract {

    interface IServiceFeedbackFragment : BaseFragmentContract.IBaseFragment {
    }

    interface IServiceFeedbackPresenter {
        /**
         * This function will handle the back icon click
         */
        fun onBackClicked()

        /**
         * This function will handle the Submit Feedback button click
         */
        fun onSubmitClicked()
    }

}