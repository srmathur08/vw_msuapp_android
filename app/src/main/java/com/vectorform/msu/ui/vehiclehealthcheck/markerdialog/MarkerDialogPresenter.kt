package com.vectorform.msu.ui.vehiclehealthcheck.markerdialog

import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckContract
import com.vectorform.msu.ui.vehiclehealthcheck.uidata.VehicleHealthCheckUIModelItem

class MarkerDialogPresenter(
    private val view: MarkerDialogContract.IMarkerDialogFragment,
    private val router: VehicleHealthCheckContract.VehicleHealthCheckRouter,
    private val pagerItem: VehicleHealthCheckUIModelItem,
    private val clickedImageIndex: Int,
) : MarkerDialogContract.IMarkerDialogPresenter {

    //Region IMarkerDialogPresenter
    override fun viewCreated() {
        pagerItem.ScratchAndDamage?.let { scratches ->
            val images = scratches.map { s -> s.imagePath }
            view.bindImages(images, scratches, clickedImageIndex)
        }
    }

    override fun onCloseClicked() {
        view.closeMarkerDialog()
    }

    //endregion
}