package com.vectorform.msu.ui.home.notifications

import com.vectorform.msu.data.responses.vf.Notifications

interface INotificationRecylcerAdapter {

    /**
     * This function will pass the service item to the view to handle clicked item in service recycler
     * @param service : SRsByDateResponse.ServiceRequest
     */
    fun onServiceItemClicked(notification: Notifications)
}