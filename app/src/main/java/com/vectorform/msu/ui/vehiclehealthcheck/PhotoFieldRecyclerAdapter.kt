package com.vectorform.msu.ui.vehiclehealthcheck

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vectorform.msu.databinding.SubSectionPhotoItemBinding

class PhotoFieldRecyclerAdapter(
    val context: Context,
    var itemList: List<String>,
    var listener: IVehicleImageClicked
) : RecyclerView.Adapter<PhotoFieldRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: SubSectionPhotoItemBinding =
            SubSectionPhotoItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(var binding: SubSectionPhotoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            binding.apply {
                Glide.with(context).load(itemList[position]).skipMemoryCache(true)
                    .into(imageSubSectionPhoto)

                imageSubSecPhotoDelete.setOnClickListener {
                    (itemList as MutableList).removeAt(position)
                    notifyDataSetChanged()
                }

                root.setOnClickListener { listener.onImageItemClicked(itemList, position) }
            }
        }
    }
}