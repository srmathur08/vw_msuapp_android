package com.vectorform.msu.ui.home.createservice

import androidx.annotation.DrawableRes

data class ServiceDetailsModel(
    val name: String,
    @DrawableRes val icon: Int,
    var isSelected: Boolean
)
