package com.vectorform.msu.ui.home

import com.google.android.gms.common.api.ResolvableApiException
import com.vectorform.msu.data.responses.sfdc.services.ServiceHistoryData
import com.vectorform.msu.data.responses.vf.SRsByDateResponse

interface HomeContract{

    interface IHomeActivity {
    }

    /**
     * Contract to update presenter from view
     */
    interface IHomePresenter {
        /**
         * This function will update the onGoing service object in Home Presenter
         * @param wip : SRsByDateResponse.ServiceRequest
         */
        fun setWorkInProgress(wip: SRsByDateResponse.ServiceRequest)

        /**
         * This function will return optional onGoing Service
         * @return SRsByDateResponse.ServiceRequest (Optional)
         */
        fun getWorkInProgress(): SRsByDateResponse.ServiceRequest?

        /**
         * This function will mark the IsVehicleHealthCheckCompletedFlag to true
         */
        fun markVehicleHealthCheckCompleted()
    }

    /**
     * This tells the router to handle view events
     */
    interface HomeRouter {
        /**
         * This function will navigate users to previous screen
         */
        fun popBack()

        /**
         * This function will navigate users from Dashboard to onGoing Service screen.
         * Also it will update the onGoingService object in Router
         * @param wip : SRsByDateResponse.ServiceRequest
         */
        fun navigateDashboardToWorkInProgressDirection(wip: SRsByDateResponse.ServiceRequest)

        /**
         * This function will navigate users from OnGoing Service screen to Service History Screen
         */
        fun navigateWorkInProgressToServiceHistory()

        /**
         * This function will navigate user from Service History page to Service Details page with the selected service record
         * @param serviceHistory : ServiceHistoryData
         */
        fun navigateServiceHistoryToServiceDetails(
            serviceHistory: ServiceHistoryData,
            overAllRating: String?,
            lessRatingPoint: String?
        )

        /**
         * This function will navigate user from OnGoing Service to start Vehicle Health Check Section
         */
        fun navigateWorkInProgressToFirstVehicleHealthCheck()

        /**
         * This function will navigate user from Dashboard to First Vehicle Health Check Screen
         * @param service : SRsByDateResponse.ServiceRequest
         */
        fun navigateDashBoardToFirstVehicleHealthCheck(service: SRsByDateResponse.ServiceRequest)

        /**
         * This function will navigate user from Dashboard to Create New Service Request screen
         */
        fun navigateDashBoardToCreateServiceRequest()

        /**
         * This function will navigate user from Dashboard to UserProfile screen
         */
        fun navigateDashboardToUserProfile()

        /**
         * This function will navigate user to Global Login screen
         */
        fun navigateToLogin()

        /**
         * This function will return the instance of OnGoing Service(optional) to the Child Presenters implementing home routers
         * @return SRsByDateResponse.ServiceRequest (Optional)
         */
        fun getWorkInProgress(): SRsByDateResponse.ServiceRequest?

        /**
         * This function will open resolution alert for Location access
         * @param exception : ResolvableApiException
         */
        fun showLocationResolution(exception: ResolvableApiException)

        /**
         * This function will open camera to capture image
         */
        fun openCameraToTakePhoto()

        /**
         * This function will open Gallery to choose image
         */
        fun openGalleryToChoosePhoto()

        fun navigateUserProfileToNotifications()
        fun navigateUserNotificationsToNotificationDetails()
    }

}