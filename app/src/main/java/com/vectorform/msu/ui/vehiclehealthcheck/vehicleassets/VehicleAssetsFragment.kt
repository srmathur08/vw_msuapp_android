package com.vectorform.msu.ui.vehiclehealthcheck.vehicleassets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.vectorform.msu.databinding.FragmentVehicleAssetsBinding
import com.vectorform.msu.ui.vehiclehealthcheck.IVehicleImageClicked
import com.vectorform.msu.ui.vehiclehealthcheck.PhotoFieldRecyclerAdapter
import com.vectorform.msu.ui.vehiclehealthcheck.VehicleHealthCheckActivity
import com.vectorform.msu.utils.BaseFragment

class VehicleAssetsFragment : BaseFragment(), VehicleAssetsContract.IVehicleAssetsFragment,
    View.OnClickListener,
    IVehicleImageClicked {

    private var binding: FragmentVehicleAssetsBinding? = null
    private var presenter: VehicleAssetsContract.IVehicleAssetsPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let { safeContext ->
            presenter =
                VehicleAssetsPresenter(safeContext, this, activity as VehicleHealthCheckActivity)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVehicleAssetsBinding.inflate(inflater, container, false)
        presenter?.onViewCreated()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            imageVABack.setOnClickListener(this@VehicleAssetsFragment)
            textVABack.setOnClickListener(this@VehicleAssetsFragment)
            layoutVAUpload.setOnClickListener(this@VehicleAssetsFragment)
            imageVADirections.setOnClickListener(this@VehicleAssetsFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    //Region View OnClickListener
    override fun onClick(v: View?) {
        binding?.apply {
            when (v) {
                imageVABack, textVABack -> {
                    presenter?.onBackClicked()
                }
                imageVADirections -> {
                    presenter?.onNextClicked()
                }
                layoutVAUpload -> {
                    presenter?.uploadImageClicked()
                }
            }
        }
    }
    //endregion


    //Region Public functions
    /**
     * This function will be invoked from router activity to handle captured image from camera
     * @param imagePath : String (local storage path for image)
     */
    fun cameraResult(imagePath: String) {
        presenter?.processCameraResult(imagePath)
    }
    //end region

    //Region IVehicleAssetsFragment
    override fun bindRecycler(images: ArrayList<String>) {
        activity?.let { safeContext ->
            binding?.apply {
                val adapter =
                    PhotoFieldRecyclerAdapter(safeContext, images, this@VehicleAssetsFragment)
                recyclerVAAssets.layoutManager =
                    LinearLayoutManager(safeContext, LinearLayoutManager.HORIZONTAL, false)
                recyclerVAAssets.adapter = adapter
            }
        }
    }

    override fun notifyAdapter() {
        binding?.apply {
            recyclerVAAssets.adapter?.notifyDataSetChanged()
        }
    }

    override fun setValidationError(message: String) {
        binding?.apply {
            textVAValidation.text = message
        }
    }
    //endregion

    //Regoin IVehicleImageClicked
    override fun onImageItemClicked(images: List<String>, selectedIndex: Int) {
        presenter?.onImageItemClicked(images, selectedIndex)
    }
    //endregion
}