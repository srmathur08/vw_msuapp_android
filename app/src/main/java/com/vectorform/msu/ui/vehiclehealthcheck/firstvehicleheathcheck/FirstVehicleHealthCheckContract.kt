package com.vectorform.msu.ui.vehiclehealthcheck.firstvehicleheathcheck

import com.vectorform.msu.data.responses.vf.SRsByDateResponse
import com.vectorform.msu.utils.BaseFragmentContract

interface FirstVehicleHealthCheckContract {

    interface IFirstVehicleHealthCheckFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will show the Service Date in UI
         * @param date : String
         */
        fun preFillServiceDate(date: String)

        /**
         * This function will update the customer related data in UI
         * @param srData: SRsByDateResponse.ServiceRequest
         */
        fun preFillCustomerData(srData: SRsByDateResponse.ServiceRequest)

        /**
         * This function will show mileage field validation message
         */
        fun showMileageError(error: String)

        /**
         * This function will show dialog to skip vehicle health check
         */
        fun showSkipVehicleHealthCheck()
    }

    interface IFirstVehicleHealthCheckPresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will handle the close icon click
         */
        fun onCloseClicked()

        /**
         * This function will start Vehicle Health Check
         * @param fuelLevelPercent : Float
         * @param vehicleMileage : String
         */
        fun onCreateVehicleHealthCheck(fuelLevelPercent: Float, vehicleMileage: String)

        /**
         * This function will handle Next button click
         * @param fuelLevelPercent : Float
         * @param vehicleMileage : String
         */
        fun onNextClicked(fuelLevelPercent: Float, vehicleMileage: String)

        /**
         * This function will redirect to OnGoing Service page
         */
        fun skipVehicleHealthCheck()
    }

}