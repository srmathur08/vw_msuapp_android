package com.vectorform.msu.ui.vehiclehealthcheck.fullimage

import com.vectorform.msu.utils.BaseFragmentContract

interface FullImageContract {

    interface IFullImageFragment : BaseFragmentContract.IBaseFragment {
        /**
         * This function will bind the images in view pager and show the selected index
         * @param images : List<String>
         * @param selectedIndex : Int
         * @param isDynamicUrl : Boolean to differentiate between local image url and server image url
         */
        fun bindImages(images: List<String>, selectedIndex: Int, isDynamicUrl: Boolean)
    }

    interface IFullImagePresenter {
        /**
         * This function will be invoked when OnViewCreated in called in Fragment Lifecycle
         */
        fun viewCreated()

        /**
         * This function will handle the Close icon click
         */
        fun onCloseClicked()
    }

}