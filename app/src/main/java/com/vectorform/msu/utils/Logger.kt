package com.vectorform.msu.utils

import android.util.Log

class Logger {

    companion object {
        /**
         * This function will write the custom error logs
         * @param key : String
         * @param data : String optional
         */
        fun log(key: String, data: String?) {
            data?.let {
                Log.e(key, it)
            }
        }
    }
}