package com.vectorform.msu.utils

enum class ServiceComplainsEnum(private val complian : String) {

    IsOilChange("Oil and Filter Replacement"),
    IsAirFilter("Air Filter Replacement"),
    IsBrakes("Brake Pad Replacement"),
    IsBrakesDisk("Brake Disk Replacement"),
    IsSuspensionStrut("Suspension Strut Replacement"),
    IsShockAbsorber("Shock Absorber Replacement"),
    IsControlArm("Control Arm Replacement"),
    IsBatteryCheck("Battery Replacement"),
    IsBulbReplace("Bulb / Fuse Replacement"),
    IsHornReplace("Horn Replacement"),
    IsTyreCheck("Tire Repair"),
    IsTyreReplace("Tire Replacement");

    fun getComplainText() : String{
        return complian
    }
}