package com.vectorform.msu.utils

import android.content.Context
import java.io.IOException
import java.io.InputStream

class UiJsonParser {
    fun loadJSONFromAsset(context : Context): String? {
        var json: String? = null
        json = try {
            val `is`: InputStream = context.assets.open("UIDATA_UPDATED.json")
            val size: Int = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, Charsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

}