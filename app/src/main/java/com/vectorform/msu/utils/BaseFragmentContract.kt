package com.vectorform.msu.utils

import android.content.DialogInterface
import androidx.annotation.StringRes

interface BaseFragmentContract {

    interface IBaseFragment {
        /**
         * This function will show the loading
         */
        fun showLoading()

        /**
         * This function will hide the loading
         */
        fun hideLoading()

        /**
         * This function will show Alert messages
         * @param title : Int String Resource
         * @param message : String
         * @param listener : DialogINterface.OnClickListener optional
         */
        fun showCustomMessage(
            @StringRes title: Int,
            message: String,
            listener: DialogInterface.OnClickListener?
        )
    }
}