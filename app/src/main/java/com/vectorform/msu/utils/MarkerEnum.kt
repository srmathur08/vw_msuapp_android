package com.vectorform.msu.utils

import android.graphics.Color

enum class MarkerEnum(private val color: Int) {

    Repair(Color.parseColor(Constants.REPAIR_MARKER_COLOR)),
    Damage(Color.parseColor(Constants.DAMAGE_MARKER_COLOR));


    /**
     * This function will return the color for enum names
     * @return Int Color
     */
    fun getMarkerColor(): Int {
        return color
    }

    companion object {
        /**
         * This function will return the enum name for color value
         * @param markerColor : Int
         * @return String
         */
        fun getMarkerName(markerColor: Int): String {
            val filter = MarkerEnum.values().filter { m -> m.color == markerColor }
            if (filter.isNotEmpty()) {
                return filter[0].name
            }
            return ""
        }
    }
}