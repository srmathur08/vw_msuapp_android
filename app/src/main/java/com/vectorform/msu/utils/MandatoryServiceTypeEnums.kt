package com.vectorform.msu.utils

enum class MandatoryServiceTypeEnums(val serviceType : String) {

    MAINTENANCE("Maintenance"),
    INSPECTION_SERVICE("Inspection Service");

    public fun getMandatoryServiceType() : String{
        return serviceType
    }
}