package com.vectorform.msu.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.IntentSender.SendIntentException
import android.location.LocationManager
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener

class GpsUtils(private val context: Context) {
    private var onGpsListener: OnGpsListener? = null
    private val mSettingsClient: SettingsClient
    private val mLocationSettingsRequest: LocationSettingsRequest
    private val locationManager: LocationManager
    private val locationRequest: LocationRequest


    /**
     * This function will either turn on the GPS or ask user to grant permission to enable GPS
     * @param onGpsListener: OnGpsListener (optional)
     */
    fun turnGPSOn(onGpsListener: OnGpsListener?) {
        this.onGpsListener = onGpsListener
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (this.onGpsListener != null) {
                this.onGpsListener!!.gpsStatus(true)
            }
        } else {
            mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(
                    object : OnSuccessListener<LocationSettingsResponse?> {
                        @SuppressLint("MissingPermission")
                        override fun onSuccess(locationSettingsResponse: LocationSettingsResponse?) { //  GPS is already enable, callback GPS status through listener
                            onGpsListener?.gpsStatus(true)
                        }
                    })
                .addOnFailureListener(context as AppCompatActivity, object : OnFailureListener {
                    override fun onFailure(e: Exception) {
                        val statusCode: Int = (e as ApiException).statusCode
                        when (statusCode) {
                            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                onGpsListener?.showLocationResolution(e as ResolvableApiException)
                            } catch (sie: SendIntentException) {
                                Log.i(
                                    "TAG :: ",
                                    "PendingIntent unable to execute request."
                                )
                            }
                            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                                val errorMessage =
                                    "Location settings are inadequate, and cannot be " +
                                            "fixed here. Fix in Settings."
                                Log.e("TAG :: ", errorMessage)
                                Toast.makeText(
                                    context,
                                    errorMessage,
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                })
        }
    }

    /**
     * Listener for GPS change
     */
    interface OnGpsListener {
        /**
         * This function will pass resolution required event to parent
         * @param exception : ResolvableApiException
         */
        fun showLocationResolution(exception: ResolvableApiException)

        /**
         * This function will pass the GPS status to parent
         * @param isGPSEnable : Boolean
         */
        fun gpsStatus(isGPSEnable: Boolean)
    }

    init {
        locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        mSettingsClient = LocationServices.getSettingsClient(context)
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10 * 1000
        locationRequest.fastestInterval = 2 * 1000
        val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        mLocationSettingsRequest = builder.build() //**************************
        builder.setAlwaysShow(true) //this is the key ingredient
        //**************************
    } // method for turn on GPS
}