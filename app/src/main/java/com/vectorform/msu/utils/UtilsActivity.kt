package com.vectorform.msu.utils

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.os.Looper
import android.os.Message
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.vectorform.msu.databinding.ProgressLayoutBinding
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress

open class UtilsActivity : AppCompatActivity() {


    private var progressDialog: AlertDialog? = null

    /**
     * This function will show custom Alert message
     * @param title : Int String Resource
     * @param message : String
     * @param listener : DialogInterface.OnClickListener (optional)
     * @param cancelListener : DialogInterface.OnClickListener (optional)
     */
    protected fun showMessage(
        title: String,
        message: String,
        listener: DialogInterface.OnClickListener?,
        cancelListener: DialogInterface.OnClickListener?
    ) {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setNegativeButton("CANCEL", cancelListener)
        builder.setPositiveButton("OK", listener)
        builder.show()
    }

    /**
     * This function will show custom Alert message
     * @param title : Int String Resource
     * @param message : String
     * @param listener : DialogInterface.OnClickListener (optional)
     */
    protected fun showMessage(
        title: String,
        message: String,
        listener: DialogInterface.OnClickListener?
    ) {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK", listener)
        builder.show()
    }

    /**
     * This function will show the loading screen
     */
    protected fun showProgress() {
        if (progressDialog == null) {
            var builder = AlertDialog.Builder(this@UtilsActivity)
            var binding: ProgressLayoutBinding? = ProgressLayoutBinding.inflate(layoutInflater)
            builder.setView(binding?.root)
            progressDialog = builder.create()
            progressDialog?.setCancelable(false)
            progressDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        progressDialog?.apply {
            if (!isShowing && !isFinishing) {
                try {
                    show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    /**
     * This function will hide the loading screen
     */
    protected fun dismissProgress() {
        if (progressDialog != null && progressDialog!!.isShowing && !isFinishing) {
            progressDialog!!.dismiss()
        }
    }

    /**
     * This function will check for Internet connectivity and Access
     */
    fun IsInternetAvailable(checkConnection: CheckConnection) {
        Thread {
            try {
                val timeoutMs = 30000
                val socket = Socket()
                val socketAddress: SocketAddress = InetSocketAddress("8.8.8.8", 53)
                socket.connect(socketAddress, timeoutMs)
                socket.close()
                mHandler.post {
                    checkConnection.hasAccesss(true)
                }
            } catch (e: Exception) {
                mHandler.post {
                    checkConnection.hasAccesss(false)
                }
            }
        }.start()
    }


    /**
     * Interface to listen Internet connection
     */
    interface CheckConnection {
        /**
         * This function will pass the internet access status to parent
         * @param isAvailable : Boolean
         */
        fun hasAccesss(isAvailable: Boolean)
    }

    /**
     * This will notify the evets on Main Thread
     */
    var mHandler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {

        }
    }


    /**
     * This function will check for required permissions
     * @return Boolean if all permissions are granted or not
     */
    protected fun permissionNeeded(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.INTERNET
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_PHONE_STATE
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
    }

}