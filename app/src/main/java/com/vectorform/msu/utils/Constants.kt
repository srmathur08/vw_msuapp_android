package com.vectorform.msu.utils

class Constants {

    companion object {

        val PREF_NAME: String = "MsuPrefs"
        val FIREBASE_DEVICE_TOKEN: String = "FirebaseDeviceToken"
        val NOTIFICATION_ID: Int = 123456
        val DEVICE_TYPE = "Android"


        //Region Login Keys
        val USER_MOBILE_NUMBER = "LoggedInUserMobile"
        val LOGIN_RESPONSE: String = "LoginResponse"
        val USER_LOGGED_IN: String = "UserLoggedIn"
        val LOGGED_IN_USER_DEALERSHIP_NAME: String = "LoggedInUserDealershipName"
        val LOGGED_IN_USER_DEALERSHIP_Id: String = "LoggedInUserDealershipId"
        val LOGGED_IN_USER_EMAIL: String = "LoggedInUserEmail"
        val LOGGED_IN_USER_FULLNAME: String = "LoggedInUserFullName"
        val LOGGED_IN_USER_ROLE: String = "LoggedInUserRole"
        val LOGGED_IN_USER_ROLE_ID: String = "LoggedInUserRoleId"
        val LOGGED_IN_USER_ID: String = "LoggedInUserUserId"
        val PROFILE_PIC_URL: String = "ProfilePicUrl"
        //endregion

        //Region LoginRoles
        val ROLE_SERVICE_TECHNICIAN = "Service Technicians"
        val ROLE_SERVICE_ADVISOR = "Service Advisors"
        //endregion

        /*Boolean Strings*/
        val MTRUE: String = "true"
        val MFALSE: String = "false"

        //Region Vehicle Health Check Section Names
        val SECTION_INSPECTION = "Inspection"
        val SECTION_INTERIOR = "Interior"
        val SECTION_EXTERIOR = "Exterior"
        val SECTION_ENGINE_COMPARTMENT = "Engine Compartment"
        val SECTION_UNKNOWN = "Unknown"
        val SECTION_BOOT = "Boot"
        val SECTION_EXTERIOR_CHECK = "Exterior Check"
        val SECTION_SCRATCH_DENT_AND_DAMAGE = "Scratch/Dent/Damage"
        val SECTION_ACKNOWLEDGE = "Acknowledge"
        val JOIN_SECTION_VEHICLE_IMAGES = "Scratches/Dents/Damages"
        //endregion

        //Region SubSection View Type
        val TYPE_ONE = "Type1"
        val TYPE_TWO = "Type2"
        val TYPE_THREE = "Type3"
        val TYPE_FOUR = "Type4"
        val TYPE_FIVE = "Type5"
        val TYPE_SIX = "Type6"
        val TYPE_SEVEN = "Type7"

        //Color Strings
        val REPAIR_MARKER_COLOR = "#FFD130"
        val DAMAGE_MARKER_COLOR = "#FF335C"


        val SERVICE_REQUEST_DATA = "ServiceRequestData"
        val VEHICLE_CHECK_UP_ID = "VehicleCheckUpId"
        val FUEL_LEVEL = "Fuel Level"

        //Region Service Statuses
        val GET_DIRECTIONS_CLICKED_STATUS = "2"
        val SERVICE_ACTIVE_STATUS = "3"
        val SERVICE_COMPLETED_STATUS = "4"
        //endregion


        //Region Settings API Keys
        val SETTINGS_ANDROID_VERSION = "AndroidVersion"
        val SETTINGS_PLAY_STORE_LINK = "PlayStoreLink"
        //endregion

        //Region DateFormats
        val GENERIC_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
        //endregion

        //Region URL
        val BASEURL: String = baseUrl()

        /**
         * This function will return the Base URL for APIs based on the build type(PRO or UAT)
         * @return String url
         */
        private fun baseUrl(): String {

//            LIVE
//              return "https://vwassistance.com/"

//            Pilot
//            return "http://pos.skodalive.in/vwmsu-pilot/"

//            QA
            return "http://pos.skodalive.in/vwmsu-dev/"

        }
        //endregion
    }
}