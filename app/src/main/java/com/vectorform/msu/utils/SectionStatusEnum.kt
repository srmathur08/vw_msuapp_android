package com.vectorform.msu.utils

import java.io.Serializable

enum class SectionStatusEnum(val id: Int) : Serializable {
    RED(1),
    YELLOW(2),
    GREEN(3);


    /**
     * This function will return the Status Id for Enum Names
     * @return Int
     */
    fun getStatusId(): Int {
        return id
    }
}