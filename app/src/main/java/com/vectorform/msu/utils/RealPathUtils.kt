package com.fcaindia.drive.utils

import android.annotation.TargetApi
import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import com.fcaindia.drive.utils.PathUtils.Companion.generateFileName
import com.fcaindia.drive.utils.PathUtils.Companion.getDocumentCacheDir
import com.fcaindia.drive.utils.PathUtils.Companion.saveFileFromUri
import java.io.File
import java.io.FileOutputStream
import java.util.*

class RealPathUtils {
    companion object {
        @TargetApi(Build.VERSION_CODES.KITKAT)
        fun getPath(context: Context, uri: Uri): String? {

            // DocumentProvider
            if (DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).toTypedArray()
                    val type = split[0]
                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory()
                            .toString() + "/" + split[1]
                    }

                    // TODO handle non-primary volumes
                } else if (isDownloadsDocument(uri)) {
                    val id = DocumentsContract.getDocumentId(uri)
                    if (id != null && id.startsWith("raw:")) {
                        return id.substring(4)
                    }
                    val contentUriPrefixesToTry = arrayOf(
                        "content://downloads/public_downloads",
                        "content://downloads/my_downloads",
                        "content://downloads/all_downloads"
                    )
                    for (contentUriPrefix in contentUriPrefixesToTry) {
                        val contentUri = ContentUris.withAppendedId(
                            Uri.parse(contentUriPrefix),
                            java.lang.Long.valueOf(Objects.requireNonNull(id))
                        )
                        try {
                            val path = getDataColumn(context, contentUri, null, null)
                            if (path != null) {
                                return path
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    // path could not be retrieved using ContentResolver, therefore copy file to accessible cache using streams
                    val fileName: String? = PathUtils.getFileName(context, uri)
                    val cacheDir: File = getDocumentCacheDir(context)
                    val file: File? = generateFileName(fileName, cacheDir)
                    var destinationPath: String? = null
                    if (file != null) {
                        destinationPath = file.absolutePath
                        saveFileFromUri(context, uri, destinationPath)
                    }
                    return destinationPath

                    /*final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);*/
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).toTypedArray()
                    val type = split[0]
                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    val selection = "_id=?"
                    val selectionArgs = arrayOf(
                        split[1]
                    )
                    return getDataColumn(context, contentUri, selection, selectionArgs)
                } // MediaStore (and general)
                else if (isGoogleDriveUri(uri)) {
                    return getDriveFilePath(uri, context)
                } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                    return uri.path
                }
            } else if ("content".equals(uri.scheme, ignoreCase = true)) {

                // Return the remote address
                if (isGooglePhotosUri(uri)) {
                    return uri.lastPathSegment
                } else if (isGoogleDriveUri(uri)) {
                    return uri.lastPathSegment
                }
                return getDataColumn(context, uri, null, null)
            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path
            }
            return null
        }

        private fun getDataColumn(
            context: Context, uri: Uri?, selection: String?,
            selectionArgs: Array<String>?
        ): String? {
            val column = "_data"
            val projection = arrayOf(
                column
            )
            try {
                context.contentResolver
                    .query(uri!!, projection, selection, selectionArgs, null).use { cursor ->
                        if (cursor != null && cursor.moveToFirst()) {
                            val index = cursor.getColumnIndexOrThrow(column)
                            return cursor.getString(index)
                        }
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

        private fun getDriveFilePath(
            uri: Uri,
            context: Context
        ): String? {
            var file: File? = null
            try {
                context.contentResolver
                    .query(uri, null, null, null, null).use { returnCursor ->
                        val nameIndex =
                            Objects.requireNonNull(returnCursor)!!
                                .getColumnIndex(OpenableColumns.DISPLAY_NAME)
                        //            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                        returnCursor!!.moveToFirst()
                        val name = returnCursor.getString(nameIndex)
                        //            String size = (Long.toString(returnCursor.getLong(sizeIndex)));
                        file = File(context.cacheDir, name)
                        val inputStream =
                            context.contentResolver.openInputStream(uri)
                        val outputStream = FileOutputStream(file)
                        var read: Int
                        val maxBufferSize = 1024 * 1024
                        val bytesAvailable =
                            Objects.requireNonNull(inputStream)!!.available()
                        val bufferSize = Math.min(bytesAvailable, maxBufferSize)
                        val buffers = ByteArray(bufferSize)
                        while (inputStream!!.read(buffers).also { read = it } != -1) {
                            outputStream.write(buffers, 0, read)
                        }
                        Log.e("File Size", "Size " + file!!.length())
                        inputStream.close()
                        outputStream.close()
                        Log.e("File Path", "Path " + file!!.path)
                        Log.e("File Size", "Size " + file!!.length())
                    }
            } catch (e: Exception) {
            }
            return if (file != null) {
                file!!.path
            } else {
                null
            }
        }


        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is ExternalStorageProvider.
         */
        private fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is DownloadsProvider.
         */
        private fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is MediaProvider.
         */
        private fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is Google Photos.
         */
        private fun isGooglePhotosUri(uri: Uri): Boolean {
            return "com.google.android.apps.photos.content" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is Google Drive.
         */
        private fun isGoogleDriveUri(uri: Uri): Boolean {
            return "com.google.android.apps.docs.storage" == uri.authority || "com.google.android.apps.docs.storage.legacy" == uri.authority
        }

    }
}
