package com.fcaindia.drive.utils

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Environment
import android.provider.Settings
import android.util.Base64
import android.view.View
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.Logger
import java.io.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


class CommonUtils {

    companion object {

        /**
         * This function will encrypt key/value pair and save in shared preferences
         * @param mContext : Context
         * @param key : String
         * @param value : String (optional)
         */
        fun saveString(mContext: Context, key: String, value: String?) {
            try {
                var aes = AESencrypt()
                var sp = mContext.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
                var editor = sp.edit()
                editor.putString(aes.encrypt(key), aes.encrypt(value))
                editor.apply()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        /**
         * This function will return the decrypted value from shared preferences against key
         * @param mContext : Context
         * @param key : String
         * @return String (default empty screen)
         */
        fun getString(mContext: Context, key: String): String {
            var aes = AESencrypt()
            var sp = mContext.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
            val value = sp.getString(aes.encrypt(key), null)
            return aes.decrypt(value)
        }


        /**
         * This function will encrypt and save the firebase token against key in shared preferences
         * @param mContext : Context
         * @param key : String
         * @param value : String (optional)
         */
        fun saveFirebaseToken(mContext: Context, key: String, value: String?) {
            try {
                var aes = AESencrypt()
                var sp = mContext.getSharedPreferences(
                    Constants.FIREBASE_DEVICE_TOKEN,
                    Context.MODE_PRIVATE
                )
                var editor = sp.edit()
                editor.putString(aes.encrypt(key), aes.encrypt(value))
                editor.apply()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        /**
         * This function will return the decrypted value from shared preferences against key
         * @param mContext : Context
         * @param key : String
         * @return String (default empty screen)
         */
        fun getFirebaseToken(mContext: Context, key: String): String {
            var aes = AESencrypt()
            var sp =
                mContext.getSharedPreferences(Constants.FIREBASE_DEVICE_TOKEN, Context.MODE_PRIVATE)
            val value = sp.getString(aes.encrypt(key), null)
            return aes.decrypt(value)
        }

        /**
         * This function will clear app shared preferences
         * @param context : Context
         */
        fun clearPrefs(context: Context) {
            val sharedPreferences = context.getSharedPreferences(
                Constants.PREF_NAME,
                Context.MODE_PRIVATE
            )
            sharedPreferences.edit().clear().commit()
        }

        /**
         * This function will return the Android Device Id
         * @param mContext : Context
         * @return AndroidId : String
         */
        fun getAndroidId(mContext: Context): String {
            val imei =
                Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
            return Constants.DEVICE_TYPE + imei
        }

        /**
         * This function will create a file on external storage and return the file
         * @param mContext : Context
         * @return file : File
         */
        @Throws(IOException::class)
        fun createImageFile(mContext: Context): File {
            val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val storageDir: File = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
            return File.createTempFile("JPEG_${timeStamp}_", ".jpg", storageDir)
        }

        /**
         * This function will return the background from a view
         * @param view : View
         * @return Bitmap (optional)
         */
        fun getBitmapFromViewBackground(view: View): Bitmap? {
            val returnedBitmap = Bitmap.createBitmap(
                view.width,
                view.height, Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(returnedBitmap)
            val bgDrawable = view.background
            if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
            view.draw(canvas)
            val bs = ByteArrayOutputStream()
            returnedBitmap.compress(Bitmap.CompressFormat.PNG, 50, bs)
            return returnedBitmap
        }

        /**
         * This function will save the captured signatures to external storage
         * @param finalBitmap : Bitmap
         * @param file : File
         */
        fun saveSignature(finalBitmap: Bitmap, file: File) {
            val out = FileOutputStream(file)
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.flush()
            out.close()
        }

        /**
         * This function will return the file name from file path
         * @param path : String
         * @return fileName : String
         */
        fun getFileName(path: String): String {
            return File(path).name
        }

        /**
         * This function will encode the filePath to Base64 string and return BAse64 string
         * @param filepath : String (optional)
         * @return base64String : String
         */
        fun convertBase64(filepath: String?): String {
            val file = File(filepath)
            return Base64.encodeToString(convertFileToBytesArray(file), Base64.DEFAULT)
        }

        /**
         * This function will encode file to byte array
         * @param file : File
         * @return ByteArray (optional)
         */
        private fun convertFileToBytesArray(file: File): ByteArray? {
            val size = file.length().toInt()
            val bytes = ByteArray(size)
            val tmpBuff = ByteArray(size)
            var fis: FileInputStream? = null
            try {
                fis = FileInputStream(file)
                var read = fis.read(bytes, 0, size)
                if (read < size) {
                    var remain = size - read
                    while (remain > 0) {
                        read = fis.read(tmpBuff, 0, remain)
                        System.arraycopy(tmpBuff, 0, bytes, size - remain, read)
                        remain -= read
                    }
                }
            } catch (e: IOException) {
            } finally {
                try {
                    fis!!.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            return bytes
        }


        /**
         * This function will use Geocoder to get address for latitude and longitude
         * @param mContext : Context
         * @param latitude : Double
         * @param longitude : Double
         * @return Address Optional
         */
        fun getAddress(mContext: Context, latitude: Double, longitude: Double): Address? {
            val geocoder: Geocoder
            val addresses: List<Address>
            geocoder = Geocoder(mContext, Locale.getDefault())

            var currentAddress: Address? = null

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1)
                if (addresses != null && addresses.count() > 0) {
                    currentAddress = addresses.get(0)

                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return currentAddress
        }

        fun getPathFromIntent(
            mContext: Context,
            data: Intent
        ): String {
            val uri = data.data
            return RealPathUtils.getPath(mContext, uri!!)!!
        }

        fun getFormattedAmount(amount: Int): String? {
            var value = amount.toFloat()
            val arr = arrayOf("", "K", "M", "B", "T", "P", "E")
            var index = 0
            while (value / 1000 >= 1) {
                value /= 1000
                index++
            }
            val decimalFormat = DecimalFormat("#.##")
            return String.format("%s %s", decimalFormat.format(value), arr[index])
        }

        fun getFormattedDate(dateString : String?) : String{
            dateString?.let {
                val format = SimpleDateFormat(Constants.GENERIC_DATE_TIME_FORMAT)
                try {
                    val date = format.parse(dateString)
                    return SimpleDateFormat("dd-MMM-yyyy").format(date.time)
                }catch (e:Exception){
                    Logger.log("DateException :: ", e.message)
                }
            }
            return ""
        }

        @Throws(IOException::class)
        fun getAssetCacheFile(context: Context, filename: String): String {
            val cacheFile = File(context.cacheDir, filename)
            try {
                val inputStream = context.assets.open(filename)
                try {
                    val outputStream = FileOutputStream(cacheFile)
                    try {
                        val buf = ByteArray(1024)
                        var len: Int
                        while (inputStream.read(buf).also { len = it } > 0) {
                            outputStream.write(buf, 0, len)
                        }
                    } finally {
                        outputStream.close()
                    }
                } finally {
                    inputStream.close()
                }
            } catch (e: IOException) {
                throw IOException("Could not open asset png", e)
            }
            return cacheFile.path
        }
    }

}