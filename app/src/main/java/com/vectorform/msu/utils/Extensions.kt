package com.vectorform.msu.utils

import android.content.res.TypedArray
import android.view.View

// see: https://stackoverflow.com/questions/18382559/how-to-get-an-enum-which-is-created-in-attrs-xml-in-code
inline fun <reified T : Enum<T>> TypedArray.getEnum(index: Int, default: T) =
    getInt(index, -1).let {
        if (it >= 0) enumValues<T>()[it] else default
    }

fun View.showIf(shouldShow: Boolean) {
    this.visibility = when (shouldShow) {
        true -> View.VISIBLE
        false -> View.GONE
    }
}