package com.vectorform.msu.utils

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.fragment.app.Fragment
import com.vectorform.msu.databinding.ProgressLayoutBinding

open class BaseFragment : Fragment(), BaseFragmentContract.IBaseFragment {

    private var progressDialog: AlertDialog? = null

    /**
     * This function will show the loading screen
     */
    override fun showLoading() {
        activity?.let { context ->
            if (progressDialog == null) {
                var builder = AlertDialog.Builder(context)
                var binding: ProgressLayoutBinding? = ProgressLayoutBinding.inflate(layoutInflater)
                builder.setView(binding?.root)
                progressDialog = builder.create()
                progressDialog?.setCancelable(false)
                progressDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
            progressDialog?.apply {
                if (!isShowing && !context.isFinishing) {
                    try {
                        show()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    /**
     * This function will hide the loading screen
     */
    override fun hideLoading() {
        activity?.let { context ->
            if (progressDialog != null && progressDialog!!.isShowing && !context.isFinishing) {
                progressDialog?.dismiss()
            }
        }
    }

    /**
     * This function will show custom Alert message
     * @param title : Int String Resource
     * @param message : String
     * @param listener : DialogInterface.OnClickListener (optional)
     */
    override fun showCustomMessage(
        title: Int,
        message: String,
        listener: DialogInterface.OnClickListener?
    ) {
        activity?.let { context ->
            AlertDialog.Builder(context)?.apply {
                setCancelable(false)
                setTitle(getString(title))
                setMessage(message)
                setPositiveButton("OK", listener)
                show()
            }
        }
    }
}