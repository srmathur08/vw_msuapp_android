package com.fcaindia.drive.utils


import android.util.Base64
import android.util.Log
import java.io.UnsupportedEncodingException
import java.security.NoSuchAlgorithmException
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class AESencrypt {
    private val characterEncoding = "UTF-8"
    private val cipherTransformation = "AES/CBC/PKCS5Padding"
    private val KEY = "gMO1ExV3aJz5t6dA0jFpIsJhZ5DiwK48"

    /**
     * This function will encrypt the provided string and return the encrypted value
     * @param data : String to be encrypted (optional)
     * @return encryptedValue : String or empty string if input param is null or blank
     */
    @Throws(Exception::class)
    fun encrypt(data: String?): String {
        var Data = data
        return if (Data == null || Data == "") {
            ""
        } else {
            Data = Data.trim { it <= ' ' }
            val c = Cipher.getInstance(cipherTransformation)
            c.init(Cipher.ENCRYPT_MODE, generateKey(), generateIv())
            val encVal = c.doFinal(Data.toByteArray(charset(characterEncoding)))
            val encryptedValue =
                Base64.encodeToString(encVal, Base64.NO_WRAP)
            Log.d("CipherText", encryptedValue)
            encryptedValue
        }
    }

    /**
     * This function will decrypt the encrypted string return plain text
     * @param encryptedData : String (optional)
     * @return plainText : String or empty string if input param is null or blank
     */
    @Throws(Exception::class)
    fun decrypt(encryptedData: String?): String {
        return if (encryptedData == null || encryptedData == "") {
            ""
        } else {
            val c = Cipher.getInstance(cipherTransformation)
            c.init(Cipher.DECRYPT_MODE, generateKey(), generateIv())
            val decordedValue =
                Base64.decode(encryptedData, Base64.NO_WRAP)
            val decValue = c.doFinal(decordedValue)
            return String(decValue)
        }
    }

    /**
     * This function will generate the Secret key for encryption/decryption
     * @return SecretKeySpec
     */
    @Throws(UnsupportedEncodingException::class)
    private fun generateKey(): SecretKeySpec {
        val pharseKey = KEY
        return SecretKeySpec(
            pharseKey.toByteArray(charset(characterEncoding)),
            ALGO
        )
    }


    /**
     * This function will generate the IVParameterSpec for encryption/decryption
     * @return IvParameterSpec
     */
    @Throws(
        NoSuchAlgorithmException::class,
        UnsupportedEncodingException::class
    )
    private fun generateIv(): IvParameterSpec {
        val myVector = "@1B2c3D4e5F6g7H8"
        return IvParameterSpec(myVector.toByteArray(charset(characterEncoding)))
    }

    companion object {
        private const val ALGO = "AES"
    }
}