package com.vectorform.msu.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Handler
import android.os.Looper
import android.os.Message
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task


@SuppressLint("MissingPermission")
class AppLocationProvider {

    private var currentLocation: Location? = null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private var userLocationCallBack: UserLocationCallBack? = null
    private var counter = 0

    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null

    /**
     * This function will fetch the current location
     * @param context : Context
     * @param callBack : UserLocationCallBack
     */
    fun getLocation(context: Context, callBack: UserLocationCallBack) {

        userLocationCallBack = callBack
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
        val task: Task<Location> = fusedLocationProviderClient!!.lastLocation
        task.addOnSuccessListener { location ->
            location?.let {
                if (it.accuracy < 55 || counter > 10) {
                    currentLocation = it
                    mHandler.sendEmptyMessage(0)
                } else {
                    ++counter
                    getLocation(context, callBack)
                }
            } ?: createLocalRequest()

        }
    }

    /**
     * This function will create Local request to get current location
     */
    fun createLocalRequest() {
        locationRequest = LocationRequest.create()
        locationRequest?.let {
            it.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            it.setInterval(20 * 1000);

            locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult?.let {
                        for (location in it.getLocations()) {
                            if (location != null) {
                                userLocationCallBack?.locationResult(location)
                                fusedLocationProviderClient?.removeLocationUpdates(locationCallback)
                                break
                            }
                        }
                    }
                }
            }
            fusedLocationProviderClient?.requestLocationUpdates(
                it,
                locationCallback,
                Looper.getMainLooper()
            )
        }

    }

    /**
     * This handler will pass the location on mainthread to update in UI
     */
    private var mHandler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            when (msg.what) {
                0 -> {
                    userLocationCallBack!!.locationResult(currentLocation)
                }
            }
        }
    }

    /**
     * This interface is listener for location events
     */
    interface UserLocationCallBack {
        /**
         * This function will pass the location to parent
         * @param location : Location(Optional)
         */
        fun locationResult(location: Location?)
    }
}