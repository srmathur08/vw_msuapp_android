package com.vectorform.msu.network.sfdc

import android.content.Context
import com.fcaindia.drive.utils.CommonUtils
import com.vectorform.msu.data.responses.sfdc.InstanceUrlResponse
import com.vectorform.msu.data.responses.sfdc.services.SFDCMasterDataResponse
import com.vectorform.msu.utils.Constants

class SfdcCalls(private val safeContext: Context) {

    fun getInstanceUrlandToken(callBack: OnResponse<InstanceUrlResponse>) {
        val url = "https://crm-nscindia.my.salesforce.com/"
        val apiInterface = SfdcApiClient(url).client?.create(SfdcApiInterface::class.java)
        val call = apiInterface?.getInstanceUrlAndToken()
        SfdcNetworkCall(
            safeContext,
            call,
            false,
            object :
                SfdcNetworkCall.OnResponse<InstanceUrlResponse> {
                override fun onSucess(response: InstanceUrlResponse) {
                    callBack.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callBack.onFailure(message)
                }
            })
    }

    fun getSfdcServicesData(
        url: String,
        token: String?,
        vin: String?,
        callBack: OnResponse<SFDCMasterDataResponse>
    ) {
        val apiInterface = SfdcApiClient(url).client?.create(SfdcApiInterface::class.java)
        val call = apiInterface?.getServices(String.format("Bearer %s", token), vin)
        SfdcNetworkCall(
            safeContext,
            call,
            false,
            object :
                SfdcNetworkCall.OnResponse<SFDCMasterDataResponse> {
                override fun onSucess(response: SFDCMasterDataResponse) {
                    callBack.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callBack.onFailure(message)
                }
            })
    }

    interface OnResponse<T> {
        fun onSucess(response: T)
        fun onFailure(message: String)
    }
}