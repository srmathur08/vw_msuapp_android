package com.vectorform.msu.network.sfdc

import com.vectorform.msu.data.responses.sfdc.InstanceUrlResponse
import com.vectorform.msu.data.responses.sfdc.services.SFDCMasterDataResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

interface SfdcApiInterface {

    @POST("services/oauth2/token?grant_type=password&client_id=3MVG9d8..z.hDcPKveQo60.BGf0ajVDkx1QA0ZONk78N.HVxwgRspg35UaJu4t0uFEWu_zbhBVjl.QeuvLmsE&client_secret=5541708532003501955&username=integrationuser@nscindia.com&password=VWKolkata2018")
    fun getInstanceUrlAndToken(): Call<InstanceUrlResponse>

    @GET("/services/apexrest/vindata/{vin}")
    fun getServices(
        @Header("Authorization") Authorization: String,
        @Path("vin") vin: String?
    ): Call<SFDCMasterDataResponse>

}