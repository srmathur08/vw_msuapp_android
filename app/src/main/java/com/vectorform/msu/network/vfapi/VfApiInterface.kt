package com.vectorform.msu.network.vfapi

import com.vectorform.msu.data.responses.vf.CommonResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface VfApiInterface {

    @POST("api/User/Login")
    fun Login(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/SRsByDate")
    fun SRsByDate(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/StartSR")
    fun StartSR(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/CompleteSR")
    fun CompleteSR(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/CreateVC")
    fun CreateVC(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/CreateVCData")
    fun CreateVCData(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/CreateVCDataArray")
    fun CreateVCDataArray(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/CreateVCAsset")
    fun CreateVCAsset(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/CreateVCDataAsset")
    fun CreateVCDataAsset(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/AppendAsset")
    fun AppendAsset(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/GetVCData")
    fun GetVCDataBySrId(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/SR/CompleteVC")
    fun MarkVehicleHealthCheckComplete(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/user/updatelocation")
    fun Updatelocation(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/sr/UpdateSRStatus")
    fun UpdateSRStatus(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/sr/UpdateServiceDetailSR")
    fun UpdateServiceDetailSR(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/User/UpdateProfilePic")
    fun UpdateProfilePic(@Body input: CommonResponse): Call<CommonResponse>

    @GET("api/user/APISetting")
    fun APISetting(): Call<CommonResponse>

    @POST("api/user/Notifications")
    fun Notifications(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/user/NotificationRead")
    fun NotificationRead(@Body input: CommonResponse): Call<CommonResponse>

    @POST("API/SR/ServiceRequestCount")
    fun ServiceRequestCount(@Body input: CommonResponse): Call<CommonResponse>

    @POST("API/User/GetOtp")
    fun GetOtp(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/user/SendSRHistorySMS")
    fun SendSRHistorySMS(@Body input: CommonResponse): Call<CommonResponse>

    @POST("api/user/SendVHCSMS")
    fun SendVHCSMS(@Body input: CommonResponse): Call<CommonResponse>

}