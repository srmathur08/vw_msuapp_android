package com.fcaindia.drive.network

import android.content.Context
import com.fcaindia.drive.utils.AESencrypt
import com.fcaindia.drive.utils.CommonUtils
import com.fcaindia.drive.utils.CommonUtils.Companion.saveString
import com.google.gson.Gson
import com.vectorform.msu.data.input.*
import com.vectorform.msu.data.responses.vf.*
import com.vectorform.msu.network.vfapi.CreateVehicleHealthCheckCall
import com.vectorform.msu.network.vfapi.VfAPiClient
import com.vectorform.msu.network.vfapi.VfApiInterface
import com.vectorform.msu.network.vfapi.VfNetworkCalls
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.Logger


class VfCalls(private val mContext: Context) {

    private val apiInterface: VfApiInterface? =
        VfAPiClient(mContext).client?.create(VfApiInterface::class.java)

    fun Login(request: LoginInput, callback: OnResponse<LoginResponse>) {

        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.Login(input)

        VfNetworkCalls(
            mContext,
            call,
            LoginResponse::class.java,
            object : VfNetworkCalls.onResponse<LoginResponse> {
                override fun onSuccess(loginResponse: LoginResponse) {

                    when (loginResponse.Role) {
                        Constants.ROLE_SERVICE_ADVISOR, Constants.ROLE_SERVICE_TECHNICIAN -> {
                            callback.onSucess(loginResponse)
                        }
                        else -> {
                            callback.onFailure("You don't have permission for login. Please contact admin.")
                        }
                    }


                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun getServicesByDate(request: SRsByDateInput, callback: OnResponse<SRsByDateResponse>) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.SRsByDate(input)

        VfNetworkCalls(
            mContext,
            call,
            SRsByDateResponse::class.java,
            object : VfNetworkCalls.onResponse<SRsByDateResponse> {
                override fun onSuccess(response: SRsByDateResponse) {
                    (response.ServiceRequests as MutableList).removeAll { s -> s.IsCompleted }
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun markServiceStart(request: SRMarkInput, callback: OnResponse<SRMarkResponse>) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.StartSR(input)

        VfNetworkCalls(
            mContext,
            call,
            SRMarkResponse::class.java,
            object : VfNetworkCalls.onResponse<SRMarkResponse> {
                override fun onSuccess(response: SRMarkResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun markServiceComplete(request: SRMarkInput, callback: OnResponse<SRMarkResponse>) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.CompleteSR(input)

        VfNetworkCalls(
            mContext,
            call,
            SRMarkResponse::class.java,
            object : VfNetworkCalls.onResponse<SRMarkResponse> {
                override fun onSuccess(response: SRMarkResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun createVehicleHealthCheck(
        request: CreateVehicleHealthCheckInput,
        callback: OnResponse<CreateVehicleHealthCheckResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.CreateVC(input)

        CreateVehicleHealthCheckCall(
            mContext,
            call,
            object : CreateVehicleHealthCheckCall.onResponse {
                override fun onSuccess(response: CreateVehicleHealthCheckResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun createVHCData(request: CreateVHCDataInput, callback: OnResponse<CreateVHCDataResponse>) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.CreateVCData(input)

        VfNetworkCalls(
            mContext,
            call,
            CreateVHCDataResponse::class.java,
            object : VfNetworkCalls.onResponse<CreateVHCDataResponse> {
                override fun onSuccess(response: CreateVHCDataResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun createVHCDataArray(
        request: CreateVHCDataArrayInput,
        callback: OnResponse<CreateVHCDataArrayResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.CreateVCDataArray(input)

        VfNetworkCalls(
            mContext,
            call,
            CreateVHCDataArrayResponse::class.java,
            object : VfNetworkCalls.onResponse<CreateVHCDataArrayResponse> {
                override fun onSuccess(response: CreateVHCDataArrayResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun createVHCAsset(
        request: CreateVHCAsset,
        callback: OnResponse<CreateVHCAssetResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.CreateVCAsset(input)

        VfNetworkCalls(
            mContext,
            call,
            CreateVHCAssetResponse::class.java,
            object : VfNetworkCalls.onResponse<CreateVHCAssetResponse> {
                override fun onSuccess(response: CreateVHCAssetResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun createVHCDataAsset(
        request: CreateVHCDataAssetInput,
        callback: OnResponse<CreateVHCDataAssetResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.CreateVCDataAsset(input)

        VfNetworkCalls(
            mContext,
            call,
            CreateVHCDataAssetResponse::class.java,
            object : VfNetworkCalls.onResponse<CreateVHCDataAssetResponse> {
                override fun onSuccess(response: CreateVHCDataAssetResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun appendAsset(request: AppendAssetInput, callback: OnResponse<AppendAssetResponse>) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.AppendAsset(input)

        VfNetworkCalls(
            mContext,
            call,
            AppendAssetResponse::class.java,
            object : VfNetworkCalls.onResponse<AppendAssetResponse> {
                override fun onSuccess(response: AppendAssetResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun getVehicleCheckUpDataBySRId(
        request: SRsBySRIdInput,
        callback: OnResponse<VehicleCheckUpDataBySrIdResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.GetVCDataBySrId(input)

        VfNetworkCalls(
            mContext,
            call,
            VehicleCheckUpDataBySrIdResponse::class.java,
            object : VfNetworkCalls.onResponse<VehicleCheckUpDataBySrIdResponse> {
                override fun onSuccess(response: VehicleCheckUpDataBySrIdResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun markVehicleHealthCheckCompleted(
        request: MarkVehicleHealthCheckCompletedInput,
        callback: OnResponse<SRMarkResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.MarkVehicleHealthCheckComplete(input)

        VfNetworkCalls(
            mContext,
            call,
            SRMarkResponse::class.java,
            object : VfNetworkCalls.onResponse<SRMarkResponse> {
                override fun onSuccess(response: SRMarkResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun updateUserLocation(
        request: UserLocationInput,
        callback: OnResponse<SRMarkResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.Updatelocation(input)

        VfNetworkCalls(
            mContext,
            call,
            SRMarkResponse::class.java,
            object : VfNetworkCalls.onResponse<SRMarkResponse> {
                override fun onSuccess(response: SRMarkResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun updateSrStatus(
        request: SrStatusInput,
        callback: OnResponse<SRMarkResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.UpdateSRStatus(input)

        VfNetworkCalls(
            mContext,
            call,
            SRMarkResponse::class.java,
            object : VfNetworkCalls.onResponse<SRMarkResponse> {
                override fun onSuccess(response: SRMarkResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun UpdateServiceDetailSR(
        request: UpdateServiceDetailsInput,
        callback: OnResponse<SRMarkResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.UpdateServiceDetailSR(input)

        VfNetworkCalls(
            mContext,
            call,
            SRMarkResponse::class.java,
            object : VfNetworkCalls.onResponse<SRMarkResponse> {
                override fun onSuccess(response: SRMarkResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun UploadProfilePic(
        request: ProfilePicUploadInput,
        callback: OnResponse<ProfilePicUploadResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }
        val call = apiInterface?.UpdateProfilePic(input)

        VfNetworkCalls(
            mContext,
            call,
            ProfilePicUploadResponse::class.java,
            object : VfNetworkCalls.onResponse<ProfilePicUploadResponse> {
                override fun onSuccess(response: ProfilePicUploadResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun GetSettings(
        callback: OnResponse<SettingsResponse>
    ) {
        val call = apiInterface?.APISetting()
        VfNetworkCalls(
            mContext,
            call,
            SettingsResponse::class.java,
            object : VfNetworkCalls.onResponse<SettingsResponse> {
                override fun onSuccess(response: SettingsResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun GetNotification(
        request : NotificationInput,
        callback: OnResponse<NotificationResponse>
    ) {

        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }

        val call = apiInterface?.Notifications(input)
        VfNetworkCalls(
            mContext,
            call,
            NotificationResponse::class.java,
            object : VfNetworkCalls.onResponse<NotificationResponse> {
                override fun onSuccess(response: NotificationResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun MarkNotification(
        request: NotificationReadInput,
        callback: OnResponse<Any>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }

        val call = apiInterface?.NotificationRead(input)
        VfNetworkCalls(
            mContext,
            call,
            Any::class.java,
            object : VfNetworkCalls.onResponse<Any> {
                override fun onSuccess(response: Any) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun GetDashboardCounts(
        request : DashboardCountInput,
        callback: OnResponse<DashboardCountResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }

        val call = apiInterface?.ServiceRequestCount(input)
        VfNetworkCalls(
            mContext,
            call,
            DashboardCountResponse::class.java,
            object : VfNetworkCalls.onResponse<DashboardCountResponse> {
                override fun onSuccess(response: DashboardCountResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun GetOTP(
        request : OtpInputModel,
        callback: OnResponse<OTPResponseModel>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }

        val call = apiInterface?.GetOtp(input)
        VfNetworkCalls(
            mContext,
            call,
            OTPResponseModel::class.java,
            object : VfNetworkCalls.onResponse<OTPResponseModel> {
                override fun onSuccess(response: OTPResponseModel) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun SendSRHistory(
        request : SRsBySRIdInput,
        callback: OnResponse<SRMarkResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }

        val call = apiInterface?.SendSRHistorySMS(input)
        VfNetworkCalls(
            mContext,
            call,
            SRMarkResponse::class.java,
            object : VfNetworkCalls.onResponse<SRMarkResponse> {
                override fun onSuccess(response: SRMarkResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }

    fun SendVHCSms(
        request : SRsBySRIdInput,
        callback: OnResponse<SRMarkResponse>
    ) {
        val json = Gson().toJson(request)
        Logger.log("Request :: ", json)
        val encryptedRequest = AESencrypt().encrypt(json)
        val input = CommonResponse().apply { data = encryptedRequest }

        val call = apiInterface?.SendVHCSMS(input)
        VfNetworkCalls(
            mContext,
            call,
            SRMarkResponse::class.java,
            object : VfNetworkCalls.onResponse<SRMarkResponse> {
                override fun onSuccess(response: SRMarkResponse) {
                    callback.onSucess(response)
                }

                override fun onFailure(message: String) {
                    callback.onFailure(message)
                }
            })
    }


    interface OnResponse<T> {
        fun onSucess(response: T)
        fun onFailure(message: String)
    }
}