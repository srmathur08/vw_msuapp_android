package com.vectorform.msu.network.vfapi

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import com.fcaindia.drive.utils.AESencrypt
import com.fcaindia.drive.utils.CommonUtils
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.vectorform.msu.R
import com.vectorform.msu.data.responses.vf.CommonResponse
import com.vectorform.msu.data.responses.vf.ResponseParser
import com.vectorform.msu.ui.login.LoginActivity
import com.vectorform.msu.utils.Logger
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException


class VfNetworkCalls<T>(
    private val mContext: Context,
    private val call: Call<CommonResponse>?,
    private val tResult: Class<T>,
    private val delegate: onResponse<T>
) {



    init {
        call?.apply {
            enqueue(object : Callback<CommonResponse> {
                override fun onResponse(
                    call: Call<CommonResponse>?,
                    response: Response<CommonResponse>?
                ) {
                    if (response?.code()!! < 200 || response?.code()!! > 300) {
                        delegate.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                    } else if (response?.body() != null && response.body()?.data != null) {
                        try {

                            val decryptedJson = AESencrypt().decrypt(response.body()?.data)
                            Logger.log("Response :: ", AESencrypt().decrypt(response.body()?.data))

                            var responseParser = Gson().fromJson(
                                decryptedJson,
                                ResponseParser::class.java
                            )
                            if (responseParser.Result != null && responseParser.Result.equals(
                                    "success"
                                )
                            ) {
                                delegate.onSuccess(Gson().fromJson(decryptedJson, tResult))
                            } else if (!TextUtils.isEmpty(responseParser.Reason)) {
                                if (responseParser.Reason.equals("UNAUTHORIZED")) {
                                    CommonUtils.clearPrefs(mContext)
                                    mContext.startActivity(
                                        Intent(
                                            mContext,
                                            LoginActivity::class.java
                                        ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    )
                                } else {
                                    delegate.onFailure(responseParser.Reason!!)
                                }
                            } else if (!TextUtils.isEmpty(responseParser.Reason1)) {
                                delegate.onFailure(responseParser.Reason1!!)
                            } else if (!TextUtils.isEmpty(responseParser.Reason2)) {
                                delegate.onFailure(responseParser.Reason2!!)
                            } else {
                                delegate.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                            }
                        } catch (e: java.lang.Exception) {
                            delegate.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                        }
                    } else {
                        delegate.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                    }
                }

                override fun onFailure(call: Call<CommonResponse>?, t: Throwable?) {
                    Logger.log("API Failure :: ", t?.message ?: "Null Error")
                    when (t) {
                        is UnknownHostException -> {
                            delegate.onFailure(mContext.getString(R.string.no_internet_error))
                        }
                        is SocketTimeoutException -> {
                            delegate.onFailure(mContext.getString(R.string.timeout_error))
                        }
                        is JsonSyntaxException -> {
                            delegate.onFailure(mContext.getString(R.string.parsing_error))
                        }
                        else -> {
                            delegate.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                        }
                    }
                }

            })
        }
            ?: run { delegate.onFailure(mContext.getString(R.string.something_went_wrong_api_message)) }
    }

    interface onResponse<T> {
        fun onSuccess(response: T)
        fun onFailure(message: String)
    }

}