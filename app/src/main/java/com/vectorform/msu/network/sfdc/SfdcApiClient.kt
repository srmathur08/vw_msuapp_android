package com.vectorform.msu.network.sfdc

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class SfdcApiClient(var url: String) {

    private var retrofit: Retrofit? = null

    @get:Synchronized
    val client: Retrofit?
        get() {
            if (retrofit == null) {

                val client = OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS).addNetworkInterceptor(object : Interceptor {
                        override fun intercept(chain: Interceptor.Chain?): Response {
                            val request: Request = chain!!.request()
                            return chain.proceed(request)
                        }

                    })
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS).followRedirects(false)
                    .followSslRedirects(
                        false
                    )
                    .build()


                val gson = GsonBuilder()
                    .setLenient()
                    .create()

                retrofit = Retrofit.Builder()
                    .baseUrl(url)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            }
            return retrofit
        }
}
