package com.vectorform.msu.network.vfapi

import android.content.Context
import com.vectorform.msu.utils.Logger
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

class ResponseIntercepter(var mContext: Context) : Interceptor {

    val newToken: String? = null
    val bodyString: String? = null

    val TAG: String = javaClass.simpleName

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response? {
        var request = chain.request()


        //Build new request
        val builder = request.newBuilder()
        builder.header("Content-Type", "application/json") //if necessary, say to consume JSON
//        builder.header("DeviceId", CommonUtils.getAndroidId(mContext))

        val token: String? = generateToken() //save token of this request for future
        Logger.log("TOKEN :: ", token!!)
        setAuthHeader(builder, token) //write current token to request
        request = builder.build() //overwrite old request
        return chain.proceed(request)
    }

    /**
     * This function will set Authentication Header to request
     * @param builder Request.Builder
     * @param token String
     */
    fun setAuthHeader(
        builder: Request.Builder,
        token: String?
    ) {
        if (token != null) //Add Auth token to each request if authorized
            builder.header("Authorization", String.format("Bearer %s", token))
    }

    /**
     * This function will generate JWS token
     */
    fun generateToken(): String? {
        val currentMillisec = System.currentTimeMillis()
        val secretKey =
            "ERMN05OPLoDvbTTa/QkqLNMI7cPLguaRyHzyg7n5qNBVjQmtBhz4SzBh4NBVCXi3KJHlSXKP+oi2+bXr6CUBTR=="
        val createdDate = Date()
        val expirationDate =
            Date(createdDate.time + TimeUnit.MINUTES.toMillis(10))


        return Jwts.builder()
            .claim(
                "iss",
                "B2lzB29zcGFyazovL3VzL09SR0FOSVpBVElPTi85BmM0Zjk1Ni1mBzliLTQwNjQtBjI1Ny05OWMwNjhlB2ZiNjB"
            )
            .setIssuedAt(createdDate)
            .setSubject(currentMillisec.toString())
            .setExpiration(expirationDate)
            .signWith(SignatureAlgorithm.HS256, secretKey)
            .compact()
    }
}
