package com.vectorform.msu.network.sfdc

import android.content.Context
import com.google.gson.JsonSyntaxException
import com.vectorform.msu.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class SfdcNetworkCall<T>(
    private val mContext: Context,
    call: Call<T>?,
    checkCode: Boolean,
    private val callback: OnResponse<T>
) {

    init {
        call?.let {
            it.enqueue(object : Callback<T?> {

                override fun onResponse(call: Call<T?>?, response: Response<T?>?) {
                    if (response?.code()!! < 200 || response?.code()!! > 300) {
                        callback.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                    } else if (checkCode) {
                        if (response?.code() == 200) {
                            response?.body()?.let { res ->
                                callback.onSucess(res)
                            } ?: run {
                                callback.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                            }
                        } else {
                            callback.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                        }
                    } else {
                        response?.body()?.let { res ->
                            callback.onSucess(res)
                        } ?: run {
                            callback.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                        }
                    }
                }

                override fun onFailure(call: Call<T?>?, t: Throwable?) {
                    t?.let { error ->
                        when (error) {
                            is UnknownHostException -> {
                                callback.onFailure(mContext.getString(R.string.no_internet_error))
                            }
                            is SocketTimeoutException -> {
                                callback.onFailure(mContext.getString(R.string.timeout_error))
                            }
                            is JsonSyntaxException -> {
                                callback.onFailure(mContext.getString(R.string.parsing_error))
                            }
                            else -> {
                                callback.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                            }
                        }
                    }
                        ?: callback.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
                }

            })
        } ?: callback.onFailure(mContext.getString(R.string.something_went_wrong_api_message))
    }

    interface OnResponse<T> {
        fun onSucess(response: T)
        fun onFailure(message: String)
    }
}