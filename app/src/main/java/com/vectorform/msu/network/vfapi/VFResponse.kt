package com.vectorform.msu.network.vfapi

class VFResponse<T> {
    val response: T? = null
}