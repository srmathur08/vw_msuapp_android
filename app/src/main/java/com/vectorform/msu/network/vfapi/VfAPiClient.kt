package com.vectorform.msu.network.vfapi

import android.content.Context
import com.vectorform.msu.utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class VfAPiClient(val context: Context) {

    private var retrofit: Retrofit? = null

    @get:Synchronized
    val client: Retrofit?
        get() {
            if (retrofit == null) {
                val BASE_URL: String = Constants.BASEURL

                val interceptor =
                    ResponseIntercepter(context)

                val client = OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .addInterceptor(interceptor).build()
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }

}