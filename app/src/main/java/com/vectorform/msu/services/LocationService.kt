package com.vectorform.msu.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.fcaindia.drive.network.VfCalls
import com.vectorform.msu.R
import com.vectorform.msu.data.input.UserLocationInput
import com.vectorform.msu.data.responses.vf.SRMarkResponse
import com.vectorform.msu.ui.home.HomeActivity
import com.vectorform.msu.utils.AppLocationProvider
import com.vectorform.msu.utils.Constants
import com.vectorform.msu.utils.Logger

class LocationService : Service() {

    private val CHANNEL_ID = "MSU Location Service"
    private var userId : String? = null

    companion object {
        fun startService(context: Context, message: String) {
            val startIntent = Intent(context, LocationService::class.java)
            startIntent.putExtra(Constants.LOGGED_IN_USER_ID, message)
            ContextCompat.startForegroundService(context, startIntent)
        }
        fun stopService(context: Context) {
            val stopIntent = Intent(context, LocationService::class.java)
            context.stopService(stopIntent)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //do heavy work on a background thread
        val input = intent?.getStringExtra(Constants.LOGGED_IN_USER_ID)
        userId = input
        createNotificationChannel()
        val notificationIntent = Intent(this, HomeActivity::class.java)
        var pendingIntent : PendingIntent ? = null
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity(
                this,
                0, notificationIntent, PendingIntent.FLAG_MUTABLE
            )
        }else{
            pendingIntent = PendingIntent.getActivity(
                this,
                0, notificationIntent, 0
            )
        }
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("MSU App is using your location")
            .setContentText("")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(1, notification)

        getUserLocation()


        //stopSelf();
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "MUS Location Service",
                NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    private fun getUserLocation(){
        AppLocationProvider().getLocation(this, object : AppLocationProvider.UserLocationCallBack{
            override fun locationResult(location: Location?) {
                location?.let { loc->
                    val input = UserLocationInput(userId, loc.latitude.toString(), loc.longitude.toString())
                    VfCalls(this@LocationService).updateUserLocation(input, object: VfCalls.OnResponse<SRMarkResponse>{
                        override fun onSucess(response: SRMarkResponse) {
                            Logger.log("LocationUpdate ::", "Success")
                            Handler(Looper.getMainLooper()).postDelayed({getUserLocation()}, 3000)
                        }

                        override fun onFailure(message: String) {
                            Logger.log("LocationUpdate ::", "Failure")
                            Handler(Looper.getMainLooper()).postDelayed({getUserLocation()}, 3000)
                        }
                    })

                } ?: getUserLocation()
            }
        })
    }
}