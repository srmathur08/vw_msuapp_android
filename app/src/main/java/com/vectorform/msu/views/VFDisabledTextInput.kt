package com.vectorform.msu.views

import android.content.Context
import android.os.Parcelable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import com.vectorform.msu.R
import com.vectorform.msu.databinding.CustomTextInputDisabledBinding
import com.vectorform.msu.utils.getEnum

class VFDisabledTextInput : ConstraintLayout {

    private var binding: CustomTextInputDisabledBinding? = null
    private var hintString: String? = null
    private var textString: String? = null
    private var inputType: VFInputType = VFInputType.TEXT

    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(
        context: Context,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        isSaveEnabled = true
        attrs?.let {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.VFTextInput, 0, 0)
            try {
                hintString = ta.getString(R.styleable.VFTextInput_VFTextInputHintText)
                textString = ta.getString(R.styleable.VFTextInput_VFTextInputText)
                inputType = ta.getEnum(R.styleable.VFTextInput_VFTextInputType, VFInputType.TEXT)
            } finally {
                ta.recycle()
            }
            binding?.textFieldTitleDisabled?.text = hintString ?: ""
            binding?.editCustomDisabled?.setText(textString ?: "")
            binding?.editCustomDisabled?.inputType = inputType.type

            if (inputType == VFInputType.NUMBER) {
                binding?.editCustomDisabled?.keyListener =
                    DigitsKeyListener.getInstance("0123456789")
                binding?.editCustomDisabled?.filters = arrayOf<InputFilter>(LengthFilter(10))
            }
        }
    }

    init {
        binding = CustomTextInputDisabledBinding.inflate(LayoutInflater.from(context), this, true)
    }

    val value: String
        get() = binding?.editCustomDisabled?.text.toString()

    fun setValue(value: String?) {
        binding?.editCustomDisabled?.setText(value)
    }

    fun setHint(hintStr: String) {
        binding?.editCustomDisabled?.hint = hintStr
    }

    fun addWatcher(watcher: TextWatcher) {
        binding?.editCustomDisabled?.addTextChangedListener(watcher)
    }

    fun removeWatcher(watcher: TextWatcher) {
        binding?.editCustomDisabled?.removeTextChangedListener(watcher)
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState: Parcelable? = super.onSaveInstanceState()
        val customSavedState = CustomSavedState(superState)
        customSavedState.savedText = binding?.editCustomDisabled?.text.toString()
        customSavedState.savedHint = binding?.editCustomDisabled?.hint.toString()
        return customSavedState
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        val customSavedState = state as CustomSavedState
        super.onRestoreInstanceState(customSavedState.superState)
        binding?.editCustomDisabled?.setText(customSavedState.savedText ?: "")
        binding?.editCustomDisabled?.hint = customSavedState.savedHint ?: ""
    }

    /**
     * Type needs to be a byte. Using the Text.InputType enum will not work.
     */
    enum class VFInputType(val type: Int) {
        TEXT(0x00000001),
        PASSWORD(0x00000081),
        EMAIL(0x00000021),
        NUMBER(0x00000031)
    }


    class CustomSavedState : View.BaseSavedState {

        var savedText: String? = null
        var savedHint: String? = null

        constructor(superState: Parcelable?) : super(superState) {
        }
    }
}