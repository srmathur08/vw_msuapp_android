package com.vectorform.msu.views

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.text.method.TransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.vectorform.msu.R
import com.vectorform.msu.databinding.CustomTextInputInverseBinding
import com.vectorform.msu.utils.getEnum

class VFTextInputInverse : ConstraintLayout, TextWatcher, View.OnFocusChangeListener {

    private var binding: CustomTextInputInverseBinding? = null
    private var errorString: String? = null
    private var hintString: String? = null
    private var textString: String? = null
    private var inputType: VFInputType = VFInputType.TEXT

    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(
        context: Context,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        isSaveEnabled = true
        attrs?.let {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.VFTextInput, 0, 0)
            try {
                hintString = ta.getString(R.styleable.VFTextInput_VFTextInputHintText)
                textString = ta.getString(R.styleable.VFTextInput_VFTextInputText)
                inputType = ta.getEnum(R.styleable.VFTextInput_VFTextInputType, VFInputType.TEXT)
                val hideError = ta.getBoolean(R.styleable.VFTextInput_VFTextInputHideError, false)
                if (hideError) {
                    binding?.textCustomError?.visibility = View.GONE
                }
            } finally {
                ta.recycle()
            }
            binding?.textFieldTitleInverse?.text = hintString ?: ""
            binding?.editCustomInverse?.setText(textString ?: "")
            binding?.editCustomInverse?.inputType = inputType.type


            if (inputType == VFInputType.NUMBER) {
                binding?.editCustomInverse?.keyListener =
                    DigitsKeyListener.getInstance("0123456789.")
//                binding?.editCustomInverse?.filters = arrayOf<InputFilter>(LengthFilter(10))
            }
        }
    }

    init {
        binding = CustomTextInputInverseBinding.inflate(LayoutInflater.from(context), this, true)
        binding?.editCustomInverse?.addTextChangedListener(this)
    }

    val value: String
        get() = binding?.editCustomInverse?.text.toString()

    fun setValue(value: String?) {
        binding?.editCustomInverse?.setText(value)
    }

    fun setError(errorStr: String? = null) {
        errorString = errorStr
        refreshView()
    }

    fun setHint(hintStr: String) {
        binding?.editCustomInverse?.hint = hintStr
    }

    fun setHintTitle(hintStr: String) {
        hintString = hintStr
        binding?.textFieldTitleInverse?.text = hintString
    }

    fun addWatcher(watcher: TextWatcher) {
        binding?.editCustomInverse?.addTextChangedListener(watcher)
    }

    fun removeWatcher(watcher: TextWatcher) {
        binding?.editCustomInverse?.removeTextChangedListener(watcher)
    }

    private fun refreshView() {
        when (errorString) {
            null -> {
                binding?.textCustomError?.text = ""
                binding?.viewBorderLineInverse?.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.textinput_borderline
                    )
                )
            }
            else -> {
                binding?.textCustomError?.text = errorString
                binding?.viewBorderLineInverse?.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.alert
                    )
                )
            }
        }
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState: Parcelable? = super.onSaveInstanceState()
        val customSavedState = CustomSavedState(superState)
        customSavedState.transitionState = binding?.layoutTextField?.transitionState
        customSavedState.savedText = binding?.editCustomInverse?.text.toString()
        customSavedState.savedErrorText = binding?.textCustomError?.text.toString()
        customSavedState.savedHint = binding?.editCustomInverse?.hint.toString()
        return customSavedState
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        val customSavedState = state as CustomSavedState
        super.onRestoreInstanceState(customSavedState.superState)
        binding?.layoutTextField?.transitionState = customSavedState?.transitionState
        binding?.editCustomInverse?.setText(customSavedState.savedText ?: "")
        binding?.textCustomError?.text = customSavedState.savedErrorText ?: ""
        binding?.editCustomInverse?.hint = customSavedState.savedHint ?: ""
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        errorString?.let {
            errorString = null
            refreshView()
        }
        textString = binding?.editCustomInverse?.text.toString()
        when (binding?.editCustomInverse?.text.toString().trim().length) {
            0 -> {
                binding?.layoutTextField?.transitionToState(R.id.field_start_inverse)
            }
            else -> {
                binding?.layoutTextField?.transitionToState(R.id.field_end_inverse)
            }
        }
    }

    override fun afterTextChanged(s: Editable?) {}

    /**
     * Type needs to be a byte. Using the Text.InputType enum will not work.
     */
    enum class VFInputType(val type: Int) {
        TEXT(0x00000001),
        PASSWORD(0x00000081),
        EMAIL(0x00000021),
        NUMBER(0x00000031)
    }

    /**
     * Called when the focus state of a view has changed.
     *
     * @param v The view whose state has changed.
     * @param hasFocus The new focus state of v.
     */
    override fun onFocusChange(v: View?, hasFocus: Boolean) {
//        binding?.viewBorderLineInverse?.setBackgroundColor(ContextCompat.getColor(context, if (hasFocus) R.color.textinput_borderline_focused else R.color.textinput_borderline))
    }

    fun addFocusChangeListener(focusListener: OnFocusChangeListener) {
        binding?.editCustomInverse?.onFocusChangeListener = focusListener
    }

    fun setTransformationMethod(transformationMethod: TransformationMethod?) {
        binding?.apply {
            editCustomInverse.transformationMethod = transformationMethod
        }
    }

    fun restoreLayoutTransition() {
        binding?.layoutTextField?.transitionState = binding?.layoutTextField?.transitionState
    }

    class CustomSavedState : View.BaseSavedState {

        var savedText: String? = null
        var savedHint: String? = null
        var savedErrorText: String? = null
        var transitionState: Bundle? = null

        constructor(superState: Parcelable?) : super(superState) {
        }
    }
}