package com.vectorform.msu.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.vectorform.msu.R

class SignatureView : View {
    val STROKE_WIDTH = 10f
    val HALF_STROKE_WIDTH: Float = STROKE_WIDTH / 2
    val paint = Paint()
    val path = Path()
    var signatureEvents: ISignatureEvents? = null
    var lastTouchX = 0f
    var lastTouchY = 0f
    val dirtyRect = RectF()

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        paint.isAntiAlias = true
        paint.color = Color.BLACK
        paint.style = Paint.Style.STROKE
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeWidth = STROKE_WIDTH
        setBackgroundResource(R.color.white)
    }

    fun clear() {
        path.reset()
        invalidate()
    }

    fun setSignatureEventListener(ise: ISignatureEvents) {
        this.signatureEvents = ise
    }

    override fun onDraw(canvas: Canvas) {
        // TODO Auto-generated method stub
        canvas.drawPath(path, paint)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val eventX = event.x
        val eventY = event.y
        if (signatureEvents != null) {
            signatureEvents?.onSigning()
        } else {
            return true
        }
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                path.moveTo(eventX, eventY)
                lastTouchX = eventX
                lastTouchY = eventY
                return true
            }
            MotionEvent.ACTION_MOVE, MotionEvent.ACTION_UP -> {
                resetDirtyRect(eventX, eventY)
                val historySize = event.historySize
                var i = 0
                while (i < historySize) {
                    val historicalX = event.getHistoricalX(i)
                    val historicalY = event.getHistoricalY(i)
                    path.lineTo(historicalX, historicalY)
                    i++
                }
                path.lineTo(eventX, eventY)
            }
        }
        invalidate(
            (dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
            (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
            (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
            (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt()
        )
        lastTouchX = eventX
        lastTouchY = eventY
        return true
    }

    private fun resetDirtyRect(eventX: Float, eventY: Float) {
        dirtyRect.left = Math.min(lastTouchX, eventX)
        dirtyRect.right = Math.max(lastTouchX, eventX)
        dirtyRect.top = Math.min(lastTouchY, eventY)
        dirtyRect.bottom = Math.max(lastTouchY, eventY)
    }

    interface ISignatureEvents {
        fun onSigning()
    }

}
