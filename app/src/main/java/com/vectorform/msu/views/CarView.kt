package com.vectorform.msu.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import com.vectorform.msu.data.input.MarkersValueInput
import com.vectorform.msu.ui.vehiclehealthcheck.secondvehiclehealthcheck.pager.threesixtyview.ImageMarkerModel
import com.vectorform.msu.utils.MarkerEnum


class CarView : AppCompatImageView {

    private var filePathList: List<String>? = null
    private var position: Int = 0
    private var mPaint: Paint = Paint()
    private val markers = ArrayList<ImageMarkerModel>()
    private var canSwitchImages = true
    private var markerListener: ICarView? = null
    private var isDrawingEnabled: Boolean = false

    private var imageHeight: Int = 0
    private var imageWidth: Int = 0


    /**
     * This function will initialize ICarView event listener
     * @param listener: ICarView
     */
    fun setMarkerListener(listener: ICarView) {
        markerListener = listener
    }

    init {
        mPaint.color = Color.TRANSPARENT
        mPaint.style = Paint.Style.FILL_AND_STROKE
    }

    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs, 0) {
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        filePathList?.let { images ->
            for (m in markers) {
                when (m.imagePath) {
                    images[position] -> {
                        for (mar in m.markers) {
                            mPaint.color = MarkerEnum.valueOf(mar.mark).getMarkerColor()
                            canvas?.drawCircle(
                                (mar.x * imageWidth) / 100,
                                (mar.y * imageHeight) / 100,
                                15f,
                                mPaint
                            )
                        }
                    }
                }
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    /**
     * This function will set the image from images list in view
     * @param images : List<String> image paths
     */
    fun setImageList(images: List<String>) {
        filePathList?.let { file ->
            (file as MutableList).clear()
        } ?: run { filePathList = ArrayList() }

        (filePathList as MutableList).addAll(images)
        switchImages(position)
    }


    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                if (isDrawingEnabled) {
                    filePathList?.let { images ->

                        val mark = markers.filter { m -> m.imagePath == images[position] }
                        if (mark.isNotEmpty()) {
                            val newMark = MarkersValueInput(
                                MarkerEnum.getMarkerName(mPaint.color),
                                (event.x * 100) / imageWidth,
                                (event.y * 100) / imageHeight
                            )
                            mark[0].markers.add(newMark)
//                            markerListener?.markerAdded(mark[0].imagePath, newMark)
                        }
                        invalidate()
                    }
                    return true
                }
                return super.onTouchEvent(event)
            }
            else -> return super.onTouchEvent(event)
        }
    }


    /**
     * This function will switch images based on position
     * @param pos : Int
     */
    private fun switchImages(pos: Int) {
        filePathList?.let { images ->
            setImageURI(Uri.parse(images[pos]))
            invalidate()
        }
    }

    /**
     * This function will update the Paint color
     * @param color : Int
     */
    fun setPainColor(color: Int) {
        mPaint?.color = color
    }

    /**
     * This function will update view status to allow/restrict switch image
     * @param flag : Boolean
     */
    fun setCanSwitchImage(flag: Boolean) {
        canSwitchImages = flag
    }

    /**
     * This function will return the  list Markers on an image
     * @return ArrayList<ImageMarkerModel>
     */
    fun getPoints(): ArrayList<ImageMarkerModel> {
        return markers
    }

    /**
     * This function will set the markers
     * @param points : ArrayList<ImageMarkerModel>
     */
    fun setMarkers(points: ArrayList<ImageMarkerModel>) {
        markers.clear()
        markers.addAll(points)
        invalidate()
    }

    /**
     * This function will enable/Disable the marker Draw
     * @param flag : Boolean
     */
    fun enableDrawing(flag: Boolean) {
        isDrawingEnabled = flag
    }

    /**
     * This function will update the view with new image
     */
    fun nextImage() {
        filePathList?.let { images ->
            if (position < images.size - 1) {
                switchImages(++position)
            } else {
                position = 0
                switchImages(position)
            }
        }
    }

    /**
     * This function will update the view with previous image
     */
    fun previousImage() {
        filePathList?.let { images ->
            if (position == 0) {
                position = images.size - 1
                switchImages(position)
            } else {
                switchImages(--position)
            }
        }
    }

    /**
     * This function will remove the last marker
     */
    fun undo() {
        filePathList?.let { images ->
            val current = markers.filter { m -> m.imagePath.equals(images[position]) }
            if (current.isNotEmpty() && current[0].markers.isNotEmpty()) {
                current[0].markers.removeAt(current[0].markers.size - 1)
                invalidate()
            }
        }
    }

    /**
     * This function will show the image at index
     * @param index : Int
     */
    fun setImageIndex(index: Int) {
        position = index
        switchImages(index)
    }

    /**
     * This function will return the index of current selected image
     * @return Int
     */
    fun getCurrentImageIndex(): Int {
        return position
    }

    /**
     * This function will update the width and height of the container to draw marker for any resolution in percentage
     * @param width: Int (width of car container)
     * @param height: Int (height of car container)
     */
    fun setContainerHeightWidth(width: Int, height: Int) {
        imageWidth = width
        imageHeight = height
    }

    /**
     * Listener to Car view events
     */
    interface ICarView {
        /**
         * This function will notify parent about the newly added marker
         * @param image : String
         * @param maeker : MarkersValueInput
         */
        fun markerAdded(image: String, marker: MarkersValueInput)
    }
}