package com.vectorform.msu.views

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.Parcelable
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.text.method.TransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.Nullable
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.vectorform.msu.R
import com.vectorform.msu.databinding.CustomTextInputBinding
import com.vectorform.msu.utils.getEnum

class VFTextOTPInput : ConstraintLayout, TextWatcher, View.OnFocusChangeListener {

    private var binding: CustomTextInputBinding? = null
    private var errorString: String? = null
    private var hintString: String? = null
    private var textString: String? = null

    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(
        context: Context,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        isSaveEnabled = true
        attrs?.let {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.VFTextInput, 0, 0)
            try {
                hintString = ta.getString(R.styleable.VFTextInput_VFTextInputHintText)
                textString = ta.getString(R.styleable.VFTextInput_VFTextInputText)
            } finally {
                ta.recycle()
            }
            binding?.textFieldTitle?.text = hintString ?: ""
            binding?.editCustom?.setText(textString ?: "")

            binding?.editCustom?.inputType = InputType.TYPE_CLASS_NUMBER

            binding?.editCustom?.keyListener = DigitsKeyListener.getInstance("0123456789")
            binding?.editCustom?.filters = arrayOf<InputFilter>(LengthFilter(6))

            binding?.layoutTextField?.setTransitionListener(object: MotionLayout.TransitionListener{
                override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
//                    TODO("Not yet implemented")
                }

                override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
//                    TODO("Not yet implemented")
                }

                override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                    Handler(Looper.getMainLooper()).postDelayed({binding?.editCustom?.requestFocus()}, 500)
                }

                override fun onTransitionTrigger(
                    p0: MotionLayout?,
                    p1: Int,
                    p2: Boolean,
                    p3: Float
                ) {
//                    TODO("Not yet implemented")
                }
            })
        }
    }

    init {
        binding = CustomTextInputBinding.inflate(LayoutInflater.from(context), this, true)
        binding?.editCustom?.addTextChangedListener(this)
    }

    val value: String
        get() = binding?.editCustom?.text.toString()

    fun setValue(value: String) {
        binding?.editCustom?.setText(value)
    }

    fun setError(errorStr: String? = null) {
        errorString = errorStr
        refreshView()
    }

    fun setHint(hintStr: String) {
        binding?.editCustom?.hint = hintStr
    }

    fun addWatcher(watcher: TextWatcher) {
        binding?.editCustom?.addTextChangedListener(watcher)
    }

    fun removeWatcher(watcher: TextWatcher) {
        binding?.editCustom?.removeTextChangedListener(watcher)
    }

    private fun refreshView() {
        when (errorString) {
            null -> {
                binding?.textCustomError?.text = ""
                binding?.viewBorderLine?.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.textinput_borderline
                    )
                )
            }
            else -> {
                binding?.textCustomError?.text = errorString
                binding?.viewBorderLine?.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.alert
                    )
                )
            }
        }
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState: Parcelable? = super.onSaveInstanceState()
        val customSavedState = CustomSavedState(superState)
        customSavedState.savedText = binding?.editCustom?.text.toString()
        customSavedState.savedErrorText = binding?.textCustomError?.text.toString()
        customSavedState.savedHint = binding?.editCustom?.hint.toString()
        return customSavedState
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        val customSavedState = state as CustomSavedState
        super.onRestoreInstanceState(customSavedState.superState)
        binding?.editCustom?.setText(customSavedState.savedText ?: "")
        binding?.textCustomError?.text = customSavedState.savedErrorText ?: ""
        binding?.editCustom?.hint = customSavedState.savedHint ?: ""
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        errorString?.let {
            errorString = null
            refreshView()
        }
        textString = binding?.editCustom?.text.toString()
        when (binding?.editCustom?.text.toString().trim().length) {
            0 -> {
                binding?.layoutTextField?.transitionToState(R.id.field_start)
            }
            else -> {
                binding?.layoutTextField?.transitionToState(R.id.field_end)
            }
        }
    }

    override fun afterTextChanged(s: Editable?) {}

    /**
     * Type needs to be a byte. Using the Text.InputType enum will not work.
     */
    enum class VFInputType(val type: Int) {
        TEXT(0x00000001),
        PASSWORD(0x00000081),
        EMAIL(0x00000021),
        NUMBER(0x00000031)
    }

    /**
     * Called when the focus state of a view has changed.
     *
     * @param v The view whose state has changed.
     * @param hasFocus The new focus state of v.
     */
    override fun onFocusChange(v: View?, hasFocus: Boolean) {

    }

    fun addFocusChangeListener(focusListener: OnFocusChangeListener) {
        binding?.editCustom?.onFocusChangeListener = focusListener
    }

    fun setTransformationMethod(transformationMethod: TransformationMethod?) {
        binding?.apply {
            editCustom.transformationMethod = transformationMethod
        }
    }

    class CustomSavedState : View.BaseSavedState {

        var savedText: String? = null
        var savedHint: String? = null
        var savedErrorText: String? = null

        constructor(superState: Parcelable?) : super(superState) {
        }
    }
}