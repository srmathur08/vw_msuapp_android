package com.vectorform.msu.data.input

data class SRsByDateInput(
    val DealershipId: String,
    val ServiceDate: String,
    val Assignee: String
)