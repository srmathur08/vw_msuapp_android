package com.vectorform.msu.data.input

data class SrStatusInput(val Srid : String, val StatusId : String?, val ServiceOther : String?, val Revenue : String?)
