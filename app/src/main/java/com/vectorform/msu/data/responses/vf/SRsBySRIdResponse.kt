package com.vectorform.msu.data.responses.vf

data class SRsBySRIdResponse(
    val Result: String,
    val VehicleCheckup: VehicleCheckupItem
) {
    data class VehicleCheckupItem(
        val FuelLevel: Double,
        val Id: Int,
        val Srid: Int,
        val VehicleCheckupAssets: List<VehicleCheckupAsset>,
        val VehicleCheckupData: List<VehicleCheckupDataItem>,
        val VehicleMileage: String
    ) {
        data class VehicleCheckupAsset(
            val Asset: String,
            val Id: Int,
            val Thumb: String
        )

        data class VehicleCheckupDataItem(
            val Assets: List<Asset>,
            val Id: Int,
            val Name: String,
            val Remarks: String,
            val SectionName: String,
            val Status: Int,
            val Value: String,
            val VehicleCheckupId: Int
        ) {
            data class Asset(
                val Asset: String,
                val Thumb: String
            )
        }
    }
}