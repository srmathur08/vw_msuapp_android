package com.vectorform.msu.data.responses.sfdc.services

import java.io.Serializable

data class SFDCMasterDataResponse(
    val Dealer_PSF_Question_rated_less_than_4_star: Any,
    val Message: String,
    val Overall_customer_satisfaction_score: String,
    val Service_History_Data: List<ServiceHistoryData>?,
    val customerMasterData: CustomerMasterData,
    val vehicleMasterData: VehicleMasterData
) : Serializable