package com.vectorform.msu.data.input

data class CreateVHCDataArrayInput(
    val CheckupData: List<CreateVHCDataInput>,
    val VehicleCheckupId: Int
)