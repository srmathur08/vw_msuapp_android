package com.vectorform.msu.data.responses.vf

class NotificationResponse {
    var Result : String? = null
    var data : List<Notifications>? = null
}

class Notifications{
    var Id : Int = 0
    var Title : String? = null
    var Description: String? = null
    var ModuleType : String? = null
    var ModuleId : String? = null
    var SubModuleId : String? = null
    var Assignee : String? = null
    var Action : String? = null
    var IsRead : Boolean = false
    var CreatedDate : String? = null
}