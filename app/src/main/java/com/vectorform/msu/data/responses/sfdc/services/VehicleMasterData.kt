package com.vectorform.msu.data.responses.sfdc.services

import java.io.Serializable

data class VehicleMasterData(
    val First_Delivery_Date__c: String,
    val Id: String,
    val Product__c: String,
    val Product__r: ProductR,
    val VW_ModelDesc__c: String,
    val VW_ModelYear__c: String,
    val Variant_name__c: String,
    val attributes: Attributes
) : Serializable