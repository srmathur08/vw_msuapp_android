package com.vectorform.msu.data.responses.vf

data class CreateVehicleHealthChcekFailureResponse(
    val Reason: String,
    val Result: String,
    val VehicleCheckupId: Int
)