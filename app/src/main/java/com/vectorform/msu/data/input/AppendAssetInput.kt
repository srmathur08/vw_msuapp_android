package com.vectorform.msu.data.input

data class AppendAssetInput(
    val Asset: String,
    val AssetPath: String,
    val IsLastChunk: Boolean
)