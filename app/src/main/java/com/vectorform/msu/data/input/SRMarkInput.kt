package com.vectorform.msu.data.input

data class SRMarkInput(
    val Srid: Int
)