package com.vectorform.msu.data.responses.vf

import java.io.Serializable

data class SRsByDateResponse(
    val Result: String,
    val ServiceRequests: List<ServiceRequest>
) : Serializable {
    data class ServiceRequest(
        val CustomerName: String?,
        val Duration: String?,
        val InvoiceAmount: Double,
        val IsBatteryCheck: Boolean,
        val IsAirFilter : Boolean,
        val IsBrakes: Boolean,
        val IsBrakesDisk : Boolean,
        val IsOilChange: Boolean,
        val IsTyreCheck: Boolean,
        val IsSuspensionStrut : Boolean,
        val IsShockAbsorber : Boolean,
        val IsControlArm : Boolean,
        val IsBulbReplace : Boolean,
        val IsHornReplace : Boolean,
        val IsTyreReplace : Boolean,
        val Lat: Double,
        val Lng: Double,
        val OdometerReading: String?,
        val SFDCOdometerReading: String?,
        val RegNum: String?,
        val ServiceType: String?,
        val Srid: Int,
        val Srnumber: String?,
        val VariantName: String?,
        val VehicleImage: String?,
        val VehicleModel: String?,
        val Vinnumber: String?,
        val IsStarted: Boolean,
        val IsCompleted: Boolean,
        var IsVCCompleted : Boolean,
        var Type : String?,
        var ServiceOther : String?,
        val Revenue : String?,
        val MobileNumber : String?
    ) : Serializable
}