package com.vectorform.msu.data.input

data class CreateVHCAsset(
    val Asset: String,
    val AssetBase64String: String,
    val IsLastChunk: Boolean,
    val VehicleCheckupId: Int
)