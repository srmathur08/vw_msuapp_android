package com.vectorform.msu.data.input

class UpdateServiceDetailsInput {

    var Srid : String? = null
    var IsOilChange : Boolean = false
    var IsAirFilter  : Boolean = false
    var IsBrakes : Boolean = false
    var IsBrakesDisk : Boolean = false
    var IsSuspensionStrut : Boolean = false
    var IsShockAbsorber : Boolean = false
    var IsControlArm : Boolean = false
    var IsBatteryCheck : Boolean = false
    var IsBulbReplace : Boolean = false
    var IsHornReplace : Boolean = false
    var IsTyreCheck : Boolean = false
    var IsTyreReplace  : Boolean = false
}