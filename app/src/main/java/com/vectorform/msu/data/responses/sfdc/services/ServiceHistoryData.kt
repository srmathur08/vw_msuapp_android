package com.vectorform.msu.data.responses.sfdc.services

import java.io.Serializable

data class ServiceHistoryData(
    val CreatedDate: String,
    val Customer_Voice__c: String,
    val Delivery_Date_Nadcon__c: String,
    val Id: String,
    val Mileage_Out__c: Int,
    val Item_Amount__c: String,
    val Labor_Amount__c: String,
    val Amount__c: String,
    val Post_Service_feedback_for_VW_HQ__c: Boolean,
    val RO_Closed_Date__c: String,
    val RO_Type__c: String,
    val RecordTypeId: String,
    val Repair_Order__c: String,
    val Repair_Order__r: RepairOrderR,
    val Selling_Dealer_Code__c: String,
    val Selling_Dealer__c: String,
    val Selling_Dealer__r: SellingDealerR,
    val Service_Dealer__c: String,
    val Service_Dealer__r: ServiceDealerR,
    val VW_Brand__c: String,
    val VW_RO_Number__c: String,
    val VW_Registration_No__c: String,
    val VW_VIN__c: String,
    val attributes: Attributes
) : Serializable