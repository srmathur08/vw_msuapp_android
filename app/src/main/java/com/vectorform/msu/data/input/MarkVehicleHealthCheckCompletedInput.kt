package com.vectorform.msu.data.input

data class MarkVehicleHealthCheckCompletedInput(
    val Id: Int
)