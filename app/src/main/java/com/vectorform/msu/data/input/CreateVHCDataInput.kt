package com.vectorform.msu.data.input

data class CreateVHCDataInput(
    val Name: String?,
    val Remarks: String?,
    val SectionName: String?,
    val Status: Int,
    val Value: String?,
    val VehicleCheckupId: Int
)