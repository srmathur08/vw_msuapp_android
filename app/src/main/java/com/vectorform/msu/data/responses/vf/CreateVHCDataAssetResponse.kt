package com.vectorform.msu.data.responses.vf

data class CreateVHCDataAssetResponse(
    val AssetPath: String,
    val Result: String,
    val VehicleCheckupDataAssetId: Int
)