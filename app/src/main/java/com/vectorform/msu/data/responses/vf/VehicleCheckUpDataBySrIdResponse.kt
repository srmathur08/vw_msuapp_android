package com.vectorform.msu.data.responses.vf

data class VehicleCheckUpDataBySrIdResponse(
    val Result: String,
    val VehicleCheckup: List<VehicleCheckupData>
) {
    data class VehicleCheckupData(
        val FuelLevel: Double,
        val Id: Int,
        val Srid: Int,
        val VehicleCheckupAssets: List<VehicleAssetItem>,
        val VehicleCheckupData: List<VehicleCheckupDataItem>,
        val VehicleMileage: String
    ) {
        data class VehicleCheckupDataItem(
            val Assets: List<Asset>,
            val Id: Int,
            val Name: String,
            val Remarks: String,
            val SectionName: String,
            val Status: Int,
            val Value: String,
            val VehicleCheckupId: Int
        ) {
            data class Asset(
                val Asset: String,
                val AssetBase64String: String,
                val Id: Int,
                val IsLastChunk: Boolean,
                val IsVideo: Boolean,
                val Thumb: String,
                val VehicleCheckupDataId: Int
            )
        }
    }
}