package com.vectorform.msu.data.input

data class SRsBySRIdInput(
    val Srid: Int
)