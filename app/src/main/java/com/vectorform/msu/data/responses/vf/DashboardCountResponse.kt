package com.vectorform.msu.data.responses.vf

class DashboardCountResponse {
    var Result: String? = null
    var serviceRequests: DashboardCounts? = null
}

class DashboardCounts {
    var Service_Assigned_Today: Int = 0
    var Service_Completed_Today: Int = 0
    var Service_Missed_Today: Int = 0
    var Estimate_Amount_Today: Int = 0
    var Service_Assigned_Month: Int = 0
    var Service_Completed_Month: Int = 0
    var Service_Missed_Month: Int = 0
    var Estimate_Amount_Month: Int = 0
}