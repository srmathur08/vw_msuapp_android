package com.vectorform.msu.data.responses.vf

class ProfilePicUploadResponse {
    var Result : String? = null
    var ProfilePicUrl : String? = null
    var Reason : String? = null
}