package com.vectorform.msu.data.responses.vf

data class CreateVHCDataArrayResponse(
    val CheckupData: List<CheckupDataItem>,
    val Result: String
) {
    data class CheckupDataItem(
        val Name: String,
        val SectionName: String,
        val VehicleCheckupDataId: Int
    )
}