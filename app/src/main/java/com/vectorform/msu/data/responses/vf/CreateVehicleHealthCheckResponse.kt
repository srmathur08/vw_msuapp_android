package com.vectorform.msu.data.responses.vf

data class CreateVehicleHealthCheckResponse(
    val Result: String,
    val VehicleCheckupId: Int
)