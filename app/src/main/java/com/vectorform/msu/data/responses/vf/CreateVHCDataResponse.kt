package com.vectorform.msu.data.responses.vf

data class CreateVHCDataResponse(
    val Result: String,
    val VehicleCheckupDataId: Int
)