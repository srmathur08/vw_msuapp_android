package com.vectorform.msu.data.responses.vf

data class SRMarkResponse(
    val Result: String
)