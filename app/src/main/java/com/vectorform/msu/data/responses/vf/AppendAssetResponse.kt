package com.vectorform.msu.data.responses.vf

data class AppendAssetResponse(
    val Result: String
)