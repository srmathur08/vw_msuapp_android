package com.vectorform.msu.data.responses.sfdc.services

import java.io.Serializable

data class Attributes(
    val type: String,
    val url: String
) : Serializable