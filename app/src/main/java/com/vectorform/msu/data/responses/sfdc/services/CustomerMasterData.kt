package com.vectorform.msu.data.responses.sfdc.services

import java.io.Serializable

data class CustomerMasterData(
    val Id: String,
    val VW_Active__c: Boolean,
    val VW_CustomerName__c: String,
    val VW_End_User_Mobile__c: String,
    val VW_First_name__c: String,
    val VW_Last_Name__c: String,
    val VW_Pin_code__c: String,
    val VW_Primary_Email_ID__c: String,
    val attributes: Attributes
) : Serializable