package com.vectorform.msu.data.input

data class LoginInput(
    val Password: String,
    val PhoneNo: String,
    val FcmToken : String
)