package com.vectorform.msu.data.responses.sfdc.services

import java.io.Serializable

data class ServiceDealerR(
    val Id: String,
    val Name: String,
    val attributes: Attributes
) : Serializable