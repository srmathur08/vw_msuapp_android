package com.vectorform.msu.data.input

class ProfilePicUploadInput {
    var ImageFileName : String? = null
    var ImageBase64String : String? = null
    var UserId : String? = null

}