package com.vectorform.msu.data.responses.vf

data class LoginResponse(
    val Dealership: String,
    val DealershipId: Int,
    val Email: String,
    val Fullname: String,
    val Result: String,
    val Role: String,
    val RoleId: Int,
    val UserId: Int,
    val ProfilePicUrl : String?
)