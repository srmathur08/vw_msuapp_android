package com.vectorform.msu.data.responses.sfdc

data class InstanceUrlResponse(
    val access_token: String,
    val id: String,
    val instance_url: String,
    val issued_at: String,
    val signature: String,
    val token_type: String
)