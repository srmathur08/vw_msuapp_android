package com.vectorform.msu.data.input

data class CreateVHCDataAssetInput(
    val Asset: String,
    val AssetBase64String: String,
    val IsLastChunk: Boolean,
    val VehicleCheckupDataId: Int
)