package com.vectorform.msu.data.input

data class NotificationReadInput(val UserId: String? , val NotificationId: String?)
