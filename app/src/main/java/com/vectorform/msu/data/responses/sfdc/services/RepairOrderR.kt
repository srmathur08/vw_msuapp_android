package com.vectorform.msu.data.responses.sfdc.services

import java.io.Serializable

data class RepairOrderR(
    val Id: String,
    val attributes: Attributes
) : Serializable