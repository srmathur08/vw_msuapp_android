package com.vectorform.msu.data.input

data class CreateVehicleHealthCheckInput(
    val FuelLevel: Float,
    val Srid: Int,
    val VehicleMileage: String
)