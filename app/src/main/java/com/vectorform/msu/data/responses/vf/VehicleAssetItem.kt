package com.vectorform.msu.data.responses.vf

data class VehicleAssetItem(
    val Id: Int,
    val VehicleCheckupId: Int,
    val Asset: String,
    val IsLastChunk: Boolean,
    val IsVideo: Boolean
)