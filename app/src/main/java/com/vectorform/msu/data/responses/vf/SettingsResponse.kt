package com.vectorform.msu.data.responses.vf

data class SettingsResponse(val Result : String?, val data : List<Setting>) {
}
data class Setting(val Id :Int, val Key : String, val Value : String){

}