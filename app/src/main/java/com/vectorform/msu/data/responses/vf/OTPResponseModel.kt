package com.vectorform.msu.data.responses.vf

data class OTPResponseModel(val Result : String?, val OTP : String?)
