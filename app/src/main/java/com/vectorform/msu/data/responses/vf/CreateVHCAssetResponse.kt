package com.vectorform.msu.data.responses.vf

data class CreateVHCAssetResponse(
    val AssetPath: String,
    val Result: String
)