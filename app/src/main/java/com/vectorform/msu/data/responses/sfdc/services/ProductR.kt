package com.vectorform.msu.data.responses.sfdc.services

import java.io.Serializable

data class ProductR(
    val Grade_Description__c: String,
    val Id: String,
    val attributes: Attributes
) : Serializable