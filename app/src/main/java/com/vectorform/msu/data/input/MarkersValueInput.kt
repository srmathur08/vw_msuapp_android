package com.vectorform.msu.data.input

import java.io.Serializable

data class MarkersValueInput(
    val mark: String,
    val x: Float,
    val y: Float
) : Serializable