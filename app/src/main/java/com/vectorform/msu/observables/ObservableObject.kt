package com.fcaindia.drive.observables

import java.util.*

class ObservableObject : Observable() {

    companion object{

        private val instance = ObservableObject()

        fun getInstance(): ObservableObject? {
            return instance
        }
    }

    fun updateValue(data: Any?) {
        synchronized(this) {
            setChanged()
            notifyObservers(data)
        }
    }
}