package com.vectorform.msu.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.fcaindia.drive.utils.CommonUtils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.vectorform.msu.R
import com.vectorform.msu.ui.splash.SplashActivity
import com.vectorform.msu.utils.Constants


class NotificationService : FirebaseMessagingService() {

    private var mNotificationManager: NotificationManager? = null
    private var mBuilder: NotificationCompat.Builder? = null


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        /*Log.e("From: ", remoteMessage.from)
        Log.e("Message Body: ", remoteMessage.notification!!.body)*/
        createNotification(remoteMessage)

    }

    private fun createNotification(remoteMessage: RemoteMessage) {
        val notifyIntent = Intent(this, SplashActivity::class.java)
        val stackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addParentStack(SplashActivity::class.java)
        stackBuilder.addNextIntent(notifyIntent)
        val contentIntent = stackBuilder
            .getPendingIntent(
                Constants.NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT
                        or PendingIntent.FLAG_ONE_SHOT
            )

        if (mNotificationManager == null) {
            mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            var mChannel =
                mNotificationManager!!.getNotificationChannel(getString(R.string.app_name))
            if (mChannel == null) {
                mChannel = NotificationChannel(
                    getString(R.string.app_name),
                    getString(R.string.app_name),
                    importance
                )
                mChannel.description = getString(R.string.app_name)
                mChannel.enableVibration(true)
                mChannel.vibrationPattern =
                    longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                mNotificationManager!!.createNotificationChannel(mChannel)
            }

        }
        mBuilder = NotificationCompat.Builder(this, getString(R.string.app_name))
        mBuilder!!.setSmallIcon(R.mipmap.ic_launcher)
            .setPriority(NotificationManager.IMPORTANCE_HIGH)
            .setContentTitle(remoteMessage.notification?.title)
            .setContentText(remoteMessage.notification?.body)
            .setContentIntent(contentIntent)
        val notification = mBuilder!!.build()
        mBuilder!!.setAutoCancel(true)
        mNotificationManager!!.notify(Constants.NOTIFICATION_ID, notification)

    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.e("NEW_TOKEN", s)
        sendRegistrationToServer(s)
    }

    private fun sendRegistrationToServer(token: String) {
        CommonUtils.saveFirebaseToken(this, Constants.FIREBASE_DEVICE_TOKEN, token)
    }

}